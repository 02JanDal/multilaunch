# WARNING

This is currently in a very much experimental/early stage. Rebasing, rewriting, restructuring and force pushes can happen at any time.
Do not expect anything to work, it might crash, blow up, start a nuclear war, kill kittens or even silently close itself.

# What's this?

A launcher for games. Currently Factorio and Minecraft. Some info [here](https://gist.github.com/02JanDal/ad6aeac6bba1dac4cc6b).

# License

All code is, unless otherwise noted, licensed under the following license:

    Copyright 2015 Jan Dalheimer <jan@dalheimer.de>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
