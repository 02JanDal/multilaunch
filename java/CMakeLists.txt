include(UseJava)

set(CMAKE_JAVA_COMPILE_FLAGS -source 1.6 -target 1.6)
add_jar(JavaQuery SOURCES JavaQuery.java ENTRY_POINT JavaQuery)
add_custom_target(copy_javaquery
    COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/src/java/jars
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_CURRENT_BINARY_DIR}/JavaQuery.jar ${CMAKE_BINARY_DIR}/src/java/jars/JavaQuery.jar
    DEPENDS JavaQuery
)
add_dependencies(MultiLaunchMinecraftLib copy_javaquery)
