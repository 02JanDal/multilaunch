#!/bin/sh

FILES=$(find \( -name "*.cpp" -or -name "*.h" \) -and -not -regex "^./build/.*" -and -not -regex "^./src/extra/.*" | sort)

ERRORS=()

for file in $FILES
do
	if [ "$(head -n1 $file)" != "// Licensed under the Apache-2.0 license. See README.md for details." ]
	then
		ERRORS+=("$file is missing the license header")
	fi
done

if [ ${#ERRORS[@]} -eq 0 ]
then
	echo "No errors found"
else
	for error in "${ERRORS[@]}"
	do
		echo "$error"
	done
	exit 1
fi
