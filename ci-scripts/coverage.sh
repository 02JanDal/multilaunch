#!/bin/sh

echo "Cleaning old files..."
rm *.gcov 2> /dev/null > /dev/null || true
rm -r *.gcov.dir 2> /dev/null > /dev/null || true

echo "Generating gcov files..."
for file in $(find -name "*.gcda" | grep -F "/src/")
do
	no_ending=$(echo $file | grep -oP "[A-Za-z/\.]*\.[hcpp]+")
	src=../src$(echo $no_ending | grep -oP "/[/a-z]*/[A-Za-z]*\.[hcpp]+")
	echo "  Generating coverage for $src"
	if [ -f $src ]
	then
		llvm-cov -gcda $no_ending.gcda -gcno $no_ending.gcno -o $no_ending.o $src >/dev/null
	fi
done

echo "Cleaning external or 3rd-party gcov files..."
ls | grep -P "^[a-z0-9_\-\.]*\.gcov" | xargs rm 2> /dev/null > /dev/null || true
ls | grep -iF "logicalgui" | xargs rm 2> /dev/null > /dev/null || true

echo "Creating directory structure for codecov..."
for file in $(find -name "*.gcov"); do mkdir $file.dir; mv $file $file.dir/coverage.gcov; done

echo "Uploading to codecov..."
codecov --token $CODECOV_TOKEN
