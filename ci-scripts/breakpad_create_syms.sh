#!/bin/sh

DUMP_SYS=$1

shift

touch targets
rm targets
touch targets

recurseLinkedLibraries()
{
    echo "$2Checking $1"
    targets=$(cat targets)
    if [[ ! "$targets" =~ "$1" ]]
    then
        echo $1 >> targets
        for lib in $(ldd $1 | grep -oP "/[^ ]*")
        do
            recurseLinkedLibraries $lib "  $2"
        done
    fi
}
for target in "$@"
do
    recurseLinkedLibraries $target ""
done

for target in $(cat targets)
do
    target_name=$(echo "$target" | grep -oP "[^/]*$")
    $DUMP_SYS $target > $target_name.sym

    HASH=$(head -n1 $target_name.sym | grep -oP "[A-Z0-9]{10,100}")

    mkdir -p symbols/$target_name/$HASH
    mv $target_name.sym symbols/$target_name/$HASH
done

rm targets
