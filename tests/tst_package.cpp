// Licensed under the Apache-2.0 license. See README.md for details.

#include <QTest>

#include "TestHelpers.h"
#include "core/packages/Package.h"
#include "core/packages/PackageQuery.h"

class PackageTest : public QObject
{
	Q_OBJECT
private slots:
	void test_parsePackageQuery_data()
	{
		QTest::addColumn<QString>("query");
		QTest::addColumn<Package>("package");
		QTest::addColumn<QList<VersionInterval>>("versions");

		QTest::newRow("minecraft, vanilla") << "minecraft:minecraft" << Package("minecraft", "minecraft") << QList<VersionInterval>();
		QTest::newRow("minecraft, vanilla, 1.2.5") << "minecraft:minecraft:1.2.5" << Package("minecraft", "minecraft")
												   << (QList<VersionInterval>() << VersionInterval::fromString("1.2.5"));
		QTest::newRow("minecraft, vanilla, 1.7.*") << "minecraft:minecraft:[1.7,1.8)" << Package("minecraft", "minecraft")
												   << (QList<VersionInterval>() << VersionInterval::fromString("[1.7,1.8)"));
		QTest::newRow("minecraft, forge, 10.13.2.1236") << "minecraft.forge:forge:10.13.2.1236" << Package("minecraft.forge", "forge")
														<< (QList<VersionInterval>() << VersionInterval::fromString("10.13.2.1236"));
		QTest::newRow("folder, mods") << "folder:mods" << Package("folder", "mods") << QList<VersionInterval>();
	}
	void test_parsePackageQuery()
	{
		QFETCH(QString, query);
		QFETCH(Package, package);
		QFETCH(QList<VersionInterval>, versions);

		const PackageQuery q = PackageQuery::fromString(query);

		QCOMPARE(q.package(), package);
		QCOMPARE(q.versions(), versions);
	}

	void test_parsePackage_data()
	{
		QTest::addColumn<QString>("in");
		QTest::addColumn<Package>("out");

		QTest::newRow("namespace only") << "this.is.a.namespace" << Package("this.is.a.namespace", QString());
		QTest::newRow("namespace and id") << "this.is.a.namespace:this.is.an.id" << Package("this.is.a.namespace", "this.is.an.id");
	}
	void test_parsePackage()
	{
		QFETCH(QString, in);
		QFETCH(Package, out);

		QCOMPARE(Package::fromString(in), out);
		QCOMPARE(Package::fromString(in).toString(), in);
		QCOMPARE(out.toString(), in);
	}
};

QTEST_GUILESS_MAIN(PackageTest)

#include "tst_package.moc"
