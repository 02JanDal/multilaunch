// Licensed under the Apache-2.0 license. See README.md for details.

#include <QTest>
#include <QTemporaryDir>
#include <QFile>

#include "TestHelpers.h"

#include "core/FileCache.h"
#include "core/network/CachedDownloadTask.h"
#include "core/util/FileSystem.h"

class tst_Download : public QObject
{
	Q_OBJECT
private slots:
	void test_FileCache()
	{
		QTemporaryDir dir;
		TestHelpers::setupTempDir(dir);

		QVERIFY(g_fileCache->allEntries().isEmpty());

		QFile file(g_fileCache->fileName("test", "a/b/c.d"));
		FS::ensureExists(QFileInfo(file.fileName()).dir());
		file.open(QFile::ReadWrite);
		file.write("This is a test");
		file.close();
		QVERIFY(FS::exists(file.fileName()));

		g_fileCache->addToCache("test", "a/b/c.d", file.fileName());

		QCOMPARE(g_fileCache->allEntries().size(), 1);

		QTest::qWait(300);
		QVERIFY(FS::exists(QDir::current().absoluteFilePath("filecache.dat")));
		g_fileCache->loadNow();

		QCOMPARE(g_fileCache->allEntries().size(), 1);
		FileCache::Entry entry = g_fileCache->getEntry("test", "a/b/c.d");
		QVERIFY(entry.isValid());
		QCOMPARE(entry.group, QString("test"));
		QCOMPARE(entry.id, QString("a/b/c.d"));
		QCOMPARE(entry.origin, QUrl());
		QCOMPARE(entry.onDisk, QFileInfo(file.fileName()));
		QCOMPARE(entry.hash, QByteArray::fromHex("a54d88e06612d820bc3be72877c74f257b561b19"));
	}

	void test_CachedDownloadTask()
	{
		QTemporaryDir dir;
		TestHelpers::setupTempDir(dir);

		CachedDownloadTask *task1 = new CachedDownloadTask("IP Request", "test", "http/ip.json", QUrl("http://httpbin.org/ip"), this);
		TestHelpers::runTask(task1);
		QVERIFY(!FS::read("test/http/ip.json").isEmpty());

		CachedDownloadTask *task2 = new CachedDownloadTask("IP Request", "test", "http/agent.json", QUrl("http://httpbin.org/user-agent"), this);
		TestHelpers::runTask(task2);
		QCOMPARE(FS::read("test/http/agent.json"), QByteArray("{\n  \"user-agent\": \"MultiLaunch (Cached)\"\n}\n"));

		CachedDownloadTask *task3 = new CachedDownloadTask("404 Request", "test", "http/asdf.json", QUrl("http://httpbin.org/asdf"), this);
		TestHelpers::runTask(task3, false);
	}
};

QTEST_GUILESS_MAIN(tst_Download)

#include "tst_download.moc"
