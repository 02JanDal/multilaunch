#!/bin/sh

if [ "$DISPLAY" ]
then
    TEMPLATE=$1
    EXE=$2

    VERSIONS=$($EXE --template $TEMPLATE --template-versions --bare --quiet | tail -n +2 | grep -oP "^[^ ]*")

    for version in $VERSIONS
    do
        echo "Testing $version..."
        echo "  Creating container..."
        UUID=$($EXE --create "$TEMPLATE-$version-Test" --template $TEMPLATE --template-version $version --group "Tests" --quiet)
        echo "    UUID: $UUID"
        echo "  Launching container..."
        $EXE --container "$UUID" --launch --abort 20 --quiet
        EXIT_CODE=$?
        echo "  Removing container..."
        echo "    $($EXE --container "$UUID" --remove --quiet)"
        if [ ! $EXIT_CODE -eq 0 ]
        then
            echo $version >> failed
            echo "########### FAILURE ON $version"
        else
            echo $version >> passed
            echo "  Done. $version passed!"
        fi
    done
else
    echo "Not running template tests: Headless environment"
fi
