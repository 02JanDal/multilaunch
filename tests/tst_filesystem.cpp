// Licensed under the Apache-2.0 license. See README.md for details.

#include <QTest>
#include <QBuffer>
#include <QCryptographicHash>

#include "TestHelpers.h"

#include "core/util/FileSystem.h"

class tst_Download : public QObject
{
	Q_OBJECT
private slots:
	void test_chunkedWriteSmall()
	{
		QBuffer input;
		input.open(QBuffer::ReadWrite);
		for (int i = 0; i < 1024; ++i)
		{
			input.write(QByteArray(128, QChar(1024 % 10 + 'a').toLatin1()));
		}

		QBuffer output;
		output.open(QBuffer::ReadWrite);

		FS::chunkedTransfer(&input, &output);

		QCOMPARE(input.readAll(), output.readAll());
	}
};

QTEST_GUILESS_MAIN(tst_Download)

#include "tst_filesystem.moc"
