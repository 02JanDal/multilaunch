// Licensed under the Apache-2.0 license. See README.md for details.

#include <QTest>

#include "core/packages/Version.h"
#include "TestHelpers.h"

class VersionTest : public QObject
{
	Q_OBJECT
	void setupVersions()
	{
		QTest::addColumn<QString>("first");
		QTest::addColumn<QString>("second");
		QTest::addColumn<bool>("lessThan");
		QTest::addColumn<bool>("equal");

		QTest::newRow("equal, explicit") << "1.2.0" << "1.2.0" << false << true;
		QTest::newRow("equal, implicit 1") << "1.2" << "1.2.0" << false << true;
		QTest::newRow("equal, implicit 2") << "1.2.0" << "1.2" << false << true;
		QTest::newRow("equal, two-digit") << "1.42" << "1.42" << false << true;

		QTest::newRow("lessThan, explicit 1") << "1.2.0" << "1.2.1" << true << false;
		QTest::newRow("lessThan, explicit 2") << "1.2.0" << "1.3.0" << true << false;
		QTest::newRow("lessThan, explicit 3") << "1.2.0" << "2.2.0" << true << false;
		QTest::newRow("lessThan, implicit 1") << "1.2" << "1.2.1" << true << false;
		QTest::newRow("lessThan, implicit 2") << "1.2" << "1.3.0" << true << false;
		QTest::newRow("lessThan, implicit 3") << "1.2" << "2.2.0" << true << false;
		QTest::newRow("lessThan, two-digit") << "1.41" << "1.42" << true << false;

		QTest::newRow("greaterThan, explicit 1") << "1.2.1" << "1.2.0" << false << false;
		QTest::newRow("greaterThan, explicit 2") << "1.3.0" << "1.2.0" << false << false;
		QTest::newRow("greaterThan, explicit 3") << "2.2.0" << "1.2.0" << false << false;
		QTest::newRow("greaterThan, implicit 1") << "1.2.1" << "1.2" << false << false;
		QTest::newRow("greaterThan, implicit 2") << "1.3.0" << "1.2" << false << false;
		QTest::newRow("greaterThan, implicit 3") << "2.2.0" << "1.2" << false << false;
		QTest::newRow("greaterThan, two-digit") << "1.42" << "1.41" << false << false;

		QTest::newRow("equal, appendix") << "1.2.0-pre2" << "1.2.0-pre2" << false << true;
		QTest::newRow("equal, appendix, -/_") << "1.2.0_pre2" << "1.2.0-pre2" << false << true;
		QTest::newRow("equal, appendix, _/-") << "1.2.0-pre2" << "1.2.0_pre2" << false << true;
		QTest::newRow("equal, appendix, words, pre/rc") << "1.2.0-pre" << "1.2.0-rc" << false << true;

		QTest::newRow("lessThan, appendix, implicit") << "1.2.0-pre2" << "1.2.0" << true << false;
		QTest::newRow("lessThan, appendix, explicit") << "1.2.0-pre2" << "1.2.0-pre4" << true << false;
		QTest::newRow("lessThan, appendix, words, alpha/beta") << "1.2.0-alpha" << "1.2.0-beta" << true << false;
		QTest::newRow("lessThan, appendix, words, beta/pre") << "1.2.0-beta" << "1.2.0-pre" << true << false;
		QTest::newRow("lessThan, appendix, random letters, implicit") << "1.2.0-dfd" << "1.2.0" << true << false;
		QTest::newRow("lessThan, appendix, random letters, explicit") << "1.2.0-dfd" << "1.2.0-efd" << true << false;

		QTest::newRow("greaterThan, appendix, implicit") << "1.2.0" << "1.2.0-pre2" << false << false;
		QTest::newRow("greaterThan, appendix, explicit") << "1.2.0-pre4" << "1.2.0-pre2" << false << false;
		QTest::newRow("greaterThan, appendix, words, alpha/beta") << "1.2.0-beta" << "1.2.0-alpha" << false << false;
		QTest::newRow("greaterThan, appendix, words, beta/pre") << "1.2.0-pre" << "1.2.0-beta" << false << false;
		QTest::newRow("greaterThan, appendix, random letters, implicit") << "1.2.0" << "1.2.0-dfd" << false << false;
		QTest::newRow("greaterThan, appendix, random letters, explicit") << "1.2.0-efd" << "1.2.0-dfd" << false << false;
	}

private slots:
	void initTestCase()
	{

	}
	void cleanupTestCase()
	{

	}

	void test_versionIsInInterval_data()
	{
		QTest::addColumn<QString>("version");
		QTest::addColumn<QString>("interval");
		QTest::addColumn<bool>("result");

		QTest::newRow("empty, true") << "1.2.3" << "" << true;
		QTest::newRow("one version, true") << "1.2.3" << "1.2.3" << true;
		QTest::newRow("one version, false") << "1.2.3" << "1.2.2" << false;

		QTest::newRow("one version inclusive <-> infinity, true") << "1.2.3" << "[1.2.3,)" << true;
		QTest::newRow("one version exclusive <-> infinity, true") << "1.2.3" << "(1.2.2,)" << true;
		QTest::newRow("one version inclusive <-> infitity, false") << "1.2.3" << "[1.2.4,)" << false;
		QTest::newRow("one version exclusive <-> infinity, false") << "1.2.3" << "(1.2.3,)" << false;

		QTest::newRow("infinity <-> one version inclusive, true") << "1.2.3" << "(,1.2.3]" << true;
		QTest::newRow("infinity <-> one version exclusive, true") << "1.2.3" << "(,1.2.4)" << true;
		QTest::newRow("infinity <-> one version inclusive, false") << "1.2.3" << "(,1.2.2]" << false;
		QTest::newRow("infinity <-> one version exclusive, false") << "1.2.3" << "(,1.2.3)" << false;

		QTest::newRow("inclusive <-> inclusive, true") << "1.2.3" << "[1.2.2,1.2.3]" << true;
		QTest::newRow("inclusive <-> exclusive, true") << "1.2.3" << "[1.2.3,1.2.4)" << true;
		QTest::newRow("exclusive <-> inclusive, true") << "1.2.3" << "(1.2.2,1.2.3]" << true;
		QTest::newRow("exclusive <-> exclusive, true") << "1.2.3" << "(1.2.2,1.2.4)" << true;
		QTest::newRow("inclusive <-> inclusive, false") << "1.2.3" << "[1.0.0,1.2.2]" << false;
		QTest::newRow("inclusive <-> exclusive, false") << "1.2.3" << "[1.0.0,1.2.3)" << false;
		QTest::newRow("exclusive <-> inclusive, false") << "1.2.3" << "(1.2.3,2.0.0]" << false;
		QTest::newRow("exclusive <-> exclusive, false") << "1.2.3" << "(1.0.0,1.2.3)" << false;
	}
	void test_versionIsInInterval()
	{
		QFETCH(QString, version);
		QFETCH(QString, interval);
		QFETCH(bool, result);

		QCOMPARE(VersionInterval::fromString(interval).contains(Version::fromString(version)), result);
	}

	void test_versionCompareLessThan_data()
	{
		setupVersions();
	}
	void test_versionCompareLessThan()
	{
		QFETCH(QString, first);
		QFETCH(QString, second);
		QFETCH(bool, lessThan);
		QFETCH(bool, equal);

		const auto v1 = Version::fromString(first);
		const auto v2 = Version::fromString(second);

		QCOMPARE(v1 < v2, lessThan);
	}
	void test_versionCompareGreaterThan_data()
	{
		setupVersions();
	}
	void test_versionCompareGreaterThan()
	{
		QFETCH(QString, first);
		QFETCH(QString, second);
		QFETCH(bool, lessThan);
		QFETCH(bool, equal);

		const auto v1 = Version::fromString(first);
		const auto v2 = Version::fromString(second);

		QCOMPARE(v1 > v2, !lessThan && !equal);
	}
	void test_versionCompareEqual_data()
	{
		setupVersions();
	}
	void test_versionCompareEqual()
	{
		QFETCH(QString, first);
		QFETCH(QString, second);
		QFETCH(bool, lessThan);
		QFETCH(bool, equal);

		const auto v1 = Version::fromString(first);
		const auto v2 = Version::fromString(second);

		QCOMPARE(v1 == v2, equal);
	}
};

QTEST_GUILESS_MAIN(VersionTest)

#include "tst_version.moc"
