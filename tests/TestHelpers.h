// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QTemporaryDir>
#include <QTest>
#include <QSignalSpy>

#include "core/tasks/Task.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"
namespace TestHelpers
{
static void setupTempDir(const QTemporaryDir &dir)
{
	Q_ASSERT(dir.isValid());
	QDir::setCurrent(dir.path());
}
static void runTask(Task *task, bool shouldSucceed = true)
{
	QSignalSpy startedSpy(task, &Task::started);
	QSignalSpy succeededSpy(task, &Task::succeeded);
	QSignalSpy failedSpy(task, &Task::failed);
	QSignalSpy finishedSpy(task, &Task::finished);
	QSignalSpy statusSpy(task, &Task::statusChanged);
	QSignalSpy progressSpy(task, &Task::progressChanged);
	QVERIFY(!task->isStarted());
	QVERIFY(!task->isRunning());
	QVERIFY(!task->isFinished());
	task->start();
	QVERIFY(task->isStarted());
	QVERIFY(task->isRunning());
	QVERIFY(!task->isFinished());
	QCOMPARE(startedSpy.count(), 1);
	finishedSpy.wait();
	QVERIFY(task->isStarted());
	QVERIFY(!task->isRunning());
	QVERIFY(task->isFinished());
	QCOMPARE(startedSpy.count(), 1);
	QCOMPARE(finishedSpy.count(), 1);
	if (shouldSucceed)
	{
		QVERIFY(task->isSuccess());
		QVERIFY(!task->isFailure());
		QCOMPARE(succeededSpy.count(), 1);
		QCOMPARE(failedSpy.count(), 0);
	}
	else
	{
		QVERIFY(!task->isSuccess());
		QVERIFY(task->isFailure());
		QCOMPARE(succeededSpy.count(), 0);
		QCOMPARE(failedSpy.count(), 1);
	}
	QVERIFY(statusSpy.count() > 0);
	QVERIFY(progressSpy.count() > 0);
}
}
#pragma clang diagnostic pop
