// Licensed under the Apache-2.0 license. See README.md for details.

#include <QTest>

#include "TestHelpers.h"
#include "core/GlobalSettings.h"

class GlobalSettingsTest : public QObject
{
	Q_OBJECT
private slots:
	void testGlobalSettings()
	{
		QTemporaryDir dir;
		TestHelpers::setupTempDir(dir);

		GlobalSettings *settings = new GlobalSettings;
		settings->registerSetting("kitchensink.color", QVariant::Color, QColor(Qt::black));
		settings->registerSetting("kitchensink.height", QVariant::Int, 15);
		settings->registerSetting("bar", QVariant::String, "abc");
		settings->loadNow();

		QCOMPARE(settings->get("kitchensink.color").value<QColor>(), QColor(Qt::black));
		QCOMPARE(settings->get<int>("kitchensink.height"), 15);
		QCOMPARE(settings->get<QString>("bar"), QString("abc"));

		settings->set("kitchensink.color", QColor(Qt::white));
		settings->set("kitchensink.height", 42);
		settings->set("bar", "cba");

		QCOMPARE(settings->get<QColor>("kitchensink.color"), QColor(Qt::white));
		QCOMPARE(settings->get("kitchensink.height").toInt(), 42);
		QCOMPARE(settings->get<QString>("bar"), QString("cba"));

		settings->saveNow();
		delete settings;
		settings = new GlobalSettings;
		settings->registerSetting("kitchensink.color", QVariant::Color, QColor(Qt::black));
		settings->registerSetting("kitchensink.height", QVariant::Int, 15);
		settings->registerSetting("bar", QVariant::String, "abc");
		settings->loadNow();

		QCOMPARE(settings->get<QColor>("kitchensink.color"), QColor(Qt::white));
		QCOMPARE(settings->get<int>("kitchensink.height"), 42);
		QCOMPARE(settings->get("bar").value<QString>(), QString("cba"));

		settings->reset("kitchensink.color");
		settings->reset("kitchensink.height");
		settings->reset("bar");

		QCOMPARE(settings->get<QColor>("kitchensink.color"), QColor(Qt::black));
		QCOMPARE(settings->get<int>("kitchensink.height"), 15);
		QCOMPARE(settings->get<QString>("bar"), QString("abc"));

		settings->saveNow();
		delete settings;
		settings = new GlobalSettings;
		settings->registerSetting("kitchensink.color", QVariant::Color, QColor(Qt::black));
		settings->registerSetting("kitchensink.height", QVariant::Int, 15);
		settings->registerSetting("bar", QVariant::String, "abc");
		settings->loadNow();

		QCOMPARE(settings->get<QColor>("kitchensink.color"), QColor(Qt::black));
		QCOMPARE(settings->get<int>("kitchensink.height"), 15);
		QCOMPARE(settings->get<QString>("bar"), QString("abc"));
	}
};

QTEST_GUILESS_MAIN(GlobalSettingsTest)

#include "tst_globalsettings.moc"
