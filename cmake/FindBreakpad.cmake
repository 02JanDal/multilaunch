find_path(BREAKPAD_INCLUDE_DIR
    NAMES google_breakpad/common/breakpad_types.h
    PATH_SUFFIXES breakpad
)
find_library(BREAKPAD_CLIENT_LIB libbreakpad_client.a)

set(BREAKPAD_INCLUDE_DIRS "${BREAKPAD_INCLUDE_DIR}")
set(BREAKPAD_LIBRARIES ${BREAKPAD_CLIENT_LIB})
find_program(BREAKPAD_DUMP_SYMS dump_syms)
find_program(BREAKPAD_MINIDUMP_STACKWALK minidump_stackwalk)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Breakpad DEFAULT_MSG BREAKPAD_INCLUDE_DIRS BREAKPAD_LIBRARIES BREAKPAD_DUMP_SYMS BREAKPAD_MINIDUMP_STACKWALK)

mark_as_advanced(BREAKPAD_INCLUDE_DIR BREAKPAD_CLIENT_LIB)

add_library(Breakpad::Client STATIC IMPORTED GLOBAL)
set_property(TARGET Breakpad::Client PROPERTY IMPORTED_LOCATION ${BREAKPAD_CLIENT_LIB})
set_property(TARGET Breakpad::Client PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${BREAKPAD_INCLUDE_DIRS})
