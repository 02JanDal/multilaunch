// Licensed under the Apache-2.0 license. See README.md for details.

#include "config.h"

#include <QApplication>
#include <QDir>
#include <QResource>
#include <QtPlugin>
#include <QPluginLoader>

#ifdef BREAKPAD_FOUND
# define typeof(X) std::remove_reference<decltype(X)>::type
# include <breakpad/client/linux/handler/exception_handler.h>
# include <breakpad/client/linux/handler/minidump_descriptor.h>
# undef typeof
#endif

#include "gui/MainWindow.h"
#include "gui/dialogs/AccountsDialog.h"
#include "gui/dialogs/AccountLoginDialog.h"
#include "gui/pages/ContainerComponentsPage.h"
#include "gui/pages/ContainerSettingsPage.h"
#include "gui/pages/FolderPage.h"
#include "minecraft/java/JavaVersionList.h"
#include "core/util/FileSystem.h"
#include "core/packages/PackageHandler.h"
#include "core/packages/CorePackageHandler.h"
#include "core/Gui.h"
#include "core/BasePlugin.h"
#include "core/GlobalSettings.h"
#include "core/Cli.h"

Q_IMPORT_PLUGIN(MinecraftPlugin)
Q_IMPORT_PLUGIN(FactorioPlugin)
Q_IMPORT_PLUGIN(ScreenshotsPlugin)

class GuiHandler : public QObject
{
	Q_OBJECT
public:
	explicit GuiHandler()
		: QObject()
	{
		g_gui->bind("RequestAccount", &GuiHandler::requestAccount);
		g_gui->bind("LoginToAccount", &GuiHandler::loginToAccount);
		g_gui->bind("CreateComponentsPage", &GuiHandler::createComponentsPage);
		g_gui->bind("CreateSettingsPage", &GuiHandler::createSettingsPage);
		g_gui->bind("CreateFolderPage", &GuiHandler::createFolderPage);
	}

private slots:
	BaseAccount *requestAccount(const QString &type)
	{
		AccountsDialog dlg;
		dlg.setRequestedAccountType(type);
		if (dlg.exec() == QDialog::Accepted)
		{
			return dlg.account();
		}
		else
		{
			return nullptr;
		}
	}
	bool loginToAccount(BaseAccount *account)
	{
		AccountLoginDialog dlg(account);
		return dlg.exec() == QDialog::Accepted;
	}
	IPage *createComponentsPage(const bool isEditable, Container *container)
	{
		return new ContainerComponentsPage(isEditable, container);
	}
	IPage *createSettingsPage(Container *container)
	{
		return new ContainerSettingsPage(container);
	}
	IPage *createFolderPage(FolderComponent *component)
	{
		return new FolderPage(component);
	}
};

int main(int argc, char **argv)
{
#ifdef BREAKPAD_FOUND
	google_breakpad::MinidumpDescriptor descriptor(qPrintable(QDir::tempPath()));
	google_breakpad::ExceptionHandler eh(descriptor,
										 nullptr,
										 [](const google_breakpad::MinidumpDescriptor &desc, void *ctx, bool succeeded)
	{
		printf("Crash report dumped: %s\n", desc.path());
		return succeeded;
	},
										 nullptr,
										 true,
										 -1);
#endif

	QApplication app(argc, argv);
	app.setApplicationDisplayName(QObject::tr("MultiLaunch"));
	app.setApplicationName("MultiLaunch");
	app.setApplicationVersion(ML_VERSION);
	app.setOrganizationDomain("https://bitbucket.org/02JanDal/MultiLaunch");
	app.setOrganizationName("Jan Dalheimer");

	QDir::setCurrent(app.applicationDirPath());

	if (FS::exists("tmp"))
	{
		FS::remove(QDir("tmp/"));
	}

	GuiHandler guiHandler;

	for (auto plugin : QPluginLoader::staticPlugins())
	{
		BasePlugin *p = qobject_cast<BasePlugin *>(plugin.instance());
		p->init();
	}
	g_settings->loadNow();

	g_packages->registerHandler(QStringList() << "core.folder", new CorePackageHandler);

	for (const auto entry : QDir(QDir::current().absoluteFilePath("..")).entryInfoList(QStringList() << "*.rcc", QDir::Files))
	{
		QResource::registerResource(entry.absoluteFilePath());
	}

	g_cli->handle(app);

	MainWindow win;
	win.show();

	return app.exec();
}

#include "main.moc"
