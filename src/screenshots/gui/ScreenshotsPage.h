// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QWidget>
#include <QDir>

#include "core/IPage.h"

class ScreenshotsPageWidget : public QWidget
{
	Q_OBJECT
public:
	explicit ScreenshotsPageWidget(const QDir &dir, QWidget *parent = nullptr);

	void init();
	void apply();
	bool hasChanges() const;

private:
	QDir m_dir;
};

class ScreenshotsPage : public IPage
{
public:
	explicit ScreenshotsPage(const QDir &dir) : m_dir(dir) {}

	QString id() const override { return "screenshots"; }
	QString name() const override { return ScreenshotsPageWidget::tr("Screenshots"); }
	QString header() const override { return name(); }
	QString icon() const override { return "screenshots"; }
	QObject *createWidget() const override { return new ScreenshotsPageWidget(m_dir); }
	void initializePage(QObject *widget) override { qobject_cast<ScreenshotsPageWidget *>(widget)->init(); }
	void applyPage(QObject *widget) override { qobject_cast<ScreenshotsPageWidget *>(widget)->apply(); }
	bool hasChanges(QObject *widget) const override { return qobject_cast<ScreenshotsPageWidget *>(widget)->hasChanges(); }

private:
	const QDir m_dir;
};
