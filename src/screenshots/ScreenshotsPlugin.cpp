// Licensed under the Apache-2.0 license. See README.md for details.

#include "ScreenshotsPlugin.h"

#include "core/accounts/AccountModel.h"

#include "screenshots/imgur/ImgurAccount.h"

void ScreenshotsPlugin::init()
{
	g_accounts->registerType(new ImgurAccountType);
}
QList<IPage *> ScreenshotsPlugin::createGlobalPages() const
{
	return QList<IPage *>();
}
QList<IPagePart *> ScreenshotsPlugin::createGlobalPageParts() const
{
	return QList<IPagePart *>();
}
