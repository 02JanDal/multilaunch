// Licensed under the Apache-2.0 license. See README.md for details.

#include "ContainerIconModel.h"

#include "IconRegistry.h"

DEFINE_SINGLETON(g_containerIcons)

ContainerIconModel::ContainerIconModel()
	: AbstractCommonModel<ContainerIconModelEntry>(Qt::Vertical)
{
	addEntry<QString>(&ContainerIconModelEntry::key, 0, Qt::UserRole);
	addEntry<QString>(&ContainerIconModelEntry::key, 0, Qt::DisplayRole);
	addEntry<QString>(&ContainerIconModelEntry::fullKey, 0, Qt::DecorationRole);

	for (const auto key : g_icons->keys("container_icons"))
	{
		append(ContainerIconModelEntry{key, "container_icons/" + key});
	}
}
