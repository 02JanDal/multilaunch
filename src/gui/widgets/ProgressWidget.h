// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QWidget>

class Task;
class QProgressBar;
class QLabel;

class ProgressWidget : public QWidget
{
	Q_OBJECT
public:
	explicit ProgressWidget(QWidget *parent = nullptr);

public slots:
	void start(Task *task);
	bool exec(Task *task);

private slots:
	void handleTaskFinish();
	void handleTaskStatus();
	void handleTaskProgress();
	void taskDestroyed();

private:
	QLabel *m_label;
	QProgressBar *m_bar;
	Task *m_task = nullptr;
};
