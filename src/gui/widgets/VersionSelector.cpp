// Licensed under the Apache-2.0 license. See README.md for details.

#include "VersionSelector.h"

#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>

#include "core/BaseVersion.h"
#include "gui/dialogs/VersionDialog.h"

VersionSelector::VersionSelector(BaseVersionList *list, QWidget *parent)
	: QWidget(parent), m_list(list)
{
	m_edit = new QLineEdit(this);
	m_edit->setReadOnly(true);
	m_button = new QPushButton(tr("..."), this);
	m_button->setEnabled(!!list);
	connect(m_button, &QPushButton::clicked, this, &VersionSelector::showDialog);

	QHBoxLayout *layout = new QHBoxLayout(this);
	layout->addWidget(m_edit);
	layout->addWidget(m_button);
	setLayout(layout);
}

void VersionSelector::setVersionList(BaseVersionList *list)
{
	setVersion(QWeakPointer<BaseVersion>());
	m_list = list;
	m_version.clear();
	m_button->setEnabled(!!list);
}

void VersionSelector::setVersion(const QWeakPointer<BaseVersion> &version)
{
	m_version = version;
	if (m_version)
	{
		m_edit->setText(m_version.lock()->id().toString());
	}
	else
	{
		m_edit->setText(m_nullText);
	}
	emit versionChanged();
}

void VersionSelector::showDialog()
{
	if (!m_list)
	{
		return;
	}
	VersionDialog dlg(parentWidget() ? parentWidget() : this);
	dlg.setList(m_list);
	dlg.setCurrent(m_version);
	if (dlg.exec() == QDialog::Accepted)
	{
		setVersion(dlg.current());
	}
}
