// Licensed under the Apache-2.0 license. See README.md for details.

#include "ProgressWidget.h"

#include <QProgressBar>
#include <QLabel>
#include <QVBoxLayout>
#include <QEventLoop>

#include "core/tasks/Task.h"

ProgressWidget::ProgressWidget(QWidget *parent)
	: QWidget(parent)
{
	m_label = new QLabel(this);
	m_label->setWordWrap(true);
	m_bar = new QProgressBar(this);
	m_bar->setMinimum(0);
	m_bar->setMaximum(100);
	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(m_label);
	layout->addWidget(m_bar);
	layout->addStretch();
	setLayout(layout);
}

void ProgressWidget::start(Task *task)
{
	if (m_task)
	{
		disconnect(m_task, 0, this, 0);
	}
	m_task = task;
	connect(m_task, &Task::finished, this, &ProgressWidget::handleTaskFinish);
	connect(m_task, &Task::statusChanged, this, &ProgressWidget::handleTaskStatus);
	connect(m_task, &Task::progressChanged, this, &ProgressWidget::handleTaskProgress);
	connect(m_task, &Task::destroyed, this, &ProgressWidget::taskDestroyed);
	if (!m_task->isStarted())
	{
		QMetaObject::invokeMethod(m_task, "start", Qt::QueuedConnection);
	}
}
bool ProgressWidget::exec(Task *task)
{
	QEventLoop loop;
	connect(task, &Task::finished, &loop, &QEventLoop::quit);
	start(task);
	if (!task->isFinished())
	{
		loop.exec();
	}
	return task->isSuccess();
}

void ProgressWidget::handleTaskFinish()
{
	if (m_task->isFailure())
	{
		m_label->setText(m_task->errorString());
	}
}
void ProgressWidget::handleTaskStatus()
{
	m_label->setText(m_task->status());
}
void ProgressWidget::handleTaskProgress()
{
	if (m_task->progress() > 100)
	{
		m_bar->setMaximum(0);
	}
	else
	{
		m_bar->setMaximum(100);
		m_bar->setValue(m_task->progress());
	}
}
void ProgressWidget::taskDestroyed()
{
	m_task = nullptr;
}
