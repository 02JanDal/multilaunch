// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QWidget>
#include <QWeakPointer>

class QLineEdit;
class QPushButton;
class BaseVersion;
class BaseVersionList;

class VersionSelector : public QWidget
{
	Q_OBJECT
	Q_PROPERTY(QWeakPointer<BaseVersion> version READ version WRITE setVersion NOTIFY versionChanged)
public:
	explicit VersionSelector(BaseVersionList *list, QWidget *parent = nullptr);

	void setVersionList(BaseVersionList *list);
	void setNullText(const QString &text) { m_nullText = text; }

	QWeakPointer<BaseVersion> version() const { return m_version; }

public slots:
	void setVersion(const QWeakPointer<BaseVersion> &version);

signals:
	void versionChanged();

private:
	QWeakPointer<BaseVersion> m_version;
	BaseVersionList *m_list;
	QString m_nullText;
	QLineEdit *m_edit;
	QPushButton *m_button;

private slots:
	void showDialog();
};
