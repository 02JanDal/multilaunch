// Licensed under the Apache-2.0 license. See README.md for details.

#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QTreeView>
#include <QCoreApplication>
#include <QInputDialog>
#include <QMessageBox>
#include <QPluginLoader>

#include "core/ContainerModel.h"
#include "core/Container.h"
#include "core/IPage.h"
#include "core/tasks/FlowTask.h"
#include "core/BasePlugin.h"

#include "dialogs/NewContainerWizard.h"
#include "dialogs/PageDialog.h"
#include "dialogs/ProgressDialog.h"
#include "dialogs/AccountsDialog.h"
#include "dialogs/ChooseContainerIconDialog.h"
#include "pages/LogPage.h"
#include "IconRegistry.h"
#include "IconProxyModel.h"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent),
	  ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	m_containerView = new QTreeView(this);
	m_containerView->setFrameShape(QFrame::NoFrame);
	m_containerView->setAnimated(true);
	m_containerView->setModel(IconProxyModel::mixin(g_containerModel));
	m_containerView->setSelectionBehavior(QTreeView::SelectRows);
	m_containerView->setSelectionMode(QTreeView::SingleSelection);
	connect(m_containerView->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &MainWindow::selectionChanged);
	setCentralWidget(m_containerView);

	connect(ui->actionQuit, &QAction::triggered, qApp, &QCoreApplication::quit);

	ui->actionRename->setText("");
	ui->actionChangeIcon->setText("");

	g_icons->setForTarget(ui->actionNewContainer, "new");
	g_icons->setForTarget(ui->actionCopyContainer, "copy");
	g_icons->setForTarget(ui->actionSettings, "settings");
	g_icons->setForTarget(ui->actionAbout, "about");
	g_icons->setForTarget(ui->actionAccounts, "accounts");
}
MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_actionNewContainer_triggered()
{
	NewContainerWizard(this).execNew();
}
void MainWindow::on_actionCopyContainer_triggered()
{
	NewContainerWizard(this).execDuplicate(nullptr); // TODO copying of containers
}

void MainWindow::on_actionChangeGroup_triggered()
{
	Container *container = g_containerModel->container(m_containerView->currentIndex());
	if (container)
	{
		bool ok = false;
		const QString newGroup = QInputDialog::getText(this, tr("Change Group"), tr("Enter the name of the new group"), QLineEdit::Normal,
													   g_containerModel->group(container), &ok);
		if (ok)
		{
			g_containerModel->changeContainerGroup(container, newGroup);
		}
	}
}
void MainWindow::on_actionChangeIcon_triggered()
{
	Container *container = g_containerModel->container(m_containerView->currentIndex());
	if (container)
	{
		ChooseContainerIconDialog dlg(container->iconKey(), this);
		if (dlg.exec() == ChooseContainerIconDialog::Accepted)
		{
			container->setIconKey(dlg.iconKey());
		}
	}
}
void MainWindow::on_actionRename_triggered()
{
	Container *container = g_containerModel->container(m_containerView->currentIndex());
	const QString group = g_containerModel->group(m_containerView->currentIndex());
	if (container)
	{
		bool ok = false;
		const QString newName = QInputDialog::getText(this, tr("Rename"), tr("Change the container name"), QLineEdit::Normal, container->name(), &ok);
		if (ok)
		{
			container->setName(newName);
		}
	}
	else if (!group.isNull())
	{
		bool ok = false;
		const QString newName = QInputDialog::getText(this, tr("Rename"), tr("Change the group name"), QLineEdit::Normal, group, &ok);
		if (ok)
		{
			g_containerModel->renameGroup(group, newName);
		}
	}
}
void MainWindow::on_actionRemove_triggered()
{
	Container *container = g_containerModel->container(m_containerView->currentIndex());
	if (container)
	{
		int res = QMessageBox::warning(this, tr("Continue?"),
									   tr("You are about to remove \"%1\". This is permanent, and cannot be undone. Do you wish to continue?")
									   .arg(container->name()),
									   QMessageBox::No, QMessageBox::Yes);
		if (res == QMessageBox::Yes)
		{
			g_containerModel->deleteContainer(container);
		}
	}
}
void MainWindow::on_actionContainerSettings_triggered()
{
	Container *container = g_containerModel->container(m_containerView->currentIndex());
	if (container)
	{
		QList<IPage *> pages = container->createPages(false);
		resolveParts(pages, container->createPageParts(false));
		PageDialog(pages, nullptr, this).exec();
	}
}
void MainWindow::on_actionLaunch_triggered()
{
	Container *container = g_containerModel->container(m_containerView->currentIndex());
	if (container)
	{
		FlowTask *task = new FlowTask(FlowTask::Launch, container, this);
		QList<IPage *> pages = container->createPages(true);
		resolveParts(pages, container->createPageParts(true));
		pages.prepend(new LogPage(task, container));
		PageDialog(pages, task, this).exec();
	}
}
void MainWindow::on_actionAccounts_triggered()
{
	AccountsDialog(nullptr, this).exec();
}
void MainWindow::on_actionSettings_triggered()
{
	QList<IPage *> pages;
	QList<IPagePart *> parts;
	for (auto plugin : QPluginLoader::staticInstances())
	{
		BasePlugin *p = qobject_cast<BasePlugin *>(plugin);
		pages.append(p->createGlobalPages());
		parts.append(p->createGlobalPageParts());
	}
	resolveParts(pages, parts);
	PageDialog(pages, nullptr, this).exec();
}

void MainWindow::selectionChanged(const QModelIndex &current, const QModelIndex &previous)
{
	const Container *container = g_containerModel->container(current);
	const Container *previousContainer = g_containerModel->container(previous);
	const QString group = g_containerModel->group(current);
	ui->instanceToolbar->setEnabled(true);
	if (previousContainer)
	{
		disconnect(previousContainer, &Container::iconKeyChanged, this, &MainWindow::containerIconChanged);
	}
	if (container)
	{
		g_icons->setForTarget(ui->actionChangeIcon, container->icon());
		connect(container, &Container::iconKeyChanged, this, &MainWindow::containerIconChanged);
		ui->actionRename->setText(container->name());
		ui->actionRename->setEnabled(true);
		ui->actionChangeGroup->setEnabled(true);
		ui->actionChangeIcon->setEnabled(true);
		ui->actionLaunch->setEnabled(true);
		ui->actionContainerSettings->setEnabled(true);
	}
	else if (!group.isNull())
	{
		g_icons->setForTarget(ui->actionChangeIcon, QString());
		ui->actionRename->setText(group);
		ui->actionRename->setEnabled(true);
		ui->actionChangeGroup->setEnabled(false);
		ui->actionChangeIcon->setEnabled(false);
		ui->actionLaunch->setEnabled(false);
		ui->actionContainerSettings->setEnabled(false);
	}
	else
	{
		ui->instanceToolbar->setEnabled(false);
	}
}

void MainWindow::containerIconChanged()
{
	g_icons->setForTarget(ui->actionChangeIcon, qobject_cast<Container *>(sender())->icon());
}
