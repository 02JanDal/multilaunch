// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/util/AbstractCommonModel.h"
#include "core/util/Singleton.h"

struct ContainerIconModelEntry
{
	QString key;
	QString fullKey;
};
class ContainerIconModel : public AbstractCommonModel<ContainerIconModelEntry>
{
public:
	explicit ContainerIconModel();
};
DECLARE_SINGLETON(ContainerIconModel, g_containerIcons)
