// Licensed under the Apache-2.0 license. See README.md for details.

#include "ContainerSettingsPage.h"

#include <QFormLayout>
#include <QLineEdit>
#include <QComboBox>

#include "core/Container.h"
#include "core/ContainerModel.h"

ContainerSettingsPageWidget::ContainerSettingsPageWidget(QWidget *parent)
	: QWidget(parent)
{
	m_nameEdit = new QLineEdit(this);
	m_groupEdit = new QComboBox(this);

	QFormLayout *layout = new QFormLayout(this);
	layout->addRow(tr("Name"), m_nameEdit);
	layout->addRow(tr("Group"), m_groupEdit);
}

void ContainerSettingsPageWidget::init(Container *container)
{
	m_nameEdit->setText(container->name());
	m_groupEdit->setCurrentText(g_containerModel->group(container));
}
void ContainerSettingsPageWidget::apply(Container *container)
{
	if (container->name() != m_nameEdit->text())
	{
		container->setName(m_nameEdit->text());
	}
	if (m_groupEdit->currentText() != g_containerModel->group(container))
	{
		g_containerModel->changeContainerGroup(container, m_groupEdit->currentText());
	}
}
bool ContainerSettingsPageWidget::hasChanges(Container *container) const
{
	if (container->name() != m_nameEdit->text())
	{
		return true;
	}
	if (m_groupEdit->currentText() != g_containerModel->group(container))
	{
		return true;
	}
	return false;
}
