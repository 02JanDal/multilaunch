// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QWidget>
#include <QIcon>
#include "core/IPage.h"

class Container;
class ComponentModel;
class VersionSelector;
class QTreeView;
class QGroupBox;
class QPushButton;

class ContainerComponentsPageWidget : public QWidget
{
	Q_OBJECT
public:
	explicit ContainerComponentsPageWidget(const bool isEditable, QWidget *parent = nullptr);

	void init(Container *container);
	void apply(Container *container);
	bool hasChanges(Container *container) const;

private slots:
	void currentChanged(const QModelIndex &current, const QModelIndex &previous);
	void versionChanged();
	void removeClicked();

private:
	ComponentModel *m_model;
	QTreeView *m_tree;
	QGroupBox *m_currentBox;
	VersionSelector *m_versionSelector;
	QPushButton *m_removeBtn;
};

class ContainerComponentsPage : public IPage
{
public:
	explicit ContainerComponentsPage(const bool isEditable, Container *container) : m_container(container), m_editable(isEditable) {}

	QString id() const override { return "Container.Components"; }
	QString name() const override { return QObject::tr("Components"); }
	QString header() const override { return QObject::tr("Components"); }
	QString icon() const override { return "components"; }

	QObject *createWidget() const override { return new ContainerComponentsPageWidget(m_editable); }
	void initializePage(QObject *widget) override { qobject_cast<ContainerComponentsPageWidget *>(widget)->init(m_container); }
	void applyPage(QObject *widget) override { qobject_cast<ContainerComponentsPageWidget *>(widget)->apply(m_container); }
	bool hasChanges(QObject *widget) const override { return qobject_cast<ContainerComponentsPageWidget *>(widget)->hasChanges(m_container); }

private:
	Container *m_container;
	bool m_editable;
};
