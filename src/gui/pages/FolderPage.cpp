// Licensed under the Apache-2.0 license. See README.md for details.

#include "FolderPage.h"

#include <QTreeView>
#include <QFileSystemModel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QDesktopServices>
#include <QStandardPaths>
#include <QMessageBox>

#include "core/components/FolderComponent.h"
#include "core/util/FileSystem.h"
#include "gui/IconRegistry.h"

FolderPageWidget::FolderPageWidget(FolderComponent *component, QWidget *parent)
	: QWidget(parent), m_component(component)
{
	m_view = new QTreeView(this);
	m_model = new QFileSystemModel(this);
	m_view->setModel(m_model);
	m_view->setSortingEnabled(true);
	m_view->setColumnHidden(1, true);
	m_view->setColumnHidden(2, true);
	m_view->setColumnHidden(3, true);

	QPushButton *addBtn = new QPushButton(tr("Add"), this);
	QPushButton *removeBtn = new QPushButton(tr("Remove"), this);
	QPushButton *viewBtn = new QPushButton(tr("View Folder"), this);

	connect(addBtn, &QPushButton::clicked, [this]()
	{
		const QStringList locations = QStandardPaths::standardLocations(QStandardPaths::DownloadLocation);
		const QString file = QFileDialog::getOpenFileName(this, tr("Choose file"), locations.isEmpty() ? QDir::homePath() : locations.first());
		if (!file.isEmpty())
		{
			try
			{
				FS::copy(file, m_component->dir());
			}
			catch (Exception &e)
			{
				QMessageBox::warning(this, tr("Error"), tr("An error occured while trying to copy %1 to %2:\n\n%3").arg(
										 file, m_component->dir().absolutePath(), e.message()));
			}
		}
	});
	connect(removeBtn, &QPushButton::clicked, [this]()
	{
		const QModelIndex index = m_view->currentIndex();
		if (index.isValid())
		{
			const QString name = index.sibling(index.row(), 0).data().toString();
			const int res = QMessageBox::warning(this, tr("Remove?"), tr("Are you sure you want to remove %1? This is permanent!").arg(name),
												 QMessageBox::No | QMessageBox::Yes, QMessageBox::No);
			if (res == QMessageBox::Yes)
			{
				if (!m_model->remove(index))
				{
					QMessageBox::warning(this, tr("Error"), tr("An unknown error occured while trying to remove %1").arg(name));
				}
			}
		}
	});
	connect(viewBtn, &QPushButton::clicked, [this]()
	{
		QDesktopServices::openUrl(QUrl::fromLocalFile(m_component->dir().absolutePath()));
	});

	QVBoxLayout *btns = new QVBoxLayout;
	btns->addWidget(addBtn);
	btns->addWidget(removeBtn);
	btns->addStretch();
	btns->addWidget(viewBtn);

	QHBoxLayout *layout = new QHBoxLayout(this);
	layout->addWidget(m_view);
	layout->addLayout(btns);
}

void FolderPageWidget::init()
{
	m_view->setRootIndex(m_model->setRootPath(m_component->dir().absolutePath()));
}
void FolderPageWidget::apply()
{
}

FolderPage::FolderPage(FolderComponent *component)
	: m_component(component)
{
}
QString FolderPage::id() const
{
	return "folder_" + m_component->dir().dirName();
}
QString FolderPage::name() const
{
	return QObject::tr("Folder: %1").arg(m_component->dir().dirName());
}
QString FolderPage::icon() const
{
	return "folder";
}
