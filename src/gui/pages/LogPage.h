// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QWidget>
#include "core/IPage.h"

#include "core/components/BaseComponent.h"
#include "core/BaseProcess.h"

class QPlainTextEdit;
class QStackedLayout;
class QPushButton;
class BaseLaunchData;
class FlowTask;
class Container;
class ProgressWidget;

class LogPageWidget : public QWidget
{
	Q_OBJECT
public:
	explicit LogPageWidget(FlowTask *task, Container *container, QWidget *parent = nullptr);

signals:
	void abortClicked();

private slots:
	void flowInteractionRequired(const Stage stage, IStageData *data);
	void processStateChanged(const BaseProcess::State state);
	void logMessage(const QString &text, const BaseProcess::LogLevel level);

private:
	QPlainTextEdit *m_edit;
	ProgressWidget *m_progressWidget;
	QPushButton *m_abortBtn;

	FlowTask *m_task;
	BaseLaunchData *m_data;
};

class LogPage : public IPage
{
public:
	explicit LogPage(FlowTask *task, Container *container) : m_task(task), m_container(container) {}

	QString id() const override { return "Core.Log"; }
	QString name() const override { return QObject::tr("Log"); }
	QString header() const override { return QObject::tr("Log"); }
	QString icon() const override { return "logs"; }

	QObject *createWidget() const override { return new LogPageWidget(m_task, m_container); }
	void initializePage(QObject *widget) override {}
	void applyPage(QObject *widget) override {}
	bool hasChanges(QObject *widget) const override { return false; }

private:
	FlowTask *m_task;
	Container *m_container;
};
