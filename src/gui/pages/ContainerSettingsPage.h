// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QWidget>
#include <QIcon>
#include "core/IPage.h"

class Container;
class QLineEdit;
class QComboBox;

class ContainerSettingsPageWidget : public QWidget
{
	Q_OBJECT
public:
	explicit ContainerSettingsPageWidget(QWidget *parent = nullptr);

	void init(Container *container);
	void apply(Container *container);
	bool hasChanges(Container *container) const;

private:
	QLineEdit *m_nameEdit;
	QComboBox *m_groupEdit;
};

class ContainerSettingsPage : public IPage
{
public:
	explicit ContainerSettingsPage(Container *container) : m_container(container) {}

	QString id() const override { return "Container.Settings"; }
	QString name() const override { return QObject::tr("Settings"); }
	QString header() const override { return QObject::tr("Settings"); }
	QString icon() const override { return "settings"; }

	QObject *createWidget() const override { return new ContainerSettingsPageWidget; }
	void initializePage(QObject *widget) override { qobject_cast<ContainerSettingsPageWidget *>(widget)->init(m_container); }
	void applyPage(QObject *widget) override { qobject_cast<ContainerSettingsPageWidget *>(widget)->apply(m_container); }
	bool hasChanges(QObject *widget) const override { return qobject_cast<ContainerSettingsPageWidget *>(widget)->hasChanges(m_container); }

private:
	Container *m_container;
};
