// Licensed under the Apache-2.0 license. See README.md for details.

#include "ContainerComponentsPage.h"

#include <QHBoxLayout>
#include <QTreeView>
#include <QGroupBox>
#include <QPushButton>
#include <QFormLayout>
#include <QMessageBox>

#include "core/Container.h"
#include "core/ComponentModel.h"
#include "core/packages/PackageHandler.h"
#include "gui/widgets/VersionSelector.h"

ContainerComponentsPageWidget::ContainerComponentsPageWidget(const bool isEditable, QWidget *parent)
	: QWidget(parent)
{
	m_tree = new QTreeView(this);

	m_currentBox = new QGroupBox(this);
	m_currentBox->setEnabled(false);
	QFormLayout *boxLayout = new QFormLayout(m_currentBox);

	m_versionSelector = new VersionSelector(nullptr, m_currentBox);
	m_versionSelector->setEnabled(false);
	boxLayout->addWidget(m_versionSelector);
	connect(m_versionSelector, &VersionSelector::versionChanged, this, &ContainerComponentsPageWidget::versionChanged);

	m_removeBtn = new QPushButton(tr("Remove"), m_currentBox);
	m_removeBtn->setEnabled(false);
	boxLayout->addWidget(m_removeBtn);
	connect(m_removeBtn, &QPushButton::clicked, this, &ContainerComponentsPageWidget::removeClicked);

	QHBoxLayout *layout = new QHBoxLayout(this);
	layout->addWidget(m_tree);
	layout->addWidget(m_currentBox);
}

void ContainerComponentsPageWidget::init(Container *container)
{
	m_tree->setModel(m_model = new ComponentModel(container, this));
	connect(m_tree->selectionModel(), &QItemSelectionModel::currentChanged, this, &ContainerComponentsPageWidget::currentChanged);
}
void ContainerComponentsPageWidget::apply(Container *container)
{
	// TODO defer changing of versions until here
}
bool ContainerComponentsPageWidget::hasChanges(Container *container) const
{
	return false;
}

void ContainerComponentsPageWidget::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	BaseComponent *comp = m_model->component(current);
	if (comp)
	{
		m_currentBox->setEnabled(true);
		m_removeBtn->setEnabled(comp->canRemove());
		m_versionSelector->setEnabled(comp->canChangeVersion());
		auto handler = g_packages->handler(comp->package());
		m_versionSelector->setVersionList(handler ? handler->getVersionsList(comp->package()) : nullptr);
		m_versionSelector->setVersion(comp->versionObject());
	}
	else
	{
		m_currentBox->setEnabled(false);
		m_removeBtn->setEnabled(false);
		m_versionSelector->setEnabled(false);
		m_versionSelector->setVersionList(nullptr);
		m_versionSelector->setVersion(QWeakPointer<BaseVersion>());
	}
}

void ContainerComponentsPageWidget::removeClicked()
{
	BaseComponent *comp = m_model->component(m_tree->currentIndex());
	if (comp && comp->canRemove())
	{
		const int res = QMessageBox::question(this, tr("Remove?"), tr("Do you really want to remove %1?").arg(comp->type()),
											  QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
		if (res == QMessageBox::Yes)
		{
			try
			{
				comp->remove();
			}
			catch (Exception &e)
			{
				QMessageBox::critical(this, tr("Error"), tr("Couldn't remove %1: %2").arg(comp->type(), e.message()));
			}
		}
	}
}
void ContainerComponentsPageWidget::versionChanged()
{
	BaseComponent *comp = m_model->component(m_tree->currentIndex());
	if (comp && comp->canChangeVersion() && !m_versionSelector->version().isNull())
	{
		comp->setVersion(m_versionSelector->version().lock());
	}
}
