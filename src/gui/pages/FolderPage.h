// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/IPage.h"
#include <QWidget>

class FolderComponent;
class QTreeView;
class QFileSystemModel;

class FolderPageWidget : public QWidget
{
	Q_OBJECT
public:
	explicit FolderPageWidget(FolderComponent *component, QWidget *parent = nullptr);

	void init();
	void apply();
	bool hasChanges() const { return false; }

private:
	FolderComponent *m_component;
	QTreeView *m_view;
	QFileSystemModel *m_model;
};
class FolderPage : public IPage
{
public:
	explicit FolderPage(FolderComponent *component);

	QString id() const override;
	QString name() const override;
	QString header() const override { return name(); }
	QString icon() const override;
	QObject *createWidget() const override { return new FolderPageWidget(m_component); }
	void initializePage(QObject *widget) override { qobject_cast<FolderPageWidget *>(widget)->init(); }
	void applyPage(QObject *widget) override { qobject_cast<FolderPageWidget *>(widget)->apply(); }
	bool hasChanges(QObject *widget) const override { return qobject_cast<FolderPageWidget *>(widget)->hasChanges(); }

private:
	FolderComponent *m_component;
};
