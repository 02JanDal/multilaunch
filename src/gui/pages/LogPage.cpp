// Licensed under the Apache-2.0 license. See README.md for details.

#include "LogPage.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QStackedLayout>

#include "core/Container.h"
#include "core/tasks/FlowTask.h"
#include "gui/widgets/ProgressWidget.h"

LogPageWidget::LogPageWidget(FlowTask *task, Container *container, QWidget *parent)
	: QWidget(parent), m_task(task)
{
	m_edit = new QPlainTextEdit(this);
	m_progressWidget = new ProgressWidget(this);
	m_edit->setVisible(false);
	m_edit->setReadOnly(true);

	m_abortBtn = new QPushButton(tr("Abort"), this);
	m_abortBtn->setEnabled(false);
	connect(m_abortBtn, &QPushButton::clicked, this, &LogPageWidget::abortClicked);

	QHBoxLayout *btnsLayout = new QHBoxLayout;
	btnsLayout->addStretch();
	btnsLayout->addWidget(m_abortBtn);

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(m_edit);
	layout->addWidget(m_progressWidget);
	layout->addLayout(btnsLayout);

	connect(m_task, &FlowTask::interactionRequired, this, &LogPageWidget::flowInteractionRequired);

	QMetaObject::invokeMethod(m_progressWidget, "start", Qt::QueuedConnection, Q_ARG(Task*, m_task));
}

void LogPageWidget::flowInteractionRequired(const Stage stage, IStageData *data)
{
	if (stage == Stage::UpdateCheck)
	{
		// TODO
		m_task->interactionFinished(true);
	}
	else if (stage == Stage::Launch)
	{
		m_data = static_cast<BaseLaunchData *>(data);
		connect(m_data->process, &BaseProcess::stateChanged, this, &LogPageWidget::processStateChanged);
		connect(m_data->process, &BaseProcess::log, this, &LogPageWidget::logMessage);
		m_edit->setVisible(true);
		m_progressWidget->setVisible(false);
		m_data->process->start();
	}
}

void LogPageWidget::logMessage(const QString &text, const BaseProcess::LogLevel level)
{
	QColor color;
	switch (level)
	{
	case BaseProcess::Extra:
		color = Qt::blue; break;
	case BaseProcess::Debug:
	case BaseProcess::Trace:
		color = Qt::gray; break;
	case BaseProcess::Info:
		color = Qt::black; break;
	case BaseProcess::Warn:
		color = Qt::darkYellow; break;
	case BaseProcess::Error:
		color = Qt::darkRed; break;
	case BaseProcess::Fatal:
		color = Qt::red; break;
	default:
		color = Qt::black;
	}
	QTextCursor cursor = m_edit->textCursor();
	QTextCharFormat fmt;
	fmt.setForeground(QBrush(color));
	fmt.setFont(QFont("Monospace"));
	cursor.movePosition(QTextCursor::End);
	cursor.insertText(text, fmt);
	cursor.insertBlock();
}

void LogPageWidget::processStateChanged(const BaseProcess::State state)
{
	if (state == BaseProcess::Running)
	{
		m_abortBtn->setEnabled(true);
	}
	else
	{
		m_abortBtn->setEnabled(false);
	}
}
