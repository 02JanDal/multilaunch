// Licensed under the Apache-2.0 license. See README.md for details.

#include "IconRegistry.h"

#include <QDir>
#include <QMetaProperty>
#include <QLabel>
#include <QAction>
#include <QAbstractItemModel>
#include <QUrl>
#include <QPersistentModelIndex>

#include "core/network/CachedDownloadTask.h"
#include "core/FileCache.h"

DEFINE_SINGLETON(g_icons)

/// Fetches an icon and updates a model index
class ModelIconFetcher : public QObject
{
	Q_OBJECT
public:
	explicit ModelIconFetcher(const QPersistentModelIndex &index, const QUrl &url, QObject *parent = nullptr)
		: QObject(parent)
	{
		CachedDownloadTask *task = new CachedDownloadTask("Icon download", "icons", url.toString(), url, this);
		connect(task, &CachedDownloadTask::succeeded, this, [this, index]()
		{
			if (index.isValid())
			{
				emit const_cast<QAbstractItemModel *>(index.model())->dataChanged(index, index, QVector<int>() << Qt::DecorationRole);
			}
			g_icons->m_modelFetchers.remove(index);
		});
		connect(task, &CachedDownloadTask::finished, this, &ModelIconFetcher::deleteLater);
		QMetaObject::invokeMethod(task, "start", Qt::QueuedConnection);
	}
};
/// Fetches an icon and updates a target
class TargetIconFetcher : public QObject
{
	Q_OBJECT
public:
	explicit TargetIconFetcher(const IconRegistry::Target &target, const QUrl &url, QObject *parent = nullptr)
		: QObject(parent)
	{
		CachedDownloadTask *task = new CachedDownloadTask("Icon download", "icons", url.toString(), url, this);
		connect(task, &CachedDownloadTask::succeeded, this, [this, target]()
		{
			if (!target.object.isNull() && g_icons->m_targetFetchers.contains(target))
			{
				g_icons->updateTarget(target);
			}
			g_icons->m_targetFetchers.remove(target);
		});
		connect(task, &CachedDownloadTask::finished, this, &ModelIconFetcher::deleteLater);
		QMetaObject::invokeMethod(task, "start", Qt::QueuedConnection);
	}
};

IconRegistry::IconRegistry()
{
}

QIcon IconRegistry::icon(const QString &key) const
{
	// first case: local file with an absolute path
	const QFileInfo fileinfo(key);
	if (fileinfo.exists() && fileinfo.isAbsolute())
	{
		return QIcon(key);
	}

	const QStringList files = findFiles(QString(key).remove(".png"));
	QIcon icon;
	for (const auto file : files)
	{
		bool ok = false;
		const int resolution = QFileInfo(file).baseName().toInt(&ok);
		if (ok)
		{
			icon.addFile(file, QSize(resolution, resolution));
		}
		else
		{
			icon.addFile(file);
		}
	}
	return icon;
}
QIcon IconRegistry::icon(const QString &key, const QModelIndex &index) const
{
	const QUrl url(key);
	if (url.isValid() && !url.isRelative())
	{
		// check if we need to (re)fetch the icon
		const FileCache::Entry entry = g_fileCache->getEntry("icons", url.toString());
		if (!entry.isValid() && !m_modelFetchers.contains(index))
		{
			m_modelFetchers.insert(index, new ModelIconFetcher(index, url));
		}
		// return something even if we don't have an icon, this function will be called again when the skin has arrived
		return icon(QDir::current().absoluteFilePath("icons/" + url.toString()));
	}
	else
	{
		return icon(key);
	}
}
QPixmap IconRegistry::pixmap(const QString &key) const
{
	// first case: local file with an absolute path
	const QFileInfo fileinfo(key);
	if (fileinfo.exists() && fileinfo.isAbsolute())
	{
		return QPixmap(key);
	}

	const QStringList files = findFiles(QString(key).remove(".png"));
	if (files.size() == 1)
	{
		// early exit for when only a single file is available
		return QPixmap(files.first());
	}
	else if (files.isEmpty())
	{
		// early exit for when no files are available
		return QPixmap();
	}

	// sort all icon sizes so that we can return the biggest
	QList<QPair<int, QString>> sizes;
	for (const auto file : files)
	{
		bool ok = false;
		const int size = QFileInfo(file).baseName().toInt(&ok);
		if (ok)
		{
			sizes.append(qMakePair(size, file));
		}
	}
	if (sizes.isEmpty())
	{
		// couldn't find any valid sizes: pick a file at random
		return QPixmap(files.first());
	}
	std::sort(sizes.begin(), sizes.end(), [](const QPair<int, QString> &a, const QPair<int, QString> &b) { return a.first < b.first; });
	return QPixmap(files.first());
}

QStringList IconRegistry::keys(const QString &base) const
{
	return QDir(":/" + base).entryList(QDir::Files);
}

void IconRegistry::setTheme(const QString &theme)
{
	m_theme = theme;

	// update targets
	for (const auto target : m_targets.keys())
	{
		updateTarget(target);
	}
	for (const auto pair : m_targetModels)
	{
		QAbstractItemModel *model = pair.first;
		emit model->dataChanged(model->index(0, pair.second), model->index(model->rowCount(), pair.second), QVector<int>() << Qt::DecorationRole);
	}
}
QString IconRegistry::theme() const
{
	return m_theme;
}

void IconRegistry::setForTarget(QObject *target, const char *property, const QString &key, const int size)
{
	auto t = Target{target, property, size};
	m_targets[t] = key;
	updateTarget(t);

	// still update the value so that it gets emptied
	if (key.isEmpty())
	{
		m_targets.remove(t);
	}
}
void IconRegistry::setForTarget(QLabel *label, const QString &key, const int size)
{
	setForTarget(label, "pixmap", key, size);
}
void IconRegistry::setForTarget(QAction *action, const QString &key, const int size)
{
	setForTarget(action, "icon", key, size);
}

void IconRegistry::setForModel(QAbstractItemModel *model, const int column)
{
	m_targetModels.append(qMakePair(model, column));
}

void IconRegistry::updateTarget(const IconRegistry::Target &target)
{
	Q_ASSERT(m_targets.contains(target));

	QString key = m_targets[target];
	const QUrl url(key);
	if (url.isValid() && !url.isRelative())
	{
		// check if we need to (re)fetch the icon
		const FileCache::Entry entry = g_fileCache->getEntry("icons", url.toString());
		if (!entry.isValid() && !m_targetFetchers.contains(target))
		{
			m_targetFetchers.insert(target, new TargetIconFetcher(target, url));
		}
		// set the icon to something even if we don't have an icon, this function will be called again when the skin has arrived
		key = QDir::current().absoluteFilePath("icons/" + url.toString());
	}

	const QMetaProperty prop = target.object->metaObject()->property(target.object->metaObject()->indexOfProperty(target.property));
	if (target.size > 0)
	{
		prop.write(target.object, icon(key).pixmap(target.size));
	}
	else if (prop.type() == QVariant::Icon)
	{
		prop.write(target.object, icon(key));
	}
	else
	{
		prop.write(target.object, pixmap(key));
	}
}

QStringList IconRegistry::findFiles(const QString &key) const
{
	QStringList files;

	// first case: for the current theme
	for (const auto entry : QDir(":/" + m_theme + "/" + key).entryInfoList(QDir::Files))
	{
		files += entry.absoluteFilePath();
	}
	if (!files.isEmpty())
	{
		return files;
	}

	// second case: the default fallback theme; "icons"
	for (const auto entry : QDir(":/icons/" + key).entryInfoList(QDir::Files))
	{
		files += entry.absoluteFilePath();
	}
	if (!files.isEmpty())
	{
		return files;
	}

	// third case: full path
	if (!QFile::exists(":/" + key + ".png"))
	{
		qWarning("Requested icon \"%s\" doesn't exist", qPrintable(key));
	}
	return QStringList() << ":/" + key + ".png";
}

#include "IconRegistry.moc"
