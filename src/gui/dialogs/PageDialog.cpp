// Licensed under the Apache-2.0 license. See README.md for details.

#include "PageDialog.h"
#include "ui_PageDialog.h"

#include <QGroupBox>
#include <QPushButton>
#include <QMessageBox>
#include <QCloseEvent>
#include <QVBoxLayout>
#include <QMap>

#include "core/IPage.h"
#include "core/tasks/FlowTask.h"
#include "gui/IconRegistry.h"
#include "gui/IconProxyModel.h"

class PageModel : public QAbstractListModel
{
	Q_OBJECT
public:
	explicit PageModel(const QList<IPage *> &pages, QObject *parent = nullptr)
		: QAbstractListModel(parent), m_pages(pages)
	{
	}

	int rowCount(const QModelIndex &parent) const override
	{
		return m_pages.size();
	}
	QVariant data(const QModelIndex &index, int role) const override
	{
		IPage *page = m_pages.at(index.row());
		switch (role)
		{
		case Qt::DisplayRole: return page->name();
		case Qt::DecorationRole: return page->icon();
		}
		return QVariant();
	}

private:
	QList<IPage *> m_pages;
};

PageDialog::PageDialog(const QList<IPage *> &pages,
					   FlowTask *runTask,
					   QWidget *parent)
	: QDialog(parent), ui(new Ui::PageDialog), m_pages(pages), m_task(runTask)
{
	Q_ASSERT_X(!pages.isEmpty(), "PageDialog::PageDialog", "Cannot pass empty page list");

	ui->setupUi(this);
	ui->pageList->setModel(IconProxyModel::mixin(new PageModel(m_pages, this)));

	if (m_task)
	{
		ui->buttonBox->setEnabled(m_task->canAbort());
		ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
		ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(false);
		connect(m_task, &Task::canAbortChanged, ui->buttonBox, &QDialogButtonBox::setEnabled);
		connect(m_task, &Task::finished, this, [this]()
		{
			ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
			ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(true);
			ui->buttonBox->setEnabled(true);
		});
	}

	connect(ui->buttonBox->button(QDialogButtonBox::Ok), &QAbstractButton::clicked, this, &PageDialog::okClicked);
	connect(ui->buttonBox->button(QDialogButtonBox::Apply), &QAbstractButton::clicked, this, &PageDialog::applyClicked);
	connect(ui->buttonBox->button(QDialogButtonBox::Cancel), &QAbstractButton::clicked, this, &PageDialog::cancelClicked);
	connect(ui->pageList->selectionModel(), &QItemSelectionModel::currentChanged, this, &PageDialog::currentChanged);

	currentChanged(ui->pageList->model()->index(0, 0), QModelIndex());
}
PageDialog::~PageDialog()
{
	qDeleteAll(m_pages);
	delete ui;
}

void PageDialog::closeEvent(QCloseEvent *event)
{
	if (m_currentPage && m_currentPage->hasChanges(m_currentWidget))
	{
		int answer = askForDiscard();
		if (answer == QMessageBox::Cancel)
		{
			event->ignore();
		}
		else if (answer == QMessageBox::Apply)
		{
			m_currentPage->applyPage(m_currentWidget);
			event->accept();
		}
		else if (answer == QMessageBox::Discard)
		{
			event->accept();
		}
	}
	else
	{
		event->accept();
	}
}

void PageDialog::okClicked()
{
	if (m_currentPage && m_currentPage->hasChanges(m_currentWidget))
	{
		m_currentPage->applyPage(m_currentWidget);
	}
	accept();
}
void PageDialog::applyClicked()
{
	if (m_currentPage && m_currentPage->hasChanges(m_currentWidget))
	{
		m_currentPage->applyPage(m_currentWidget);
	}
}
void PageDialog::cancelClicked()
{
	if (m_currentPage && m_currentPage->hasChanges(m_currentWidget))
	{
		int answer = askForDiscard();
		if (answer == QMessageBox::Apply)
		{
			m_currentPage->applyPage(m_currentWidget);
			accept();
		}
		else if (answer == QMessageBox::Discard)
		{
			reject();
		}
		else
		{
			return;
		}
	}
	else
	{
		reject();
	}

	if (m_task)
	{
		m_task->stop();
	}
}

void PageDialog::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	IPage *page = m_pages.at(current.row());
	IPage *previousPage = previous.isValid() ? m_pages.at(previous.row()) : nullptr;
	if (m_currentPage == page)
	{
		return;
	}
	if (m_currentPage && m_currentPage == previousPage && m_currentPage->hasChanges(m_currentWidget))
	{
		int answer = askForDiscard();
		if (answer == QMessageBox::Cancel)
		{
			QMetaObject::invokeMethod(ui->pageList, "setCurrentIndex", Qt::QueuedConnection, Q_ARG(QModelIndex, previous));
		}
		else if (answer == QMessageBox::Apply)
		{
			m_currentPage->applyPage(m_currentWidget);
		}
	}
	setCurrentPage(page);
}

int PageDialog::askForDiscard()
{
	return QMessageBox::question(this, tr("Continue?"), tr("There are unsaved changes, what do you want to do with them?"),
								 QMessageBox::Discard | QMessageBox::Cancel | QMessageBox::Apply, QMessageBox::Apply);
}

void PageDialog::setCurrentPage(IPage *page)
{
	if (m_currentPage == page)
	{
		return;
	}
	if (m_currentWidget)
	{
		ui->containerLayout->takeAt(0);
		m_currentWidget->hide();
	}
	m_currentPage = page;
	if (!m_pageCache.contains(m_currentPage))
	{
		QWidget *widget = qobject_cast<QWidget *>(m_currentPage->createWidget());
		widget->setParent(this);
		m_pageCache.insert(m_currentPage, widget);
	}
	m_currentWidget = m_pageCache[m_currentPage];
	m_currentWidget->show();
	m_currentPage->initializePage(m_currentWidget);
	ui->containerLayout->addWidget(m_currentWidget);

	ui->headerLabel->setText(m_currentPage->name());
	g_icons->setForTarget(ui->iconLabel, m_currentPage->icon(), style()->pixelMetric(QStyle::PM_LargeIconSize, nullptr, this));
}



PagePartsPageWidget::PagePartsPageWidget(const QList<IPagePart *> &parts, QWidget *parent)
	: QWidget(parent)
{
	QVBoxLayout *layout = new QVBoxLayout(this);
	for (auto part : parts)
	{
		QWidget *widget = qobject_cast<QWidget *>(part->createWidget());
		widget->setParent(this);
		if (QGroupBox *box = qobject_cast<QGroupBox *>(widget))
		{
			box->setTitle(part->name());
		}
		layout->addWidget(widget);
		m_widgets.insert(part, widget);
	}
}
PagePartsPageWidget::~PagePartsPageWidget()
{
	qDeleteAll(m_widgets.keys());
}
void PagePartsPageWidget::init()
{
	for (auto it = m_widgets.constBegin(); it != m_widgets.constEnd(); ++it)
	{
		it.key()->initializePart(it.value());
	}
}
void PagePartsPageWidget::apply()
{
	for (auto it = m_widgets.constBegin(); it != m_widgets.constEnd(); ++it)
	{
		it.key()->applyPart(it.value());
	}
}
bool PagePartsPageWidget::hasChanges() const
{
	for (auto it = m_widgets.constBegin(); it != m_widgets.constEnd(); ++it)
	{
		if (it.value() && it.key()->hasChanges(it.value()))
		{
			return true;
		}
	}
	return false;
}

void PagePartsPage::initializePage(QObject *widget)
{
	qobject_cast<PagePartsPageWidget *>(widget)->init();
}
void PagePartsPage::applyPage(QObject *widget)
{
	qobject_cast<PagePartsPageWidget *>(widget)->apply();
}
bool PagePartsPage::hasChanges(QObject *widget) const
{
	return qobject_cast<PagePartsPageWidget *>(widget)->hasChanges();
}

void resolveParts(QList<IPage *> &pages, const QList<IPagePart *> &parts)
{
	QMap<QString, IPage *> mapped;
	QMap<QString, QList<IPagePart *>> partsMapped;
	for (auto page : pages)
	{
		mapped.insert(page->id(), page);
		partsMapped.insert(page->id(), QList<IPagePart *>());
	}
	for (auto part : parts)
	{
		partsMapped[part->pageId()].append(part);
	}
	for (const auto id : partsMapped.keys())
	{
		if (partsMapped[id].isEmpty() && dynamic_cast<PagePartsPage *>(mapped[id]))
		{
			pages.removeAll(mapped[id]);
		}
	}
	for (auto it = partsMapped.constBegin(); it != partsMapped.constEnd(); ++it)
	{
		if (!it.value().isEmpty())
		{
			dynamic_cast<PagePartsPage *>(mapped[it.key()])->setParts(it.value());
		}
	}
}

#include "PageDialog.moc"
