// Licensed under the Apache-2.0 license. See README.md for details.

#include "NewContainerWizard.h"

#include <QFormLayout>
#include <QLineEdit>
#include <QLabel>
#include <QRadioButton>
#include <QComboBox>
#include <QHBoxLayout>
#include <QRegularExpressionValidator>
#include <QProgressBar>
#include <QFileSystemModel>
#include <QCompleter>
#include <QTimer>

#include "core/tasks/Task.h"
#include "core/tasks/GroupedTask.h"
#include "core/tasks/FlowTask.h"
#include "core/util/FileSystem.h"
#include "core/ContainerTemplateManager.h"
#include "core/BaseVersion.h"
#include "core/ContainerModel.h"
#include "core/Container.h"

#include "gui/IconProxyModel.h"
#include "gui/widgets/ProgressWidget.h"
#include "gui/widgets/VersionSelector.h"
#include "gui/ContainerIconModel.h"

class ContainerNameValidator : public QValidator
{
	Q_OBJECT
public:
	explicit ContainerNameValidator(QWizard *wizard, QObject *parent = nullptr)
		: QValidator(parent), m_wizard(wizard), m_exp("[^A-Za-z0-9\\.\\- ]")
	{
	}

	void fixup(QString &string) const override
	{
		string.remove(m_exp);
	}
	State validate(QString &input, int &) const override
	{
		if (input.contains(m_exp))
		{
			return Invalid;
		}
		if (FS::exists(m_wizard->field("dir").toString() + '/' + input.toLower()))
		{
			return Intermediate;
		}
		return Acceptable;
	}

private:
	QWizard *m_wizard;
	QRegularExpression m_exp;
};
class ContainerDirValidator : public QValidator
{
	Q_OBJECT
public:
	explicit ContainerDirValidator(QWizard *wizard, QObject *parent = nullptr)
		: QValidator(parent), m_wizard(wizard)
	{
	}

	State validate(QString &input, int &) const override
	{
		if (FS::exists(input + '/' + m_wizard->field("name").toString().toLower()))
		{
			return Intermediate;
		}
		else
		{
			return Acceptable;
		}
	}

private:
	QWizard *m_wizard;
};

class StartPage : public QWizardPage
{
	Q_OBJECT
public:
	explicit StartPage(QWizard *wizard, QWidget *parent = nullptr)
		: QWizardPage(parent)
	{
		setTitle(tr("Start"));
		setSubTitle(tr("Specify basic information about the new container, and select it's root component"));
		setCommitPage(true);

		QFormLayout *layout = new QFormLayout;

		m_nameEdit = new QLineEdit(this);
		m_nameEdit->setValidator(new ContainerNameValidator(wizard, this));
		layout->addRow(tr("Name"), m_nameEdit);
		registerField("name*", m_nameEdit);
		connect(m_nameEdit, &QLineEdit::textChanged, this, &StartPage::resultDirChanged);

		QComboBox *groupSelector = new QComboBox(this);
		groupSelector->setEditable(true);
		layout->addRow(tr("Group"), groupSelector);
		registerField("group", groupSelector, "currentText", "currentTextChanged");

		QComboBox *iconSelector = new QComboBox(this);
		layout->addRow(tr("Icon"), iconSelector);
		registerField("icon", iconSelector, "currentText", "currentTextChanged");
		iconSelector->setModel(IconProxyModel::mixin(g_containerIcons->model()));

		m_dirEdit = new QLineEdit(this);
		m_dirEdit->setText(QDir::current().absoluteFilePath("containers"));
		m_dirEdit->setValidator(new ContainerDirValidator(wizard, this));
		m_dirEdit->setCompleter(new QCompleter(new QFileSystemModel(this), this));
		layout->addRow(tr("Parent Directory"), m_dirEdit);
		registerField("dir", m_dirEdit);
		connect(m_dirEdit, &QLineEdit::textChanged, this, &StartPage::resultDirChanged);

		m_resultDirLabel = new QLabel(this);
		m_resultDirLabel->setTextFormat(Qt::RichText);
		m_resultDirLabel->setWordWrap(true);
		layout->addRow(m_resultDirLabel);

		QComboBox *templateSelector = new QComboBox(this);
		layout->addRow(tr("Template"), templateSelector);
		templateSelector->setModel(IconProxyModel::mixin(g_containerTemplates->model()));
		connect(templateSelector, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &StartPage::templateIndexChanged);
		registerField("templateIndex", templateSelector);

		m_versionSelector = new VersionSelector(nullptr, this);
		layout->addRow(tr("Version"), m_versionSelector);
		registerField("version", m_versionSelector, "version", "versionChanged");
		connect(m_versionSelector, &VersionSelector::versionChanged, this, &StartPage::completeChanged);

		setLayout(layout);

		resultDirChanged();
		templateIndexChanged(templateSelector->currentIndex());
	}

	bool isComplete() const override
	{
		return m_versionSelector->version() && QWizardPage::isComplete();
	}

private slots:
	void templateIndexChanged(const int index)
	{
		BaseContainerTemplate *t = index >= 0 ? g_containerTemplates->get(index) : nullptr;
		if (t)
		{
			m_versionSelector->setVersionList(t->versionsList());
		}
		else
		{
			m_versionSelector->setVersionList(nullptr);
		}
	}

	void resultDirChanged()
	{
		m_resultDirLabel->setText(tr("The container will be saved in <i>%1%2%3</i>")
								  .arg(m_dirEdit->text(),
									   QDir::separator(),
									   m_nameEdit->text().toLower()));
	}

private:
	QLabel *m_resultDirLabel;
	QLineEdit *m_nameEdit;
	QLineEdit *m_dirEdit;
	VersionSelector *m_versionSelector;
};
class FinalPage : public QWizardPage
{
	Q_OBJECT
public:
	explicit FinalPage(QWidget *parent = nullptr)
		: QWizardPage(parent)
	{
		setTitle(tr("Installing..."));
		setSubTitle(tr("Please wait while your container is constructed..."));

		m_widget = new ProgressWidget(this);
		QVBoxLayout *l = new QVBoxLayout;
		l->addWidget(m_widget);
		setLayout(l);
	}

	void initializePage() override
	{
		const QDir dir = directory();
		try
		{
			FS::ensureExists(dir);
		}
		catch (Exception &e)
		{
			m_widget->findChild<QLabel *>()->setText(e.message());
			return;
		}

		BaseContainerTemplate *t = g_containerTemplates->get(field("templateIndex").toInt());

		m_container = t->createContainer(dir, field("version").value<QWeakPointer<BaseVersion>>().lock());
		m_container->setName(field("name").toString());
		m_container->setIconKey(field("icon").toString());
		g_containerModel->addContainer(m_container, field("group").toString());

		m_task = new FlowTask(FlowTask::Creation, m_container, this);
		connect(m_task, &Task::succeeded, this, &FinalPage::completeChanged);
		QTimer::singleShot(1, this, [this](){m_widget->start(m_task);});
	}

	void cleanupPage()
	{
		delete m_container;
		delete m_task;
		try
		{
			FS::remove(directory());
		}
		catch (...)
		{
		}
	}

	bool isComplete() const override
	{
		return m_task->isFinished() && m_task->isSuccess();
	}

private:
	Container *m_container = nullptr;
	FlowTask *m_task;
	ProgressWidget *m_widget;

	QDir directory() const
	{
		return field("dir").toString() + '/' + field("name").toString().toLower();
	}
};

NewContainerWizard::NewContainerWizard(QWidget *parent)
	: QWizard(parent)
{
	setButtonText(CommitButton, tr("Create"));
	addPage(new StartPage(this));
	addPage(new FinalPage);
}

bool NewContainerWizard::execNew()
{
	return exec() == Accepted;
}
bool NewContainerWizard::execDuplicate(Container *existing)
{
	return exec() == Accepted;
}

#include "NewContainerWizard.moc"
