// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QDialog>

class QTreeView;
class BaseVersionList;
class BaseVersion;
class ProgressWidget;
class HideVersionsProxyModel;

class VersionDialog : public QDialog
{
	Q_OBJECT
public:
	explicit VersionDialog(QWidget *parent = nullptr);

	void setList(BaseVersionList *list);

	void setCurrent(const QWeakPointer<BaseVersion> &version);
	QWeakPointer<BaseVersion> current() const;

private:
	BaseVersionList *m_list;
	QTreeView *m_view;
	ProgressWidget *m_progress;
	QPushButton *m_refreshBtn;
	QPushButton *m_cancelBtn;
	QPushButton *m_okBtn;
	HideVersionsProxyModel *m_proxy;

	QString m_lastCurrentId;

private slots:
	void refreshList();
	void selectionChanged();
	void doubleClicked(const QModelIndex &index);
};
