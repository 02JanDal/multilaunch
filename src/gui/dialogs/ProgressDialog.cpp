// Licensed under the Apache-2.0 license. See README.md for details.

#include "ProgressDialog.h"

#include "core/tasks/Task.h"

ProgressDialog::ProgressDialog(QWidget *parent)
	: QProgressDialog(parent)
{
	setCancelButton(nullptr);
	setMinimum(0);
	setMaximum(0);
}

bool ProgressDialog::exec(Task *task)
{
	if (m_task)
	{
		disconnect(m_task, 0, this, 0);
	}
	m_task = task;
	connect(m_task, &Task::finished, this, &ProgressDialog::handleTaskFinish);
	connect(m_task, &Task::statusChanged, this, &ProgressDialog::handleTaskStatus);
	connect(m_task, &Task::progressChanged, this, &ProgressDialog::handleTaskProgress);
	connect(m_task, &Task::destroyed, this, &ProgressDialog::taskDestroyed);
	if (!m_task->isStarted())
	{
		m_task->start();
	}
	return QProgressDialog::exec() == QDialog::Accepted;
}

void ProgressDialog::handleTaskFinish()
{
	if (m_task->isFailure())
	{
		setLabelText(m_task->errorString());
		reject();
	}
	else
	{
		accept();
	}
}
void ProgressDialog::handleTaskStatus()
{
	setLabelText(m_task->status());
}
void ProgressDialog::handleTaskProgress()
{
	if (m_task->progress() > 100)
	{
		setMaximum(0);
	}
	else
	{
		setMaximum(100);
		setValue(m_task->progress());
	}
}
void ProgressDialog::taskDestroyed()
{
	m_task = nullptr;
}
