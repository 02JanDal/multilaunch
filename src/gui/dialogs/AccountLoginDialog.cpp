// Licensed under the Apache-2.0 license. See README.md for details.

#include "AccountLoginDialog.h"
#include "ui_AccountLoginDialog.h"

#include "core/accounts/AccountModel.h"
#include "core/accounts/BaseAccountType.h"
#include "core/accounts/BaseAccount.h"
#include "core/tasks/Task.h"
#include "gui/IconRegistry.h"
#include "gui/IconProxyModel.h"

AccountLoginDialog::AccountLoginDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::AccountLoginDialog)
{
	setWindowTitle(tr("New Account"));
	ui->setupUi(this);
	ui->progressWidget->setVisible(false);
	ui->errorLbl->setVisible(false);
	ui->loginBtn->setFocus();
	ui->usernameEdit->setFocus();
	ui->typeBox->setModel(IconProxyModel::mixin(g_accounts->typesModel()));

	connect(ui->typeBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &AccountLoginDialog::currentTypeChanged);
	currentTypeChanged(0);
}
AccountLoginDialog::AccountLoginDialog(const QString &type, QWidget *parent)
	: AccountLoginDialog(parent)
{
	setWindowTitle(tr("New Account for %1").arg(g_accounts->type(type)->text()));
	ui->typeBox->setCurrentIndex(ui->typeBox->findData(type));
	ui->typeBox->setDisabled(true);

	setupForType(type);
}
AccountLoginDialog::AccountLoginDialog(BaseAccount *account, QWidget *parent)
	: AccountLoginDialog(account->type(), parent)
{
	setWindowTitle(tr("Login"));
	m_account = account;
	ui->usernameEdit->setText(account->loginUsername());
}

AccountLoginDialog::~AccountLoginDialog()
{
	delete ui;
}

void AccountLoginDialog::on_cancelBtn_clicked()
{
	reject();
}
void AccountLoginDialog::on_loginBtn_clicked()
{
	Task *task = m_account->createLoginTask(ui->usernameEdit->text(), ui->passwordEdit->text());
	connect(task, &Task::finished, this, &AccountLoginDialog::taskFinished);
	ui->typeBox->setEnabled(false);
	ui->errorLbl->setVisible(false);
	ui->usernameEdit->setEnabled(false);
	ui->passwordEdit->setEnabled(false);
	ui->loginBtn->setEnabled(false);
	ui->progressWidget->setVisible(true);
	ui->progressWidget->start(task);
}

void AccountLoginDialog::taskFinished()
{
	Task *task = qobject_cast<Task *>(sender());
	if (task->isSuccess())
	{
		accept();
	}
	else
	{
		ui->typeBox->setEnabled(true);
		ui->errorLbl->setVisible(true);
		ui->errorLbl->setText(task->errorString());
		ui->usernameEdit->setEnabled(true);
		ui->passwordEdit->setEnabled(true);
		ui->loginBtn->setEnabled(true);
		ui->progressWidget->setVisible(false);
	}
}

void AccountLoginDialog::currentTypeChanged(const int index)
{
	const QString type = ui->typeBox->itemData(index).toString();
	if (!type.isEmpty())
	{
		setupForType(type);
	}
}
void AccountLoginDialog::setupForType(const QString &type)
{
	const BaseAccountType *t = g_accounts->type(type);
	ui->usernameLbl->setText(t->usernameText());
	ui->usernameLbl->setVisible(!t->usernameText().isNull());
	ui->usernameEdit->setVisible(!t->usernameText().isNull());
	ui->passwordLbl->setText(t->passwordText());
	ui->passwordLbl->setVisible(!t->passwordText().isNull());
	ui->passwordEdit->setVisible(!t->passwordText().isNull());
	if (t->type() == BaseAccountType::OAuth2Pin)
	{
		ui->infoLbl->setVisible(true);
		ui->infoLbl->setText(tr("To authorize, go to <a href=\"%1\">%1</a> and follow the instructions. You will receive a PIN code which you need to enter below.")
							 .arg(t->oauth2PinUrl().toString()));
	}
	else
	{
		ui->infoLbl->setVisible(false);
	}
	if (m_account && m_account->type() != type)
	{
		delete m_account;
		m_account = nullptr;
	}
	if (!m_account)
	{
		m_account = g_accounts->type(type)->createAccount();
	}
}
