// Licensed under the Apache-2.0 license. See README.md for details.

#include "ChooseContainerIconDialog.h"
#include "ui_ChooseContainerIconDialog.h"

#include "gui/ContainerIconModel.h"
#include "gui/IconProxyModel.h"

ChooseContainerIconDialog::ChooseContainerIconDialog(const QString &current, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ChooseContainerIconDialog)
{
	ui->setupUi(this);
	ui->list->setModel(m_proxy = IconProxyModel::mixin(g_containerIcons->model()));
	setWindowTitle(tr("Choose an icon"));

	for (int i = 0; i < g_containerIcons->size(); ++i)
	{
		if (g_containerIcons->get(i).key == current)
		{
			ui->list->setCurrentIndex(m_proxy->mapFromSource(g_containerIcons->model()->index(i, 0)));
			break;
		}
	}

	connect(ui->list->selectionModel(), &QItemSelectionModel::currentChanged, this, [this](const QModelIndex &index)
	{
		ui->useBtn->setEnabled(index.isValid());
	});
	ui->useBtn->setEnabled(ui->list->currentIndex().isValid());
}

ChooseContainerIconDialog::~ChooseContainerIconDialog()
{
	delete ui;
}

QString ChooseContainerIconDialog::iconKey() const
{
	return g_containerIcons->get(ui->list->currentIndex().row()).key;
}
