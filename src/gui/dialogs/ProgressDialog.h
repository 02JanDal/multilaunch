// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QProgressDialog>

class Task;

class ProgressDialog : public QProgressDialog
{
	Q_OBJECT
public:
	explicit ProgressDialog(QWidget *parent = nullptr);

	bool exec(Task *task);

private slots:
	void handleTaskFinish();
	void handleTaskStatus();
	void handleTaskProgress();
	void taskDestroyed();

private:
	using QProgressDialog::exec;
	Task *m_task = nullptr;
};
