// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QDialog>
#include <QPointer>

#include "core/IPage.h"

namespace Ui
{
class PageDialog;
}

class FlowTask;

class PageDialog : public QDialog
{
	Q_OBJECT
public:
	explicit PageDialog(const QList<IPage *> &pages, FlowTask *runTask, QWidget *parent = nullptr);
	~PageDialog();

protected:
	void closeEvent(QCloseEvent *event) override;

private slots:
	void okClicked();
	void cancelClicked();
	void applyClicked();
	void currentChanged(const QModelIndex &current, const QModelIndex &previous);

private:
	Ui::PageDialog *ui;
	QList<IPage *> m_pages;
	FlowTask *m_task;
	IPage *m_currentPage = nullptr;
	QWidget *m_currentWidget = nullptr;
	QHash<IPage *, QWidget *> m_pageCache;

	int askForDiscard();
	void setCurrentPage(IPage *page);
};

class PagePartsPageWidget : public QWidget
{
	Q_OBJECT
public:
	explicit PagePartsPageWidget(const QList<IPagePart *> &parts, QWidget *parent = nullptr);
	~PagePartsPageWidget();

	void init();
	void apply();
	bool hasChanges() const;

private:
	QHash<IPagePart *, QPointer<QWidget>> m_widgets;
};

class PagePartsPage : public IPage
{
public:
	void setParts(const QList<IPagePart *> &parts) { m_parts = parts; }

	QObject *createWidget() const { return new PagePartsPageWidget(m_parts); }
	void initializePage(QObject *widget);
	void applyPage(QObject *widget);
	bool hasChanges(QObject *widget) const;

private:
	QList<IPagePart *> m_parts;
};

void resolveParts(QList<IPage *> &pages, const QList<IPagePart *> &parts);
