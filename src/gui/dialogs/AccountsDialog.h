// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QDialog>

namespace Ui {
class AccountsDialog;
}

class Container;
class BaseAccount;

class AccountsDialog : public QDialog
{
	Q_OBJECT
public:
	explicit AccountsDialog(Container *container = nullptr, QWidget *parent = nullptr);
	~AccountsDialog();

	void setRequestedAccountType(const QString &type);

	BaseAccount *account() const;

private slots:
	void on_addBtn_clicked();
	void on_removeBtn_clicked();
	void on_containerDefaultBtn_clicked();
	void on_globalDefaultBtn_clicked();
	void on_useBtn_clicked();
	void currentChanged(const QModelIndex &current, const QModelIndex &previous);

private:
	Ui::AccountsDialog *ui;
	Container *m_container = nullptr;
	QString m_requestedType;
};
