// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QWizard>

class Container;

class NewContainerWizard : public QWizard
{
	Q_OBJECT
public:
	explicit NewContainerWizard(QWidget *parent = nullptr);

	bool execNew();
	bool execDuplicate(Container *existing);

private:
	using QWizard::exec;
};
