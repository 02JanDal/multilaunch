// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QDialog>

namespace Ui {
class ChooseContainerIconDialog;
}

class IconProxyModel;

class ChooseContainerIconDialog : public QDialog
{
	Q_OBJECT

public:
	explicit ChooseContainerIconDialog(const QString &current, QWidget *parent = 0);
	~ChooseContainerIconDialog();

	QString iconKey() const;

private:
	Ui::ChooseContainerIconDialog *ui;
	IconProxyModel *m_proxy;
};
