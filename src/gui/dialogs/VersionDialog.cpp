// Licensed under the Apache-2.0 license. See README.md for details.

#include "VersionDialog.h"

#include <QTreeView>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSortFilterProxyModel>

#include "core/tasks/Task.h"
#include "core/BaseVersionList.h"
#include "core/BaseVersion.h"
#include "gui/widgets/ProgressWidget.h"

class HideVersionsProxyModel : public QSortFilterProxyModel
{
	Q_OBJECT
public:
	explicit HideVersionsProxyModel(BaseVersionList *list, QObject *parent = nullptr)
		: QSortFilterProxyModel(parent), m_list(list)
	{
		setSourceModel(m_list);
	}

protected:
	bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override
	{
		return m_list->shouldShow(source_row, nullptr);
	}

private:
	BaseVersionList *m_list;
};

VersionDialog::VersionDialog(QWidget *parent)
	: QDialog(parent)
{
	m_view = new QTreeView(this);
	m_view->setSelectionBehavior(QTreeView::SelectRows);
	m_view->setSelectionMode(QTreeView::SingleSelection);
	connect(m_view, &QTreeView::doubleClicked, this, &VersionDialog::doubleClicked);

	m_progress = new ProgressWidget(this);
	m_progress->setVisible(false);

	m_refreshBtn = new QPushButton(tr("Refresh"), this);
	connect(m_refreshBtn, &QPushButton::clicked, this, &VersionDialog::refreshList);

	m_cancelBtn = new QPushButton(tr("Cancel"), this);
	connect(m_cancelBtn, &QPushButton::clicked, this, &VersionDialog::reject);

	m_okBtn = new QPushButton(tr("Ok"), this);
	m_okBtn->setEnabled(false);
	connect(m_okBtn, &QPushButton::clicked, this, &VersionDialog::accept);

	QHBoxLayout *btnsLayout = new QHBoxLayout;
	btnsLayout->addWidget(m_refreshBtn);
	btnsLayout->addStretch();
	btnsLayout->addWidget(m_cancelBtn);
	btnsLayout->addWidget(m_okBtn);

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(m_view);
	layout->addWidget(m_progress);
	layout->addLayout(btnsLayout);
}

void VersionDialog::setList(BaseVersionList *list)
{
	if (m_view->selectionModel())
	{
		m_view->selectionModel()->clear();
	}
	m_list = list;
	m_view->setModel(m_proxy = new HideVersionsProxyModel(m_list, this));
	connect(m_view->selectionModel(), &QItemSelectionModel::selectionChanged, this, &VersionDialog::selectionChanged);
	if (m_list->size() == 0)
	{
		refreshList();
	}
}

void VersionDialog::setCurrent(const QWeakPointer<BaseVersion> &version)
{
	Q_ASSERT(m_list);
	for (int i = 0; i < m_list->size(); ++i)
	{
		if (m_list->at(i) == version)
		{
			m_view->selectionModel()->select(m_list->index(i, 0), QItemSelectionModel::SelectCurrent);
			break;
		}
	}
}
QWeakPointer<BaseVersion> VersionDialog::current() const
{
	if (m_view->currentIndex().isValid())
	{
		return m_list->at(m_proxy->mapToSource(m_view->currentIndex()).row());
	}
	else
	{
		return QWeakPointer<BaseVersion>();
	}
}

void VersionDialog::refreshList()
{
	if (!m_list)
	{
		return;
	}
	m_refreshBtn->setEnabled(false);
	m_okBtn->setEnabled(false);
	m_view->setEnabled(false);
	Task *task = m_list->createRefreshTask();
	connect(task, &Task::finished, [this, task]()
	{
		m_progress->setVisible(false);
		m_refreshBtn->setEnabled(true);
		m_view->setEnabled(true);
		selectionChanged();
		task->deleteLater();
		if (!m_lastCurrentId.isEmpty())
		{
			setCurrent(m_list->findVersion(Version::fromString(m_lastCurrentId)));
		}
	});
	m_progress->setVisible(true);
	m_progress->start(task);
}

void VersionDialog::selectionChanged()
{
	m_okBtn->setEnabled(m_view->selectionModel()->hasSelection());
	const QSharedPointer<BaseVersion> ptr = current().lock();
	if (ptr && ptr->id().isValid())
	{
		m_lastCurrentId = ptr->id().toString();
	}
}

void VersionDialog::doubleClicked(const QModelIndex &index)
{
	if (index.isValid())
	{
		m_view->setCurrentIndex(index);
		accept();
	}
}

#include "VersionDialog.moc"
