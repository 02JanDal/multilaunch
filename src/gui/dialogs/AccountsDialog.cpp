// Licensed under the Apache-2.0 license. See README.md for details.

#include "AccountsDialog.h"
#include "ui_AccountsDialog.h"

#include "core/Container.h"
#include "core/accounts/BaseAccount.h"
#include "core/accounts/AccountModel.h"
#include "core/accounts/BaseAccountType.h"
#include "core/tasks/Task.h"
#include "gui/dialogs/ProgressDialog.h"
#include "gui/dialogs/AccountLoginDialog.h"
#include "gui/IconRegistry.h"
#include "gui/IconProxyModel.h"

AccountsDialog::AccountsDialog(Container *container, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::AccountsDialog),
	m_container(container)
{
	ui->setupUi(this);

	ui->containerDefaultBtn->setVisible(!!container);
	ui->useBtn->setVisible(!!container);
	ui->progressWidget->setVisible(false);
	ui->cancelBtn->setText(container ? tr("Cancel") : tr("Close"));

	ui->view->setModel(IconProxyModel::mixin(g_accounts));
	connect(ui->view->selectionModel(), &QItemSelectionModel::currentChanged, this, &AccountsDialog::currentChanged);
	currentChanged(ui->view->currentIndex(), QModelIndex());

	connect(ui->cancelBtn, &QPushButton::clicked, this, &AccountsDialog::reject);
}

AccountsDialog::~AccountsDialog()
{
	delete ui;
}

void AccountsDialog::setRequestedAccountType(const QString &type)
{
	m_requestedType = type;
	ui->useBtn->setVisible(true);
	currentChanged(ui->view->currentIndex(), QModelIndex());
}
BaseAccount *AccountsDialog::account() const
{
	return g_accounts->get(ui->view->currentIndex());
}

void AccountsDialog::on_addBtn_clicked()
{
	if (m_requestedType.isEmpty())
	{
		AccountLoginDialog dlg(this);
		if (dlg.exec() == QDialog::Accepted)
		{
			g_accounts->registerAccount(dlg.account());
		}
	}
	else
	{
		AccountLoginDialog dlg(m_requestedType, this);
		if (dlg.exec() == QDialog::Accepted)
		{
			g_accounts->registerAccount(dlg.account());
		}
	}

}
void AccountsDialog::on_removeBtn_clicked()
{
	BaseAccount *account = g_accounts->get(ui->view->currentIndex());
	if (account)
	{
		Task *task = account->createLogoutTask();
		if (task)
		{
			ProgressDialog(this).exec(task);
		}
		g_accounts->unregisterAccount(account);
	}
}
void AccountsDialog::on_containerDefaultBtn_clicked()
{
	BaseAccount *account = g_accounts->get(ui->view->currentIndex());
	if (account)
	{
		g_accounts->setContainerDefault(m_container, account);
	}
}
void AccountsDialog::on_globalDefaultBtn_clicked()
{
	BaseAccount *account = g_accounts->get(ui->view->currentIndex());
	if (account)
	{
		g_accounts->setGlobalDefault(account);
	}
}

void AccountsDialog::on_useBtn_clicked()
{
	BaseAccount *account = g_accounts->get(ui->view->currentIndex());
	if (account)
	{
		ui->groupBox->setEnabled(false);
		ui->useBtn->setEnabled(false);
		ui->view->setEnabled(false);
		ui->progressWidget->setVisible(true);
		Task *task = account->createCheckTask();
		ui->progressWidget->exec(task);
		ui->progressWidget->setVisible(false);

		if (task->isSuccess())
		{
			accept();
		}
		else
		{
			AccountLoginDialog dlg(account, this);
			if (dlg.exec() == AccountLoginDialog::Accepted)
			{
				accept();
			}
			else
			{
				ui->groupBox->setEnabled(true);
				ui->useBtn->setEnabled(true);
				ui->view->setEnabled(true);
			}
		}
	}
}

void AccountsDialog::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
	if (!current.isValid())
	{
		ui->groupBox->setEnabled(false);
		ui->avatarLbl->setPixmap(QPixmap());
		ui->usernameLbl->setText("");
		ui->containerDefaultBtn->setText(tr("Default for %1 (%2)").arg("-", m_container ? m_container->name() : QString()));
		ui->globalDefaultBtn->setText(tr("Default for %1").arg("-"));
		ui->useBtn->setEnabled(false);
	}
	else
	{
		BaseAccount *account = g_accounts->get(current);
		ui->groupBox->setEnabled(true);
		g_icons->setForTarget(ui->avatarLbl, account->bigAvatar());
		ui->usernameLbl->setText(account->username());
		ui->containerDefaultBtn->setText(tr("Default for %1 (%2)").arg(g_accounts->type(account->type())->text(), m_container ? m_container->name() : QString()));
		ui->globalDefaultBtn->setText(tr("Default for %1").arg(g_accounts->type(account->type())->text()));
		ui->useBtn->setEnabled(m_requestedType == account->type());
	}
}
