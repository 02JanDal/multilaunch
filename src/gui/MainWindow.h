// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QMainWindow>

class QTreeView;
class ContainerModel;

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private slots:
	void on_actionNewContainer_triggered();
	void on_actionCopyContainer_triggered();
	void on_actionChangeIcon_triggered();
	void on_actionChangeGroup_triggered();
	void on_actionRename_triggered();
	void on_actionRemove_triggered();
	void on_actionContainerSettings_triggered();
	void on_actionLaunch_triggered();
	void on_actionAccounts_triggered();
	void on_actionSettings_triggered();
	void selectionChanged(const QModelIndex &current, const QModelIndex &previous);
	void containerIconChanged();

private:
	Ui::MainWindow *ui;
	QTreeView *m_containerView;
	ContainerModel * m_containerModel;
};
