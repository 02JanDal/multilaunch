// Licensed under the Apache-2.0 license. See README.md for details.

#include "FactorioPackageHandler.h"

#include "core/Container.h"
#include "factorio/components/FactorioComponent.h"
#include "FactorioVersionList.h"

Task *FactorioPackageHandler::createFetchInformationTask(const Package &package) const
{
	if (package == Package("factorio", "factorio"))
	{
		return g_factorioVersions->createRefreshTask();
	}
	else
	{
		return nullptr;
	}
}
BaseVersionList *FactorioPackageHandler::getVersionsList(const Package &package) const
{
	if (package == Package("factorio", "factorio"))
	{
		return g_factorioVersions;
	}
	else
	{
		return nullptr;
	}
}
BaseComponent *FactorioPackageHandler::createComponent(const Package &package, Container *container) const
{
	if (package == Package("factorio", "factorio"))
	{
		return new FactorioComponent(container);
	}
	else
	{
		return nullptr;
	}
}
QList<PackageQuery> FactorioPackageHandler::getPackageDependencies(const Package &package, const Version &version) const
{
	if (package == Package("factorio", "factorio"))
	{
		return QList<PackageQuery>() << PackageQuery(Package("core.folder", "factorio/mods"));
	}
	else
	{
		return QList<PackageQuery>();
	}
}
