// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/accounts/BaseAccount.h"
#include "core/accounts/BaseAccountType.h"

class FactorioAccount : public BaseAccount
{
public:
	explicit FactorioAccount();

	QString type() const override { return "factorio"; }

	Task *createLoginTask(const QString &username, const QString &password) override;
	Task *createCheckTask() override;
	Task *createLogoutTask() override;
};

class FactorioAccountType : public BaseAccountType
{
public:
	QString id() const override { return "factorio"; }
	QString text() const override;
	QString icon() const override { return "factorio"; }
	QString usernameText() const override;
	QString passwordText() const override;
	Type type() const override { return UsernamePassword; }
	BaseAccount *createAccount() override { return new FactorioAccount; }
};
