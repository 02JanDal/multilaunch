// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QUrl>

#include "core/BaseVersionList.h"
#include "core/BaseVersion.h"
#include "core/util/Singleton.h"

class FactorioVersion : public BaseVersion
{
public:
	explicit FactorioVersion(const Version &id, const QString &type, const QString &platform, const QUrl &url)
		: m_id(id), m_type(type), m_platform(platform), m_url(url) {}
	explicit FactorioVersion() {}

	Version id() const override { return m_id; }
	QString type() const override { return m_type; }
	QString platform() const { return m_platform; }
	QUrl url() const { return m_url; }

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

	bool operator==(const QSharedPointer<BaseVersion> &other) const override;

private:
	Version m_id;
	QString m_type;
	QString m_platform;
	QUrl m_url;
};

class FactorioVersionList : public BaseVersionList
{
	Q_OBJECT
public:
	explicit FactorioVersionList(QObject *parent = nullptr);

	bool shouldShow(const int index, Container *container) const override;

	Task *createRefreshTask();

	static const QString currentPlatform();

private:
	friend class FactorioVersionsTask;
};
DECLARE_SINGLETON(FactorioVersionList, g_factorioVersions)
