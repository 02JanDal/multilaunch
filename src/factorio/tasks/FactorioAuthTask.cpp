// Licensed under the Apache-2.0 license. See README.md for details.

#include "FactorioAuthTask.h"

#include <QNetworkReply>

#include "core/util/Json.h"
#include "factorio/FactorioAccount.h"
#include "core/network/RawDownloadTask.h"

FactorioAuthenticateTask::FactorioAuthenticateTask(const QString &username, const QString &password, FactorioAccount *account, QObject *parent)
	: StandardTask(parent), m_username(username), m_password(password), m_account(account)
{
}
void FactorioAuthenticateTask::run()
{
	using namespace Json;

	// "API" auth
	{
		const auto doc = ensureDocument(networkPost(
											QUrl("https://www.factorio.com/updater/get-token?username=" + m_username + "&apiVersion=2"),
											"password=" + m_password.toLatin1(), false));
		if (doc.isArray())
		{
			const QJsonArray array = ensureArray(doc);
			if (!array.isEmpty())
			{
				m_account->setToken("token", ensureIsType<QString>(array.first()));
				m_account->setUsername(m_username);
			}
			else
			{
				setFailure(tr("Invalid response"));
			}
		}
		else
		{
			setFailure(ensureIsType<QString>(ensureObject(doc), "message"));
		}
	}

	// web auth
	{
		RawDownloadTask *task = new RawDownloadTask(QUrl("https://www.factorio.com/login"), "application/x-www-form-urlencoded");
		task->setData("username-or-email=" + m_username.toLatin1() + "&password=" + m_password.toLatin1());
		try
		{
			runTask(task);
		}
		catch (...)
		{
			if (task->reply()->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() != 302)
			{
				throw Exception("Error authenticating with Factorio Web: Unknown error");
			}
		}
		m_account->setToken("webCookies", QString::fromLatin1(task->reply()->rawHeader("Set-Cookie")));
	}

	setSuccess();
}

FactorioValidateTask::FactorioValidateTask(FactorioAccount *account, QObject *parent)
	: StandardTask(parent), m_account(account)
{
}
void FactorioValidateTask::run()
{
	using namespace Json;
	const auto doc = ensureDocument(networkGet(QUrl("https://www.factorio.com/updater/get-available-versions?username=" + m_account->username() +
													"&token=" + m_account->token("token") + "&apiVersion=2")));
	const QJsonObject obj = ensureObject(doc);
	if (obj.contains("message"))
	{
		setFailure(ensureIsType<QString>(obj, "message"));
	}
	else
	{
		setSuccess();
	}
}
