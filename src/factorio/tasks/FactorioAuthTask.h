// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/tasks/StandardTask.h"

class FactorioAccount;

class FactorioAuthenticateTask : public StandardTask
{
	Q_OBJECT
public:
	explicit FactorioAuthenticateTask(const QString &username, const QString &password, FactorioAccount *account, QObject *parent = nullptr);

private:
	void run() override;

	QString m_username;
	QString m_password;
	FactorioAccount *m_account;
};
class FactorioValidateTask : public StandardTask
{
	Q_OBJECT
public:
	explicit FactorioValidateTask(FactorioAccount *account, QObject *parent = nullptr);

private:
	void run() override;

	FactorioAccount *m_account;
};
