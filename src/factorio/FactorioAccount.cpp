// Licensed under the Apache-2.0 license. See README.md for details.

#include "FactorioAccount.h"

#include "factorio/tasks/FactorioAuthTask.h"

FactorioAccount::FactorioAccount()
	: BaseAccount()
{
}

Task *FactorioAccount::createLoginTask(const QString &username, const QString &password)
{
	return new FactorioAuthenticateTask(username, password, this);
}
Task *FactorioAccount::createCheckTask()
{
	return new FactorioValidateTask(this);
}
Task *FactorioAccount::createLogoutTask()
{
	return nullptr;
}

QString FactorioAccountType::text() const
{
	return QObject::tr("Factorio");
}
QString FactorioAccountType::usernameText() const
{
	return QObject::tr("Username");
}
QString FactorioAccountType::passwordText() const
{
	return QObject::tr("Password");
}
