// Licensed under the Apache-2.0 license. See README.md for details.

#include "FactorioPlugin.h"

#include "core/components/ComponentRegistry.h"
#include "core/accounts/AccountModel.h"
#include "core/ContainerTemplateManager.h"
#include "core/Container.h"

#include "factorio/components/FactorioComponent.h"
#include "factorio/FactorioAccount.h"
#include "factorio/FactorioVersionList.h"
#include "factorio/FactorioPackageHandler.h"

class FactorioTemplate : public BaseContainerTemplate
{
public:
	QString name() const override { return QObject::tr("Factorio"); }
	QString icon() const override { return "factorio"; }
	Container *createContainer(const QDir &root, const QSharedPointer<BaseVersion> &version) const override
	{
		FactorioComponent *component = new FactorioComponent(nullptr);
		component->setVersion(version);
		return new Container(QUuid::createUuid(), root, component);
	}
	BaseVersionList *versionsList() const override { return g_factorioVersions; }
};

void FactorioPlugin::init()
{
	g_componentRegistry->registerComponent<FactorioComponent>("Factorio.Factorio");
	g_accounts->registerType(new FactorioAccountType);
	g_containerTemplates->registerTemplate(new FactorioTemplate);
	g_packages->registerHandler(QStringList() << "factorio", new FactorioPackageHandler);
}
QList<IPage *> FactorioPlugin::createGlobalPages() const
{
	return QList<IPage *>();
}
QList<IPagePart *> FactorioPlugin::createGlobalPageParts() const
{
	return QList<IPagePart *>();
}
