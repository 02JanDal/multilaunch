// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseComponent.h"

#include <QDir>

class FactorioComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit FactorioComponent(BaseComponent *parent);

	QString id() const override { return "Factorio.Factorio"; }
	QString name() const override { return tr("Factorio"); }
	QString type() const override { return tr("Factorio"); }
	bool canBeRoot() const override { return true; }

	Task *createTask(const Stage stage, IStageData *data = nullptr) override;
	IStageData *createStageData(const Stage stage) const override;

	QSharedPointer<BaseVersion> versionObject() const override;
	void setVersion(const QSharedPointer<BaseVersion> &version) override;

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

	QString cacheId() const;
	QString findExecutable(const QString &dir = QString()) const;
	QPair<QString, QString> getInfoFromExecutable() const;

private:
	QString m_versionPlatform;

	void parentChangeNotification() override;
};
