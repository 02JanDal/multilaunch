// Licensed under the Apache-2.0 license. See README.md for details.

#include "FactorioComponent.h"

#include <QProcess>
#include <QRegularExpression>
#include <KTar>
#include <KZip>

#include "factorio/FactorioVersionList.h"
#include "core/network/CachedDownloadTask.h"
#include "core/tasks/StandardTask.h"
#include "core/tasks/LambdaTask.h"
#include "core/tasks/AuthTask.h"
#include "core/tasks/LambdaTask.h"
#include "core/tasks/ExtractionTask.h"
#include "core/tasks/CopyTask.h"
#include "core/accounts/BaseAccount.h"
#include "core/util/FileSystem.h"
#include "core/util/Json.h"
#include "core/Container.h"
#include "core/StandardProcess.h"

class FactorioDownloadTask : public StandardTask
{
	Q_OBJECT
public:
	explicit FactorioDownloadTask(FactorioComponent *component)
		: StandardTask(nullptr), m_component(component) {}

protected:
	void run() override
	{
		if (m_component->findExecutable().isNull())
		{
			AuthTask *authTask = new AuthTask("factorio", m_component->container(), nullptr, nullptr);
			runTask(authTask);

			const QSharedPointer<FactorioVersion> version = m_component->versionObject().dynamicCast<FactorioVersion>();
			if (!version)
			{
				throw Exception("Invalid version, you might need to re-create this container");
			}
			QUrl url = version->url();
#ifdef Q_OS_WIN
			url = url.toString() + "-manual";
#endif
			CachedDownloadTask *task = new CachedDownloadTask("Factorio download", "factorio/downloads", m_component->cacheId(), url);
			task->setHeaders({{"Cookie", authTask->account()->token("webCookies")}});
			runTask(task);
			setSuccess();
		}
		else
		{
			setSuccess();
		}
	}

private:
	FactorioComponent *m_component;
};
class FactorioInstallTask : public StandardTask
{
	Q_OBJECT
public:
	explicit FactorioInstallTask(FactorioComponent *component)
		: StandardTask(nullptr), m_component(component) {}

protected:
	void run() override
	{
		if (m_component->findExecutable().isNull())
		{
			setStatus(tr("Extracting Factorio..."));
			const QString filename = "factorio/downloads/" + m_component->cacheId();
#if defined(Q_OS_LINUX) || defined(Q_OS_WIN)
			runTask(new ExtractionTask(filename, m_component->container()->rootDir().absolutePath()));
#else
			runTask(new CopyTask(filename, m_component->container()->rootDir()));
			FS::copy(filename, m_component->container()->rootDir().absoluteFilePath(QFileInfo(filename).fileName()));
#endif
			setSuccess();
		}
		else
		{
			setSuccess();
		}
	}

private:
	FactorioComponent *m_component;
};

FactorioComponent::FactorioComponent(BaseComponent *parent)
	: BaseComponent(parent)
{
	setPackage(Package("factorio", "factorio"));
}

QSharedPointer<BaseVersion> FactorioComponent::versionObject() const
{
	for (int i = 0; i < g_factorioVersions->size(); ++i)
	{
		const QSharedPointer<FactorioVersion> ver = g_factorioVersions->at(i).dynamicCast<FactorioVersion>();
		if (ver->id() == version() && ver->platform() == m_versionPlatform)
		{
			return ver;
		}
	}
	return QSharedPointer<BaseVersion>();
}

void FactorioComponent::setVersion(const QSharedPointer<BaseVersion> &version)
{
	setVersionInternal(version->id());
	m_versionPlatform = version.dynamicCast<FactorioVersion>()->platform();
}

void FactorioComponent::load(const QJsonObject &obj)
{
	BaseComponent::load(obj);
	m_versionPlatform = Json::ensureIsType<QString>(obj, "versionPlatform");
}
QJsonObject FactorioComponent::save() const
{
	QJsonObject obj = BaseComponent::save();
	obj.insert("versionPlatform", m_versionPlatform);
	return obj;
}

QString FactorioComponent::cacheId() const
{
	const QSharedPointer<FactorioVersion> version = versionObject().dynamicCast<FactorioVersion>();
	if (!version)
	{
		throw Exception("Invalid version, you might need to re-create this container");
	}
	static const QString extension =
#if defined(Q_OS_LINUX)
			".tar";
#elif defined(Q_OS_WIN)
			".zip";
#elif defined(Q_OS_OSX)
			".dmg";
#endif
	return version->platform() + "/" + version->id().toString() + extension;
}

QPair<QString, QString> FactorioComponent::getInfoFromExecutable() const
{
	const QString program = findExecutable();
	if (program.isNull())
	{
		return qMakePair(QString(), QString());
	}
	QProcess proc;
	proc.setProgram(program);
	proc.setArguments(QStringList() << "--version");
	proc.start();
	proc.waitForStarted(5000);
	proc.waitForFinished(5000);
	QRegularExpression exp("^Version: (?<version>[\\d\\.]*) \\(Build \\d*, (?<platform>[^\\)]*)\\)");
	QRegularExpressionMatch match = exp.match(QString::fromUtf8(proc.readAll()));
	return qMakePair(match.captured("version"), match.captured("platform"));
}
QString FactorioComponent::findExecutable(const QString &dir) const
{
	if (dir.isNull())
	{
		if (!container())
		{
			return QString();
		}
		return findExecutable(container()->rootDir().absoluteFilePath("factorio/bin"));
	}
	for (const QFileInfo info : QDir(dir).entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files))
	{
		if (info.isFile() && info.fileName() ==
				#if defined(Q_OS_LINUX)
				"factorio"
		#elif defined(Q_OS_WIN)
				"Factorio.exe"
		#endif
				)
		{
			return info.absoluteFilePath();
		}
		else if (info.isDir())
		{
			const QString ret = findExecutable(info.absoluteFilePath());
			if (!ret.isNull())
			{
				return ret;
			}
		}
	}
	return QString();
}

void FactorioComponent::parentChangeNotification()
{
	if (!versionObject())
	{
		setVersionInternal(Version::fromString(getInfoFromExecutable().first));
	}
}

Task *FactorioComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::Download)
	{
		return new FactorioDownloadTask(this);
	}
	else if (stage == Stage::Installation)
	{
		return new FactorioInstallTask(this);
	}
	else if (stage == Stage::Launch)
	{
		return new LambdaTask<std::function<void()>>([this, data]()
		{
			StandardLaunchData *d = dynamic_cast<StandardLaunchData *>(data);
			d->process = new StandardProcess(d);
			d->workingDirectory = container()->rootDir().absoluteFilePath("factorio");
			d->executable = findExecutable();
			connect(d->process, &BaseProcess::stateChanged, this, &FactorioComponent::parentChangeNotification);
		});
	}
	else
	{
		return nullptr;
	}
}

IStageData *FactorioComponent::createStageData(const Stage stage) const
{
	if (stage == Stage::Launch)
	{
		return new StandardLaunchData;
	}
	else
	{
		return BaseComponent::createStageData(stage);
	}
}

#include "FactorioComponent.moc"
