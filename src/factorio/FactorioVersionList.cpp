// Licensed under the Apache-2.0 license. See README.md for details.

#include "FactorioVersionList.h"

#include <QSysInfo>
#include <QRegularExpression>

#include "core/network/CachedDownloadTask.h"
#include "core/util/Json.h"
#include "core/tasks/AuthTask.h"
#include "core/tasks/StandardTask.h"
#include "core/accounts/BaseAccount.h"

DEFINE_SINGLETON(g_factorioVersions)

void FactorioVersion::load(const QJsonObject &obj)
{
	using namespace Json;
	m_id = Version::fromString(ensureIsType<QString>(obj, "id"));
	m_type = ensureIsType<QString>(obj, "type");
	m_platform = ensureIsType<QString>(obj, "platform");
	m_url = ensureIsType<QUrl>(obj, "url");
}
QJsonObject FactorioVersion::save() const
{
	using namespace Json;
	QJsonObject obj;
	obj.insert("id", m_id.toString());
	obj.insert("type", m_type);
	obj.insert("platform", m_platform);
	obj.insert("url", toJson(m_url));
	return obj;
}

bool FactorioVersion::operator==(const QSharedPointer<BaseVersion> &other) const
{
	const QSharedPointer<FactorioVersion> version = other.dynamicCast<FactorioVersion>();
	if (version)
	{
		return m_id == version->m_id && m_type == version->m_type && m_platform == version->m_platform;
	}
	else
	{
		return false;
	}
}

class FactorioVersionsTask : public StandardTask
{
	Q_OBJECT
public:
	explicit FactorioVersionsTask(QObject *parent = nullptr)
		: StandardTask(parent) {}

protected:
	void run() override
	{
		// TODO fetch demos if we don't have a valid account

		AuthTask *authTask = new AuthTask("factorio", nullptr, nullptr, this);
		runTask(authTask);
		Q_ASSERT(authTask->account());
		BaseAccount *account = authTask->account();
		if (!account->hasToken("webCookies"))
		{
			throw Exception("We're out of cookies :(");
		}

		setStatus(tr("Fetching lists..."));
		QMap<QString, QString> headers = {{"Cookie", account->token("webCookies")}};
		const QByteArray stable = networkGetCached("Factorio versions (stable)", "factorio", "versions.stable.html",
												   QUrl("https://www.factorio.com/download/stable"), headers);
		const QByteArray experimental = networkGetCached("Factorio versions (experimental)", "factorio", "versions.experimental.html",
														 QUrl("https://www.factorio.com/download/experimental"), headers);

		setStatus(tr("Processing responses..."));
		QList<QSharedPointer<BaseVersion>> versions;
		QRegularExpression exp("/get\\-download/direct/(?<version>[^/]*)/(?<type>[^/]*)/(?<platform>[^/\"]*)");
		QRegularExpressionMatchIterator it = exp.globalMatch(QString::fromUtf8(stable));
		while (it.hasNext())
		{
			const QRegularExpressionMatch match = it.next();
			versions.append(QSharedPointer<FactorioVersion>::create(Version::fromString(match.captured("version")),
																	match.captured("type"),
																	match.captured("platform"),
																	QUrl("https://www.factorio.com/").resolved(match.captured())));
		}
		it = exp.globalMatch(QString::fromUtf8(experimental));
		while (it.hasNext())
		{
			const QRegularExpressionMatch match = it.next();
			versions.append(QSharedPointer<FactorioVersion>::create(Version::fromString(match.captured("version")),
																	match.captured("type") + "-experimental",
																	match.captured("platform"),
																	QUrl("https://www.factorio.com/").resolved(match.captured())));
		}
		g_factorioVersions->setVersions(versions);
		setSuccess();
	}
};

FactorioVersionList::FactorioVersionList(QObject *parent)
	: BaseVersionList("factorio/versions.json", new VersionFactory<FactorioVersion>, parent)
{
}
bool FactorioVersionList::shouldShow(const int index, Container *container) const
{
	const QSharedPointer<FactorioVersion> version = at(index).dynamicCast<FactorioVersion>();
	return version->platform() == currentPlatform();
}
Task *FactorioVersionList::createRefreshTask()
{
	return new FactorioVersionsTask;
}

const QString FactorioVersionList::currentPlatform()
{
	static const bool is64 = QSysInfo::currentCpuArchitecture().endsWith("64"); // QUESTION: is this good?
	return
		#if defined(Q_OS_LINUX)
			(is64 ? "linux64" : "linux32")
		#elif defined(Q_OS_WIN)
			(is64 ? "win64" : "win32")
		#elif defined(Q_OS_OSX)
			"osx"
		#else
			"this is garbage that'll never get matched because factorio is not supported on this os"
		#endif
			;
}

#include "FactorioVersionList.moc"
