// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QObject>

#include "core/BasePlugin.h"

class FactorioPlugin : public QObject, public BasePlugin
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "jan.dalheimer.MultiLaunch.BasePlugin" FILE "metadata.json")
	Q_INTERFACES(BasePlugin)

public:
	void init() override;
	QList<IPage *> createGlobalPages() const override;
	QList<IPagePart *> createGlobalPageParts() const override;
};
