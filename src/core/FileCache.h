// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QFileInfo>
#include <QUrl>

#include "core/util/BaseConfigObject.h"
#include "core/util/Singleton.h"

class FileCache : public BaseConfigObject
{
public:
	explicit FileCache();
	~FileCache();

	struct Entry
	{
		QString group;
		QString id;
		QFileInfo onDisk;
		QUrl origin;
		QString etag;
		qint64 lastModifiedTimestamp; // only used to prevent slow hash-recalculation
		QByteArray hash;

		bool isValid() const;

	private:
		friend class FileCache;
		explicit Entry() {}
	};
	Entry getEntry(const QString &group, const QString &id) const;
	void setEntry(const Entry &entry);
	QString file(const QString &group, const QString &id) const;

	QString fileName(const QString &group, const QString &id) const;
	void addToCache(const QString &group, const QString &id, const QString &filename = QString());

	QList<Entry> allEntries() const { return m_entries; }

	bool isValid(const Entry &entry);

protected:
	QByteArray doSave() const override;
	void doLoad(const QByteArray &data) override;

private:
	// QUESTION store in a map?
	QList<Entry> m_entries;

	static QByteArray computeHash(QIODevice *device);
};
DECLARE_SINGLETON(FileCache, g_fileCache)
