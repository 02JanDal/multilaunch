// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QList>
#include <QHash>
#include <QMetaType>

class IPage
{
public:
	virtual ~IPage() {}

	virtual QString id() const = 0;
	virtual QString name() const = 0;
	virtual QString header() const = 0;
	virtual QString icon() const = 0;
	virtual QObject *createWidget() const = 0;

	virtual void initializePage(QObject *widget) = 0;
	virtual void applyPage(QObject *widget) = 0;
	virtual bool hasChanges(QObject *widget) const = 0;
};

class IPagePart
{
public:
	virtual ~IPagePart() {}

	virtual QString pageId() const = 0;
	virtual QString name() const = 0;

	virtual QObject *createWidget() const = 0;
	virtual void initializePart(QObject *widget) = 0;
	virtual void applyPart(QObject *widget) = 0;
	virtual bool hasChanges(QObject *widget) = 0;
};

Q_DECLARE_METATYPE(IPage *)
Q_DECLARE_METATYPE(IPagePart *)
