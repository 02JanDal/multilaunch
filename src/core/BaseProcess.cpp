// Licensed under the Apache-2.0 license. See README.md for details.

#include "BaseProcess.h"

#include <QProcess>

BaseProcess::BaseProcess(BaseLaunchData *data, QObject *parent)
	: QObject(parent), m_data(data)
{
}

void BaseProcess::connectProcess(QProcess *process)
{
	connect(process, &QProcess::started, [this](){emit stateChanged(Running);});
	connect(process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), [this](int code, QProcess::ExitStatus status)
	{
		if (m_exitState == NotExited)
		{
			if (status == QProcess::NormalExit && code == 0)
			{
				m_exitState = GracefullyFinished;
			}
			else
			{
				m_exitState = Crashed;
			}
		}
		emit stateChanged(Finished);
	});

	emit stateChanged(Pending);
}
