// Licensed under the Apache-2.0 license. See README.md for details.

#include "Container.h"

#include "components/BaseComponent.h"
#include "core/util/Json.h"
#include "core/util/FileSystem.h"
#include "core/tasks/Task.h"
#include "core/tasks/GroupedTask.h"
#include "core/packages/PackageHandler.h"
#include "IPage.h"
#include "Gui.h"

Container::Container(const QUuid &uuid, const QDir &dir, BaseComponent *root)
	: BaseCompoundComponent(nullptr), BaseConfigObject(dir.absoluteFilePath("container.json")), m_uuid(uuid), m_dir(dir)
{
	Q_ASSERT(root->canBeRoot());
	m_rootComponent = root;
	addChild(root);
}
Container::Container(const QUuid &uuid, const QDir &dir)
	: BaseCompoundComponent(nullptr), BaseConfigObject(dir.absoluteFilePath("container.json")), m_uuid(uuid), m_dir(dir)
{
	loadNow();
}
Container::~Container()
{
	saveNow();
}

QString Container::icon() const
{
	if (m_iconKey == "local")
	{
		return rootDir().absoluteFilePath("icon.png");
	}
	else
	{
		return "container_icons/" + m_iconKey;
	}
}

Version Container::version() const
{
	if (m_rootComponent)
	{
		return m_rootComponent->version();
	}
	else
	{
		return Version();
	}
}
QString Container::type() const
{
	if (m_rootComponent)
	{
		return m_rootComponent->type();
	}
	else
	{
		return tr("Invalid");
	}
}

void Container::setName(const QString &name)
{
	m_name = name;
	emit nameChanged();
	scheduleSave();
}
void Container::setIconKey(const QString &icon)
{
	m_iconKey = icon;
	emit iconKeyChanged();
	scheduleSave();
}

void Container::remove()
{
	setSavingDisabled(true);
	FS::remove(m_dir);
}

static void doTopoSort(int v, QVector<bool> &visited, const QVector<QList<int>> &edges, QVector<int> &output)
{
	visited[v] = true;
	for (auto it = edges[v].constBegin(); it != edges[v].constEnd(); ++it)
	{
		if (!visited[*it])
		{
			doTopoSort(*it, visited, edges, output);
		}
	}

	output.append(v);
}
void Container::sortChildren()
{
	QVector<BaseComponent *> input = children().toVector();
	QVector<QList<int>> edges(children().size());
	QVector<int> result;
	QVector<bool> visited(children().size(), false);

	// fill edges
	for (int i = 0; i < input.size(); ++i)
	{
		if (!input[i]->package().isValid())
		{
			continue;
		}
		auto handler = g_packages->handler(input[i]->package());
		auto deps = handler->getPackageDependencies(input[i]->package(), input[i]->version());
		for (const auto dep : deps)
		{
			for (int j = 0; j < input.size(); ++j)
			{
				if (input[j]->package() == dep.package())
				{
					edges[i].append(j);
					break;
				}
			}
		}
	}

	// do the sorting
	for (int i = 0; i < input.size(); ++i)
	{
		if (!visited[i])
		{
			doTopoSort(i, visited, edges, result);
		}
	}

	QList<BaseComponent *> sorted;
	for (const auto index : result)
	{
		sorted.append(input[index]);
	}
	setChildren(sorted);
}

void Container::load(const QJsonObject &obj)
{
	m_name = Json::ensureIsType<QString>(obj, "name");
	m_iconKey = Json::ensureIsType<QString>(obj, "iconKey");
	BaseCompoundComponent::load(obj);
	m_rootComponent = findChildByUuid(Json::ensureIsType<QUuid>(obj, "rootUuid"));
	emit nameChanged();
	emit iconKeyChanged();
	emit versionChanged();
	emit typeChanged();
}
QJsonObject Container::save() const
{
	QJsonObject obj = BaseCompoundComponent::save();
	obj.insert("name", m_name);
	obj.insert("iconKey", m_iconKey);
	obj.insert("rootUuid", Json::toJson(m_rootComponent ? m_rootComponent->uuid() : QUuid()));
	return obj;
}

Task *Container::createTask(const Stage stage, IStageData *data)
{
	Task *task = BaseCompoundComponent::createTask(stage, data);
	if (task)
	{
		GroupedTask *groupedTask = dynamic_cast<GroupedTask *>(task);
		if (groupedTask && stage == Stage::Launch)
		{
			// when launching some components will override stuff from others, so we need to ensure that they aren't run at the same time
			groupedTask->setMaxConcurrent(1);
		}
		connect(task, &Task::finished, [this](){scheduleSave();});
		task->setObjectName("Task for " + stageName(stage));
	}
	return task;
}

QList<IPage *> Container::createPages(const bool isRunning)
{
	if (isRunning)
	{
		return BaseCompoundComponent::createPages(isRunning) << g_gui->wait<IPage *>("CreateComponentsPage", isRunning, this);
	}
	else
	{
		return BaseCompoundComponent::createPages(isRunning)
				<< g_gui->wait<IPage *>("CreateComponentsPage", isRunning, this)
				<< g_gui->wait<IPage *>("CreateSettingsPage", this);
	}
}

IStageData *Container::createStageData(const Stage stage) const
{
	return m_rootComponent ? m_rootComponent->createStageData(stage) : nullptr;
}

QByteArray Container::doSave() const
{
	return Json::toBinary(save());
}
void Container::doLoad(const QByteArray &data)
{
	if (data.isEmpty())
	{
		throw Exception(tr("Container data is empty"));
	}
	load(Json::ensureObject(Json::ensureDocument(data)));
}
