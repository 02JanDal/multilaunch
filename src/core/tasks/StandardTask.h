// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "Task.h"

#include <QPointer>

class QEventLoop;
class QDir;
class INetworkValidator;

class StandardTask : public Task
{
	Q_OBJECT
public:
	explicit StandardTask(QObject *parent = nullptr);

protected:
	// TODO: switch to a future-based system
	void runTask(Task *other);
	void runTaskNonBlocking(Task *other);
	QByteArray networkGet(const QUrl &url);
	QByteArray networkPost(const QUrl &url, const QByteArray &data, const bool returnIfError, const QString &contentType = "application/x-www-form-urlencoded");
	QByteArray networkGetCached(const QString &name, const QString &group, const QString &id, const QUrl &url, const bool alwaysRefetch = false,
								INetworkValidator *validator = nullptr);
	QByteArray networkGetCached(const QString &name, const QString &group, const QString &id, const QUrl &url, const QMap<QString, QString> &headers,
								INetworkValidator *validator = nullptr);
	void networkGetCachedNonBlocking(const QString &name, const QString &group, const QString &id, const QUrl &url, const bool alwaysRefetch = false,
									 INetworkValidator *validator = nullptr);
	QByteArray runProcess(const QString &process, const QStringList &arguments, const QDir &workingDirectory);
	void moveFile(const QString &from, const QString &to);
	void copyFile(const QString &from, const QString &to);
	void waitOnPending();

private:
	QEventLoop *m_loop;
	QList<QPointer<Task>> m_pendingTasks;
};
