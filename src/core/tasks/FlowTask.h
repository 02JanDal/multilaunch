// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/tasks/Task.h"
#include "core/components/BaseComponent.h"

class Container;

class FlowTask : public Task
{
	Q_OBJECT
public:
	enum Flow
	{
		Launch,
		UpdateCheck,
		Creation,
		Removal
	};

	explicit FlowTask(const Flow flow, Container *container, QObject *parent = nullptr);
	~FlowTask();

	bool canAbort() const override;

signals:
	void interactionRequired(const Stage stage, IStageData *data);

public slots:
	void interactionFinished(const bool success);

protected:
	void run() override;
	void abort() override;

private slots:
	void currentTaskFinished();
	void currentTaskStatus();
	void currentTaskProgress();

private:
	Container *m_container;
	Flow m_flow;

	IStageData *m_currentStageData = nullptr;
	Task *m_currentTask = nullptr;
	void nextTask();

	Stage m_currentStage = Stage::Start;
	Stage getNextStage() const;
};
