// Licensed under the Apache-2.0 license. See README.md for details.

#include "GroupedTask.h"

GroupedTask::GroupedTask(QObject *parent)
	: Task(parent)
{
}
GroupedTask::GroupedTask(const QList<Task *> tasks, QObject *parent)
	: Task(parent)
{
	for (auto task : tasks)
	{
		addTask(task);
	}
}

void GroupedTask::addTask(Task *task)
{
	if (!task)
	{
		return;
	}
	connect(task, &Task::finished, this, &GroupedTask::taskFinished, Qt::QueuedConnection);
	connect(task, &Task::progressChanged, this, &GroupedTask::taskProgressChanged);
	connect(task, &Task::statusChanged, this, &GroupedTask::taskStatusChanged);
	connect(task, &Task::canAbortChanged, [this](bool){emit canAbortChanged(canAbort());});
	m_tasks.append(task);
	task->setParent(this);
	if (isRunning())
	{
		m_pendingTasks.append(task);
		startTasks();
	}
}

void GroupedTask::setMaxConcurrent(int num)
{
	if (isRunning())
	{
		qWarning("Attempted to change max concurrent tasks while already running");
	}
	else
	{
		m_numTasks = num;
	}
}

void GroupedTask::abort()
{
	for (const auto task : m_runningTasks)
	{
		task->blockSignals(true);
		task->stop();
		task->blockSignals(false);
	}
	setFailure(tr("Aborted"));
}
bool GroupedTask::canAbort() const
{
	for (const auto task : m_runningTasks)
	{
		if (!task->canAbort())
		{
			return false;
		}
	}
	return true;
}
void GroupedTask::run()
{
	if (m_tasks.isEmpty())
	{
		setSuccess();
		return;
	}
	m_reportingTask = m_tasks.first();
	m_pendingTasks = m_tasks.toVector();
	m_runningTasks.clear();
	startTasks();
}

void GroupedTask::startTasks()
{
	QList<Task *> tasksToStart;
	while (m_runningTasks.size() < m_numTasks || m_numTasks <= 0)
	{
		if (m_pendingTasks.isEmpty())
		{
			break;
		}
		Task *task = m_pendingTasks.takeFirst();
		m_runningTasks.append(task);
		tasksToStart.append(task);
	}
	if (m_pendingTasks.isEmpty() && m_runningTasks.isEmpty())
	{
		setProgress(100);
		setSuccess();
	}
	else
	{
		emit canAbortChanged(canAbort());
		for (auto task : tasksToStart)
		{
			task->start();
		}
	}
}

void GroupedTask::taskFinished()
{
	Task *task = qobject_cast<Task *>(sender());
	m_runningTasks.removeAll(task);
	if (isFailure())
	{
		return;
	}
	if (task->isFailure())
	{
		setFailure(task->errorString());
	}
	else
	{
		startTasks();

		// Invalid read of size 8
		// Is this called after we've been deleted?
		if (m_reportingTask == task && !m_runningTasks.isEmpty())
		{
			m_reportingTask = m_runningTasks.first();
			setStatus(m_reportingTask->status());
		}
	}
}
void GroupedTask::taskProgressChanged()
{
	int value = 0;
	for (const auto task : m_tasks)
	{
		value += qMax(0, task->progress());
	}
	setProgress(value, 100 * m_tasks.size());
}
void GroupedTask::taskStatusChanged()
{
	Task *task = qobject_cast<Task *>(sender());
	if (task == m_reportingTask)
	{
		setStatus(task->status());
	}
}
