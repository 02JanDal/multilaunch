// Licensed under the Apache-2.0 license. See README.md for details.

#include "RunProcessTask.h"

#include <QProcess>
#include <QDir>

RunProcessTask::RunProcessTask(const QString &executable, const QStringList &arguments, const QDir &workingDirectory, QObject *parent)
	: Task(parent), m_process(new QProcess(this))
{
	m_process->setProgram(executable);
	m_process->setArguments(arguments);
	m_process->setWorkingDirectory(workingDirectory.absolutePath());
	connectSignals();
}
RunProcessTask::RunProcessTask(QProcess *process, QObject *parent)
	: Task(parent), m_process(process)
{
	connectSignals();
}

void RunProcessTask::connectSignals()
{
	connect(m_process, static_cast<void(QProcess::*)(QProcess::ProcessError)>(&QProcess::error), [this]()
	{
		if (isRunning())
		{
			setFailure(m_process->errorString());
		}
	});

	connect(m_process, static_cast<void(QProcess::*)(int,QProcess::ExitStatus)>(&QProcess::finished), [this](int exitCode, QProcess::ExitStatus status)
	{
		if (isRunning() && status == QProcess::NormalExit && exitCode == 0)
		{
			setSuccess();
		}
		else if (isRunning())
		{
			setFailure(m_process->errorString());
		}
	});
}

void RunProcessTask::run()
{
	m_process->start();
}
