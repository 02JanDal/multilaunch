// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "StandardTask.h"

class Container;

class DependencyResolutionTask : public StandardTask
{
	Q_OBJECT
public:
	explicit DependencyResolutionTask(Container *container, QObject *parent = nullptr);

private:
	void run() override;

	Container *m_container;
};
