// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QObject>

class Task : public QObject
{
	Q_OBJECT
public:
	~Task() {}

	bool isStarted() const { return m_started; }
	bool isRunning() const { return m_running; }
	bool isFinished() const { return m_finished; }
	bool isSuccess() const { return m_success; }
	bool isFailure() const { return m_failure; }
	virtual bool canAbort() const { return false; }

	QString errorString() const { return m_errorString; }
	QString status() const { return m_status; }
	int progress() const { return m_progress; }

public slots:
	void start();
	void stop();

protected:
	explicit Task(QObject *parent = nullptr);

	virtual void run() = 0;
	virtual void abort() {}

protected slots:
	void setSuccess();
	void setFailure(const QString &msg);
	void setStatus(const QString &status);
	void setProgress(qint64 current, qint64 total = 100);

private:
	QString m_errorString;
	QString m_status;
	int m_progress = 0;
	bool m_started = false;
	bool m_running = false;
	bool m_finished = false;
	bool m_success = false;
	bool m_failure = false;

signals:
	void started();
	void succeeded();
	void failed();
	void finished();
	void errorChanged();
	void statusChanged(const QString &newStatus);
	void progressChanged(int newProgress);
	void canAbortChanged(const bool canAbort);
};
