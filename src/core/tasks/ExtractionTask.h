// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "Task.h"

#include <QDir>

class ExtractionTask : public Task
{
	Q_OBJECT
public:
	explicit ExtractionTask(const QString &filename, const QDir &destDir, QObject *parent = nullptr);

private:
	void run() override;

private:
	QString m_filename;
	QDir m_destDir;
};
