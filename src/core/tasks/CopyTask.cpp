// Licensed under the Apache-2.0 license. See README.md for details.

#include "CopyTask.h"

#include <QDir>

#include "core/util/FileSystem.h"

CopyTask::CopyTask(const QString &from, const QString &to, QObject *parent)
	: Task(parent), m_from(from), m_to(to)
{
}
CopyTask::CopyTask(QObject *parent)
	: Task(parent)
{
}

void CopyTask::run()
{
	if (!FS::exists(m_to))
	{
		FS::ensureExists(QFileInfo(m_to).dir());
		FS::copy(m_from, m_to);
	}
	setSuccess();
}
