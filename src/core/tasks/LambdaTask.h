// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "Task.h"

#include <functional>

template<typename T>
class LambdaTask : public Task
{
public:
	explicit LambdaTask(T lambda)
		: m_lambda(lambda)
	{
	}

protected:
	void run() override
	{
		setProgress(200);
		m_lambda();
		setSuccess();
	}

private:
	T m_lambda;
};
