// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "StandardTask.h"

class PreLaunchData;
class Container;
class BaseAccount;

class AuthTask : public StandardTask
{
	Q_OBJECT
public:
	AuthTask(const QString &type, Container *container, PreLaunchData *data, QObject *parent = nullptr);

	BaseAccount *account() const { return m_account; }

private:
	void run() override;
	void abort() override;

	QString m_type;
	Container *m_container;
	BaseAccount *m_account;
	PreLaunchData *m_data;
};
