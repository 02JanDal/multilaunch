// Licensed under the Apache-2.0 license. See README.md for details.

#include "ExtractionTask.h"

#include <QRegularExpression>
#include <QFile>
#include <KTar>
#include <KZip>

#include "core/util/Exception.h"
#include "core/util/FileSystem.h"

ExtractionTask::ExtractionTask(const QString &filename, const QDir &destDir, QObject *parent)
	: Task(parent), m_filename(filename), m_destDir(destDir)
{
}

typedef QPair<QString, const KArchiveFile *> ArchiveEntry;
static QList<ArchiveEntry> gatherFiles(const QString &parent, const KArchiveDirectory *dir)
{
	QList<ArchiveEntry> out;
	for (const auto e : dir->entries())
	{
		const KArchiveEntry *entry = dir->entry(e);
		if (entry->isDirectory())
		{
			const QString name = parent + dir->name() + '/';
			out += gatherFiles(dir->name() == "/" ? "" : name, static_cast<const KArchiveDirectory *>(entry));
		}
		else if (entry->isFile())
		{
			out += qMakePair(parent + dir->name() + '/' + entry->name(), static_cast<const KArchiveFile *>(entry));
		}
	}
	return out;
}
void ExtractionTask::run()
{
	KArchive *arch;
	if (m_filename.contains(QRegularExpression(".*\\.tar(\\.[a-z0-9]*)?$")))
	{
		qDebug() << m_filename << "is a tar archive";
		arch = new KTar(m_filename);
	}
	else if (m_filename.contains(QRegularExpression(".*\\.zip$")))
	{
		qDebug() << m_filename << "is a zip archive";
		arch = new KZip(m_filename);
	}
	else
	{
		throw Exception("Unknown archive format");
	}

	if (!arch->isOpen())
	{
		if (!arch->open(QIODevice::ReadOnly))
		{
			throw Exception("Unable to open " + arch->fileName() + " for reading: " + arch->device()->errorString());
		}
	}
	QList<ArchiveEntry> files = gatherFiles(QString(),
											static_cast<const KArchiveDirectory *>(arch->directory()->entry(arch->directory()->entries().first())));
	std::sort(files.begin(), files.end(), [](const ArchiveEntry &a, const ArchiveEntry &b)
	{ return a.second->position() < b.second->position(); });

	const int totalFiles = files.size();
	int extractedFiles = 0;

	for (const auto pair : files)
	{
		const QString dest = m_destDir.absoluteFilePath(pair.first);
		const KArchiveFile *file = pair.second;
		setStatus(tr("Extracting %1").arg(pair.first));
		setProgress(extractedFiles++, totalFiles);
		FS::touch(dest);
		if (!file->copyTo(QFileInfo(dest).dir().path()))
		{
			throw Exception("Unable to extract " + pair.first + " to " + dest);
		}
#ifdef Q_OS_UNIX
		const mode_t perms = file->permissions();
		QFile::Permissions permissions = 0x0;
		if (perms & S_IRUSR)
		{
			permissions |= QFile::ReadOwner;
		}
		if (perms & S_IWUSR)
		{
			permissions |= QFile::WriteOwner;
		}
		if(perms & S_IXUSR)
		{
			permissions |= QFile::ExeOwner;
		}
		if (perms & S_IRGRP)
		{
			permissions |= QFile::ReadGroup;
		}
		if (perms & S_IWGRP)
		{
			permissions |= QFile::WriteGroup;
		}
		if(perms & S_IXGRP)
		{
			permissions |= QFile::ExeGroup;
		}
		if (perms & S_IROTH)
		{
			permissions |= QFile::ReadOther;
		}
		if (perms & S_IWOTH)
		{
			permissions |= QFile::WriteOther;
		}
		if(perms & S_IXOTH)
		{
			permissions |= QFile::ExeOther;
		}

		if (permissions == 0x0)
		{
			permissions = QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner | QFile::ReadGroup | QFile::ReadOther;
		}
		QFile(dest).setPermissions(permissions);
#endif
	}

	delete arch;

	setSuccess();
}
