// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "Task.h"

#include <QHash>
#include <QVector>

class GroupedTask : public Task
{
	Q_OBJECT
public:
	explicit GroupedTask(QObject *parent = nullptr);
	explicit GroupedTask(const QList<Task *> tasks, QObject *parent = nullptr);

	void addTask(Task *task);
	void setMaxConcurrent(int num);

private:
	void run() override;
	void abort() override;
	bool canAbort() const override;

	Task *m_reportingTask = nullptr;
	QList<Task *> m_tasks;
	QVector<Task *> m_pendingTasks;
	QVector<Task *> m_runningTasks;
	int m_numTasks = 50;

	void startTasks();

private slots:
	void taskFinished();
	void taskProgressChanged();
	void taskStatusChanged();
};
