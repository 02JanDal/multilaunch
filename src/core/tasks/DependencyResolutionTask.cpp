// Licensed under the Apache-2.0 license. See README.md for details.

#include "DependencyResolutionTask.h"

#include "core/packages/PackageHandler.h"
#include "core/Container.h"
#include "core/BaseVersionList.h"

DependencyResolutionTask::DependencyResolutionTask(Container *container, QObject *parent)
	: StandardTask(parent), m_container(container)
{
}

static void collectComponents(BaseComponent *from, QList<BaseComponent *> &out)
{
	out.append(from);
	for (const auto child : from->children())
	{
		collectComponents(child, out);
	}
}

void DependencyResolutionTask::run()
{
	QList<BaseComponent *> components;
	collectComponents(m_container, components);
	// TODO do real depres

	// fetch data
	setStatus(tr("Fetching dependency data..."));
	for (const auto component : components)
	{
		if (component->package().isValid())
		{
			runTaskNonBlocking(g_packages->handler(component->package())->createFetchInformationTask(component->package()));
		}
	}
	waitOnPending();

	// collect needed packages
	setStatus(tr("Doing depres..."));
	QMap<Package, QPair<QList<BaseComponent *>, PackageQuery>> queries;
	for (const auto component : components)
	{
		if (!component->package().isValid())
		{
			continue;
		}
		QList<PackageQuery> deps = g_packages->handler(component->package())->getPackageDependencies(component->package(), component->version());
		for (const auto dep : deps)
		{
			if (queries.contains(dep.package()))
			{
				queries[dep.package()].first.append(component);
				queries[dep.package()].second = queries[dep.package()].second.intersected(dep);
			}
			else
			{
				queries.insert(dep.package(), qMakePair(QList<BaseComponent *>() << component,
														dep));
			}
		}
	}
	// remove already existing
	for (const auto component : components)
	{
		if (!component->package().isValid())
		{
			continue;
		}
		queries.remove(component->package());
	}

	// create and add needed components
	for (auto it = queries.constBegin(); it != queries.constEnd(); ++it)
	{
		BasePackageHandler *handler = g_packages->handler(it.key());
		const PackageQuery query = it.value().second;

		// get and clean a list of versions
		BaseVersionList *list = handler->getVersionsList(it.key());
		QList<Version> versions;
		if (list)
		{
			versions = list->versions();
			QMutableListIterator<Version> i(versions);
			while (i.hasNext())
			{
				const Version v = i.next();
				bool isContained = false;
				for (const auto interval : query.versions())
				{
					if (interval.contains(v))
					{
						isContained = true;
						break;
					}
				}
				if (!isContained)
				{
					i.remove();
				}
			}
		}
		else
		{
			versions = QList<Version>() << Version();
		}

		if (versions.isEmpty())
		{
			setFailure(tr("No versions available for %1").arg(query.package().toString()));
		}
		else
		{
			std::sort(versions.begin(), versions.end());
			BaseComponent *component = handler->createComponent(it.key(), m_container);
			if (versions.first().isValid())
			{
				component->setVersion(handler->getVersionsList(it.key())->findVersion(versions.first()));
			}
			for (const auto addedBy : it.value().first)
			{
				component->addAddedBy(addedBy);
			}
			m_container->addChild(component);
		}
	}

	setSuccess();
}
