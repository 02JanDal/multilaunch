// Licensed under the Apache-2.0 license. See README.md for details.

#include "Task.h"

#include <QCoreApplication>
#include <QLoggingCategory>

#include "core/util/Exception.h"

Task::Task(QObject *parent)
	: QObject(parent)
{
	qRegisterMetaType<Task *>("Task*");
}

void Task::start()
{
	if (m_running)
	{
		return;
	}
	m_started = true;
	m_running = true;
	m_finished = false;
	m_success = false;
	m_failure = false;
	m_errorString.clear();
	m_status.clear();
	emit started();
	emit errorChanged();
	emit statusChanged(m_status);
	try
	{
		run();
	}
	catch (Exception &e)
	{
		setFailure(e.message());
	}
}
void Task::stop()
{
	if (!m_running)
	{
		return;
	}
	if (canAbort())
	{
		abort();
	}
}

void Task::setSuccess()
{
	Q_ASSERT(m_running);
	m_running = false;
	m_finished = true;
	m_success = true;
	m_failure = false;
	m_errorString.clear();
	emit errorChanged();
	emit succeeded();
	emit finished();
}
void Task::setFailure(const QString &msg)
{
	if (!m_running)
	{
		return;
	}
	qWarning() << this << "failed:" << msg;
	m_running = false;
	m_finished = true;
	m_success = false;
	m_failure = true;
	m_errorString = msg;
	emit errorChanged();
	emit failed();
	emit finished();
}
void Task::setStatus(const QString &status)
{
	if (!m_running)
	{
		return;
	}
	if (!status.isEmpty() && !inherits("GroupedTask") && !inherits("FlowTask"))
	{
		qDebug() << this << status;
	}
	m_status = status;
	emit statusChanged(m_status);
	qApp->processEvents(); // ensure the new message gets shown
}
void Task::setProgress(qint64 current, qint64 total)
{
	if (!m_running)
	{
		return;
	}
	m_progress = qRound(100 * double(current) / double(total));
	emit progressChanged(m_progress);
	qApp->processEvents(); // ensure the new progress gets shown
}
