// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "Task.h"

class QProcess;
class QDir;

class RunProcessTask : public Task
{
public:
	explicit RunProcessTask(const QString &executable, const QStringList &arguments, const QDir &workingDirectory, QObject *parent = nullptr);
	explicit RunProcessTask(QProcess *process, QObject *parent = nullptr);

	QProcess *process() const { return m_process; }

private:
	void connectSignals();
	void run() override;

	QProcess *m_process;
};
