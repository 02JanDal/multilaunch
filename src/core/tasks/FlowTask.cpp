// Licensed under the Apache-2.0 license. See README.md for details.

#include "FlowTask.h"

#include <QLoggingCategory>

#include "core/Container.h"
#include "core/BaseProcess.h"
#include "DependencyResolutionTask.h"

FlowTask::FlowTask(const Flow flow, Container *container, QObject *parent)
	: Task(parent), m_container(container), m_flow(flow)
{
}

FlowTask::~FlowTask()
{
	if (m_currentStageData)
	{
		for (auto data : m_currentStageData->otherStages.values())
		{
			delete data;
		}
	}
}

void FlowTask::interactionFinished(const bool success)
{
	if (success)
	{
		setProgress(100);
		nextTask();
	}
	else
	{
		setFailure(tr("User interaction failed"));
	}
}

void FlowTask::run()
{
	m_currentStage = Stage::Start;
	nextTask();
}
void FlowTask::abort()
{
	m_currentTask->blockSignals(true);
	m_currentTask->stop();
	m_currentTask->blockSignals(false);
	setFailure(tr("Aborted"));
}

bool FlowTask::canAbort() const
{
	return m_currentTask ? m_currentTask->canAbort() : false;
}

void FlowTask::nextTask()
{
	if (m_currentTask)
	{
		delete m_currentTask;
		m_currentTask = nullptr;
	}

	m_currentStage = getNextStage();
	qDebug() << "Starting" << stageName(m_currentStage);
	if (m_currentStage == Stage::End)
	{
		setSuccess();
		return;
	}
	else if (m_currentStage == Stage::Depres)
	{
		m_currentTask = new DependencyResolutionTask(m_container, this);
	}
	else
	{
		m_container->sortChildren();
		QHash<Stage, IStageData *> stages = m_currentStageData ? m_currentStageData->otherStages : QHash<Stage, IStageData *>();
		m_currentStageData = m_container->createStageData(m_currentStage);
		if (!m_currentStageData)
		{
			if (m_currentStage == Stage::UpdateCheck)
			{
				m_currentStageData = new UpdateCheckData;
			}
			else if (m_currentStage == Stage::PreLaunch)
			{
				m_currentStageData = new PreLaunchData;
			}
			else if (m_currentStage == Stage::Launch)
			{
				m_currentStageData = new BaseLaunchData;
			}
			else
			{
				m_currentStageData = new IStageData;
			}
		}
		m_currentStageData->container = m_container;
		stages[m_currentStage] = m_currentStageData;
		m_currentStageData->otherStages = stages;

		m_currentTask = m_container->createTask(m_currentStage, m_currentStageData);
	}

	if (m_currentTask)
	{
		m_currentTask->setParent(this);
		connect(m_currentTask, &Task::finished, this, &FlowTask::currentTaskFinished);
		connect(m_currentTask, &Task::statusChanged, this, &FlowTask::currentTaskStatus);
		connect(m_currentTask, &Task::progressChanged, this, &FlowTask::currentTaskProgress);
		connect(m_currentTask, &Task::canAbortChanged, this, &Task::canAbortChanged);
		m_currentTask->start();
		emit canAbortChanged(canAbort());
	}
	else
	{
		nextTask();
	}
}

void FlowTask::currentTaskFinished()
{
	disconnect(m_currentTask, 0, this, 0);
	if (m_currentTask->isSuccess())
	{
		if (m_currentStage == Stage::Launch || m_currentStage == Stage::UpdateCheck)
		{
			setProgress(101);
			if (m_currentStage == Stage::Launch)
			{
				BaseLaunchData *launchData = static_cast<BaseLaunchData *>(m_currentStageData);
				connect(launchData->process, &BaseProcess::stateChanged, [this, launchData](const BaseProcess::State state)
				{
					if (state == BaseProcess::Finished)
					{
						launchData->process->deleteLater();
						interactionFinished(launchData->process->exitState() != BaseProcess::Crashed);
					}
				});
			}
			emit interactionRequired(m_currentStage, m_currentStageData);
		}
		else
		{
			nextTask();
		}
	}
	else
	{
		setFailure(m_currentTask->errorString());
	}
}
void FlowTask::currentTaskStatus()
{
	setStatus(m_currentTask->status());
}
void FlowTask::currentTaskProgress()
{
	setProgress(m_currentTask->progress());
}

// This is the main workhorse, which decides what should be done next
Stage FlowTask::getNextStage() const
{
	if (m_flow == Creation)
	{
		switch (m_currentStage)
		{
		case Stage::Start: return Stage::Creation;
		case Stage::Creation: return Stage::Depres;
		case Stage::Depres: return Stage::PreDownload;
		case Stage::PreDownload: return Stage::Download;
		case Stage::Download: return Stage::Installation;
		case Stage::Installation:
		default:
			return Stage::End;
		}
	}
	else if (m_flow == Removal)
	{
		switch (m_currentStage)
		{
		case Stage::Start: return Stage::Removal;
		case Stage::Removal:
		default:
			return Stage::End;
		}
	}
	else if (m_flow == UpdateCheck)
	{
		switch (m_currentStage)
		{
		case Stage::Start: return Stage::UpdateCheck;
		case Stage::UpdateCheck: return Stage::Depres;
		case Stage::Depres: return Stage::PreDownload;
		case Stage::PreDownload: return Stage::Download;
		case Stage::Download: return Stage::Installation;
		case Stage::Installation:
		default:
			return Stage::End;
		}
	}
	else if (m_flow == Launch)
	{
		switch (m_currentStage)
		{
		case Stage::Start: return Stage::UpdateCheck;
		case Stage::UpdateCheck: return Stage::Depres;
		case Stage::Depres: return Stage::PreDownload;
		case Stage::PreDownload: return Stage::Download;
		case Stage::Download: return Stage::Installation;
		case Stage::Installation: return Stage::PreLaunch;
		case Stage::PreLaunch: return Stage::Launch;
		case Stage::Launch: return Stage::PostLaunch;
		case Stage::PostLaunch:
		default:
			return Stage::End;
		}
	}
	else
	{
		Q_ASSERT_X(false, "FlowTask::nextStage", "Invalid flow");
	}
}
