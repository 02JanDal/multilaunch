// Licensed under the Apache-2.0 license. See README.md for details.

#include "AuthTask.h"

#include "core/accounts/AccountModel.h"
#include "core/accounts/BaseAccount.h"
#include "core/components/BaseComponent.h"
#include "core/Gui.h"

AuthTask::AuthTask(const QString &type, Container *container, PreLaunchData *data, QObject *parent)
	: StandardTask(parent), m_type(type), m_container(container), m_data(data)
{
}

void AuthTask::run()
{
	BaseAccount *account = g_accounts->get(m_type, m_container);
	if (account)
	{
		try
		{
			setStatus(tr("Checking if default account is valid..."));
			runTask(account->createCheckTask());
			// easiest: have default and it's valid
			m_account = account;
			if (m_data)
			{
				m_data->accounts[m_type] = account;
			}
			setSuccess();
			return;
		}
		catch (...)
		{
			setStatus(tr("Querying user for authentication details..."));
			setProgress(101);
			if (g_gui->wait<bool>("LoginToAccount", account))
			{
				m_account = account;
				if (m_data)
				{
					m_data->accounts[m_type] = account;
				}
				setSuccess();
				return;
			}
			else
			{
				throw Exception(tr("No valid authentication details given"));
			}
		}
	}
	else
	{
		setStatus(tr("Querying user for account..."));
		BaseAccount *acc = g_gui->wait<BaseAccount *>("RequestAccount", m_type);
		if (acc)
		{
			m_account = acc;
			if (m_data)
			{
				m_data->accounts[m_type] = acc;
			}
			setSuccess();
			return;
		}
		else
		{
			throw Exception(tr("No account choosen"));
		}
	}
}
void AuthTask::abort()
{
}
