// Licensed under the Apache-2.0 license. See README.md for details.

#include "StandardTask.h"

#include <QEventLoop>
#include <QProcess>

#include "core/network/CachedDownloadTask.h"
#include "core/network/RawDownloadTask.h"
#include "core/util/Exception.h"
#include "core/util/FileSystem.h"
#include "core/tasks/CopyTask.h"
#include "core/tasks/MoveFileTask.h"
#include "core/tasks/RunProcessTask.h"
#include "core/FileCache.h"

StandardTask::StandardTask(QObject *parent)
	: Task(parent)
{
	m_loop = new QEventLoop(this);
}

void StandardTask::runTask(Task *other)
{
	connect(other, &Task::finished, m_loop, &QEventLoop::quit);
	connect(other, &Task::progressChanged, [this](int progress){setProgress(progress);});
	connect(other, &Task::statusChanged, this, &StandardTask::setStatus);
	if (!other->isStarted())
	{
		other->start();
	}
	if (!other->isFinished())
	{
		m_loop->exec();
	}
	disconnect(other, 0, m_loop, 0);
	disconnect(other, 0, this, 0);
	other->deleteLater();
	if (other->isFailure())
	{
		throw Exception(other->errorString());
	}
}
void StandardTask::runTaskNonBlocking(Task *other)
{
	if (!other)
	{
		return;
	}
	m_pendingTasks.append(other);
	other->start();
}
QByteArray StandardTask::networkGet(const QUrl &url)
{
	RawDownloadTask *task = new RawDownloadTask(url, "", this);
	runTask(task);
	return task->response();
}
QByteArray StandardTask::networkPost(const QUrl &url, const QByteArray &data, const bool returnIfError, const QString &contentType)
{
	RawDownloadTask *task = new RawDownloadTask(url, contentType, this);
	task->setData(data);
	try
	{
		runTask(task);
	}
	catch (...)
	{
		if (returnIfError)
		{
			return task->read();
		}
		else
		{
			throw;
		}
	}

	return task->response();
}
QByteArray StandardTask::networkGetCached(const QString &name, const QString &group, const QString &id, const QUrl &url, const bool alwaysRefetch,
										  INetworkValidator *validator)
{
	if (!alwaysRefetch && g_fileCache->getEntry(group, id).isValid())
	{
		if (validator) { delete validator; }
		return FS::read(g_fileCache->fileName(group, id));
	}
	CachedDownloadTask *task = new CachedDownloadTask(name, group, id, url, this);
	task->setValidator(validator);
	runTask(task);
	return FS::read(g_fileCache->getEntry(group, id).onDisk.absoluteFilePath());
}
QByteArray StandardTask::networkGetCached(const QString &name, const QString &group, const QString &id, const QUrl &url, const QMap<QString, QString> &headers,
										  INetworkValidator *validator)
{
	if (g_fileCache->getEntry(group, id).isValid())
	{
		if (validator) { delete validator; }
		return FS::read(g_fileCache->fileName(group, id));
	}
	CachedDownloadTask *task = new CachedDownloadTask(name, group, id, url, this);
	task->setHeaders(headers);
	task->setValidator(validator);
	runTask(task);
	return FS::read(g_fileCache->getEntry(group, id).onDisk.absoluteFilePath());
}
void StandardTask::networkGetCachedNonBlocking(const QString &name, const QString &group, const QString &id, const QUrl &url, const bool alwaysRefetch,
											   INetworkValidator *validator)
{
	if (!alwaysRefetch && g_fileCache->getEntry(group, id).isValid())
	{
		return;
	}
	CachedDownloadTask *task = new CachedDownloadTask(name, group, id, url, this);
	task->setValidator(validator);
	m_pendingTasks.append(task);
	task->start();
}
QByteArray StandardTask::runProcess(const QString &process, const QStringList &arguments, const QDir &workingDirectory)
{
	RunProcessTask *task = new RunProcessTask(process, arguments, workingDirectory, this);
	task->process()->setProcessChannelMode(QProcess::MergedChannels);
	runTask(task);
	return task->process()->readAll();
}
void StandardTask::moveFile(const QString &from, const QString &to)
{
	runTask(new MoveFileTask(from, to, this));
}
void StandardTask::copyFile(const QString &from, const QString &to)
{
	runTask(new CopyTask(from, to, this));
}
void StandardTask::waitOnPending()
{
	for (auto task : m_pendingTasks)
	{
		if (task && !task->isFinished())
		{
			runTask(task);
		}
	}
}
