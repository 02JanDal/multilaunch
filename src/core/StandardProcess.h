// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "BaseProcess.h"
#include "core/components/BaseComponent.h"

struct StandardLaunchData : public BaseLaunchData
{
	QString workingDirectory;
	QStringList arguments;
	QString executable;
};

class QProcess;

class StandardProcess : public BaseProcess
{
	Q_OBJECT
public:
	explicit StandardProcess(BaseLaunchData *data, QObject *parent = nullptr);

protected:
	void start() override;
	void abort() override;

private slots:
	void handleProcessOutput();

private:
	QProcess *m_process;
};
