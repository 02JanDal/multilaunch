// Licensed under the Apache-2.0 license. See README.md for details.

#include "ComponentModel.h"

#include "core/components/BaseComponent.h"
#include "core/components/BaseCompoundComponent.h"

static void collectComponents(BaseComponent *from, QList<BaseComponent *> &out)
{
	out.append(from);
	for (const auto child : from->children())
	{
		collectComponents(child, out);
	}
}
ComponentModel::ComponentModel(BaseComponent *root, QObject *parent)
	: QAbstractItemModel(parent), m_root(root)
{
	QList<BaseComponent *> comps;
	collectComponents(m_root, comps);
	for (auto comp : comps)
	{
		ensureIsSetup(comp);
	}
}

QModelIndex ComponentModel::index(int row, int column, const QModelIndex &parent) const
{
	BaseComponent *p = parent.isValid() ? static_cast<BaseComponent *>(parent.internalPointer()) : m_root;
	return createIndex(row, column, p->children().at(row));
}
QModelIndex ComponentModel::parent(const QModelIndex &child) const
{
	BaseComponent *parent = static_cast<BaseComponent *>(child.internalPointer())->parent();
	if (!parent || parent == m_root)
	{
		return QModelIndex();
	}
	else
	{
		return index(parent);
	}
}

int ComponentModel::rowCount(const QModelIndex &parent) const
{
	BaseComponent *component = parent.isValid() ? static_cast<BaseComponent *>(parent.internalPointer()) : m_root;
	return component->children().size();
}
int ComponentModel::columnCount(const QModelIndex &parent) const
{
	return 3;
}

QVariant ComponentModel::data(const QModelIndex &index, int role) const
{
	BaseComponent *component = static_cast<BaseComponent *>(index.internalPointer());
	if (role == Qt::DisplayRole)
	{
		switch (index.column())
		{
		case 0:
			return component->name();
		case 1:
			return component->type();
		case 2:
			return component->version().toString();
		}
	}
	return QVariant();
}
QVariant ComponentModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
	{
		switch (section)
		{
		case 0: return tr("Name");
		case 1: return tr("Type");
		case 2: return tr("Version");
		}
	}
	return QVariant();
}

Qt::ItemFlags ComponentModel::flags(const QModelIndex &index) const
{
	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

BaseComponent *ComponentModel::component(const QModelIndex &index) const
{
	return static_cast<BaseComponent *>(index.internalPointer());
}

void ComponentModel::componentChildrenChanged()
{
	beginResetModel();
	endResetModel();
}

void ComponentModel::ensureIsSetup(BaseComponent *component)
{
	if (m_setupComponents.contains(component))
	{
		return;
	}
	m_setupComponents.append(component);
	BaseCompoundComponent *compound = dynamic_cast<BaseCompoundComponent *>(component);
	if (compound)
	{
		connect(compound, &BaseCompoundComponent::childrenChanged, this, &ComponentModel::componentChildrenChanged);
	}
	connect(component, &BaseComponent::nameChanged, this, &ComponentModel::componentNameChanged);
	connect(component, &BaseComponent::typeChanged, this, &ComponentModel::componentTypeChanged);
	connect(component, &BaseComponent::versionChanged, this, &ComponentModel::componentVersionChanged);
}
void ComponentModel::componentNameChanged()
{
	BaseComponent *component = qobject_cast<BaseComponent *>(sender());
	emit dataChanged(index(component, 0), index(component, 0));
}
void ComponentModel::componentVersionChanged()
{
	BaseComponent *component = qobject_cast<BaseComponent *>(sender());
	emit dataChanged(index(component, 1), index(component, 1));
}
void ComponentModel::componentTypeChanged()
{
	BaseComponent *component = qobject_cast<BaseComponent *>(sender());
	emit dataChanged(index(component, 2), index(component, 2));
}

QModelIndex ComponentModel::index(BaseComponent *component, const int column) const
{
	if (component->parent())
	{
		return createIndex(component->parent()->children().indexOf(component), column, component);
	}
	else
	{
		return createIndex(0, column, component);
	}
}
