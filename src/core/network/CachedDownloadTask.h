// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "NetworkTask.h"

#include <QUrl>
#include <QMap>

class QIODevice;
class QSaveFile;

class INetworkValidator
{
public:
	virtual ~INetworkValidator() {}

	virtual void validate(const QByteArray &data) = 0;
};
class JsonValidator : public INetworkValidator
{
public:
	void validate(const QByteArray &data) override;
};
class MD5HashValidator : public INetworkValidator
{
public:
	explicit MD5HashValidator(const QByteArray &expected)
		: m_expected(expected) {}
	void validate(const QByteArray &data) override;

private:
	QByteArray m_expected;
};

class CachedDownloadTask : public NetworkTask
{
	Q_OBJECT
public:
	explicit CachedDownloadTask(const QString &name, const QString &group, const QString &id, const QUrl &url, QObject *parent = nullptr);
	~CachedDownloadTask();

	void setValidator(INetworkValidator *validator);
	void setHeaders(const QMap<QString, QString> &headers) { m_headers = headers; }

private slots:
	void dataReady();

private:
	QString m_name;
	QString m_group;
	QString m_id;
	QUrl m_url;
	QMap<QString, QString> m_headers;

	QByteArray m_etag;
	QSaveFile *m_file = nullptr;

	INetworkValidator *m_validator = nullptr;

	void run() override;
	bool next(QNetworkReply *reply) override;
	QNetworkReply *createReply(const QUrl &url) override;
};
