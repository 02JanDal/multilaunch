// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QNetworkAccessManager>

#include "core/util/Singleton.h"

DECLARE_SINGLETON(QNetworkAccessManager, g_nam)
