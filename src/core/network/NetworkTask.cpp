// Licensed under the Apache-2.0 license. See README.md for details.

#include "NetworkTask.h"

#include <QNetworkReply>

#include "core/util/Exception.h"

DEFINE_SINGLETON(g_nam)

NetworkTask::NetworkTask(QObject *parent)
	: Task(parent)
{
}

void NetworkTask::abort()
{
	if (m_reply)
	{
		m_reply->abort();
	}
}
void NetworkTask::setReply(QNetworkReply *reply)
{
	if (m_reply)
	{
		disconnect(m_reply, 0, this, 0);
	}
	m_reply = reply;
	m_reply->setParent(this);
	connect(m_reply, &QNetworkReply::finished, this, &NetworkTask::networkFinished);
	connect(m_reply, &QNetworkReply::downloadProgress, this, &NetworkTask::networkDownloadProgress);
	connect(m_reply, &QNetworkReply::uploadProgress, this, &NetworkTask::networkUploadProgress);
}

void NetworkTask::updateProgress()
{
	if (isRunning())
	{
		setProgress(m_downloadCurrent + m_uploadCurrent, m_downloadTotal + m_uploadTotal);
	}
}

void NetworkTask::networkFinished()
{
	try
	{
		const QUrl redirectUrl = m_reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
		if (m_reply->error() != QNetworkReply::NoError)
		{
			throw Exception(m_reply->errorString());
		}
		else if (m_followRedirects && !redirectUrl.isEmpty())
		{
			setReply(createReply(redirectUrl));
		}
		else
		{
			if (!next(m_reply))
			{
				setSuccess();
			}
		}
	}
	catch (Exception &e)
	{
		if (m_retries > 0)
		{
			qDebug() << "Network failed, retrying," << m_retries << "tries left";
			m_retries--;
			setReply(createReply());
		}
		else
		{
			setFailure(e.message());
		}
	}
}
void NetworkTask::networkDownloadProgress(qint64 current, qint64 total)
{
	m_downloadCurrent = current;
	m_downloadTotal = total;
	updateProgress();
}
void NetworkTask::networkUploadProgress(qint64 current, qint64 total)
{
	m_uploadCurrent = current;
	m_uploadTotal = total;
	updateProgress();
}

QByteArray NetworkTask::read() const
{
	return m_reply->readAll();
}
