// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/tasks/Task.h"

#include <QUrl>

#include "core/util/Singleton.h"

class QNetworkReply;
class QNetworkAccessManager;

class NetworkTask : public Task
{
	Q_OBJECT
public:
	~NetworkTask() {}

	bool canAbort() const override { return true; }

	QByteArray read() const;
	QNetworkReply *reply() const { return m_reply; }

	void setFollowRedirects(const bool follow) { m_followRedirects = follow; }

protected:
	explicit NetworkTask(QObject *parent = nullptr);

	void abort() override;
	void setReply(QNetworkReply *reply);
	/// reimplement this function if you have multiple downloads
	/// if false is returned, the task will be finished with the success status
	virtual bool next(QNetworkReply *) { return false; }
	virtual QNetworkReply *createReply(const QUrl &url = QUrl()) = 0;

private:
	QNetworkReply *m_reply = nullptr;
	qint64 m_downloadCurrent = 0, m_downloadTotal = 0, m_uploadCurrent = 0, m_uploadTotal = 0;
	bool m_followRedirects = true;
	int m_retries = 3;
	void updateProgress();

private slots:
	void networkFinished();
	void networkDownloadProgress(qint64 current, qint64 total);
	void networkUploadProgress(qint64 current, qint64 total);
};

DECLARE_SINGLETON(QNetworkAccessManager, g_nam)
