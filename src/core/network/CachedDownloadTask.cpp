// Licensed under the Apache-2.0 license. See README.md for details.

#include "CachedDownloadTask.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDir>
#include <QCryptographicHash>
#include <QSaveFile>

#include "core/FileCache.h"
#include "core/util/FileSystem.h"
#include "core/util/Json.h"
#include "NetworkAccessManager.h"

CachedDownloadTask::CachedDownloadTask(const QString &name, const QString &group, const QString &id, const QUrl &url, QObject *parent)
	: NetworkTask(parent), m_name(name), m_group(group), m_id(id), m_url(url)
{
	Q_ASSERT(url.isValid());
	setObjectName("Download for " + m_url.toString());
}
CachedDownloadTask::~CachedDownloadTask()
{
	if (m_validator)
	{
		delete m_validator;
	}
}

void CachedDownloadTask::setValidator(INetworkValidator *validator)
{
	m_validator = validator;
}

void CachedDownloadTask::dataReady()
{
	m_file->write(reply()->readAll());
}

void CachedDownloadTask::run()
{
	FileCache::Entry entry = g_fileCache->getEntry(m_group, m_id);
	if (entry.isValid())
	{
		setStatus(tr("Cached file is valid for %1/%2").arg(m_group, m_id));
		if (m_validator)
		{
			setProgress(101);
			m_validator->validate(FS::read(entry.onDisk.absoluteFilePath()));
		}
		setProgress(100);
		setSuccess();
		return;
	}
	m_etag = entry.etag.toLatin1();
	setStatus(tr("Downloading %1").arg(m_url.toString()));
	setReply(createReply(m_url));
}
bool CachedDownloadTask::next(QNetworkReply *reply)
{
	m_file->write(reply->readAll());
	setProgress(100);
	if (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 304)
	{
		m_file->cancelWriting();
	}
	else
	{
		m_file->commit();
		g_fileCache->addToCache(m_group, m_id);
		FileCache::Entry entry = g_fileCache->getEntry(m_group, m_id);
		entry.origin = m_url;
		entry.etag = QString::fromLatin1(reply->rawHeader("ETag"));
		g_fileCache->setEntry(entry);
	}
	if (m_validator)
	{
		setProgress(101);
		m_validator->validate(FS::read(g_fileCache->fileName(m_group, m_id)));
		setProgress(100);
	}
	return false;
}

QNetworkReply *CachedDownloadTask::createReply(const QUrl &url)
{
	if (m_file)
	{
		delete m_file;
	}
	FS::touch(g_fileCache->fileName(m_group, m_id));
	m_file = new QSaveFile(g_fileCache->fileName(m_group, m_id));
	if (!m_file->open(QFile::WriteOnly))
	{
		throw Exception(tr("Unable to open savefile %1: %2").arg(m_file->fileName(), m_file->errorString()));
	}

	QNetworkRequest req(url.isEmpty() ? m_url : url);
	if (!m_etag.isNull())
	{
		req.setRawHeader("If-None-Match", m_etag);
	}
	for (const auto key : m_headers.keys())
	{
		req.setRawHeader(key.toLatin1(), m_headers[key].toLatin1());
	}
	req.setHeader(QNetworkRequest::UserAgentHeader, "MultiLaunch (Cached)");

	QNetworkReply *reply = g_nam->get(req);
	connect(reply, &QNetworkReply::readyRead, this, &CachedDownloadTask::dataReady);
	return reply;
}

void JsonValidator::validate(const QByteArray &data)
{
	Json::ensureDocument(data);
}
void MD5HashValidator::validate(const QByteArray &data)
{
	QCryptographicHash hash(QCryptographicHash::Md5);
	hash.addData(data);
	if (hash.result() != m_expected)
	{
		throw Exception("Invalid MD5 hash: " + hash.result().toHex() + " (expected " + m_expected.toHex() + ")");
	}
}
