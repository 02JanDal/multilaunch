// Licensed under the Apache-2.0 license. See README.md for details.

#include "RawDownloadTask.h"

#include <QNetworkReply>

#include "NetworkAccessManager.h"

RawDownloadTask::RawDownloadTask(const QUrl &url, const QString &contentType, QObject *parent)
	: NetworkTask(parent), m_url(url), m_contentType(contentType)
{
}

void RawDownloadTask::run()
{
	setReply(createReply(m_url));
}

bool RawDownloadTask::next(QNetworkReply *reply)
{
	m_response = reply->readAll();
	return false;
}

QNetworkReply *RawDownloadTask::createReply(const QUrl &url)
{
	QNetworkRequest req(url.isEmpty() ? m_url : url);
	req.setHeader(QNetworkRequest::UserAgentHeader, "MultiLaunch (Uncached)");
	if (m_data.isNull())
	{
		return g_nam->get(req);
	}
	else
	{
		req.setHeader(QNetworkRequest::ContentTypeHeader, m_contentType);
		return g_nam->post(req, m_data);
	}
}
