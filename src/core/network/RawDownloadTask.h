// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "NetworkTask.h"

#include <QUrl>

class RawDownloadTask : public NetworkTask
{
	Q_OBJECT
public:
	explicit RawDownloadTask(const QUrl &url, const QString &contentType, QObject *parent = nullptr);

	void setData(const QByteArray &data) { m_data = data; }

	QByteArray response() const { return m_response; }

private:
	QUrl m_url;
	QString m_contentType;
	QByteArray m_data;
	QByteArray m_response;

	void run() override;
	bool next(QNetworkReply *reply) override;
	QNetworkReply *createReply(const QUrl &url) override;
};
