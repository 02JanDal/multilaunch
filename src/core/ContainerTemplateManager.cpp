// Licensed under the Apache-2.0 license. See README.md for details.

#include "ContainerTemplateManager.h"

DEFINE_SINGLETON(g_containerTemplates)

ContainerTemplateManager::ContainerTemplateManager()
	: AbstractCommonModel<BaseContainerTemplate *>(Qt::Vertical)
{
	addEntry<QString>(&BaseContainerTemplate::name, 0, Qt::DisplayRole);
	addEntry<QString>(&BaseContainerTemplate::icon, 0, Qt::DecorationRole);
}

void ContainerTemplateManager::registerTemplate(BaseContainerTemplate *containerTemplate)
{
	append(containerTemplate);
}
