// Licensed under the Apache-2.0 license. See README.md for details.

#include "GlobalSettings.h"

#include "core/util/Json.h"

DEFINE_SINGLETON(g_settings)

GlobalSettings::GlobalSettings()
	: BaseConfigObject("settings.json")
{
}

void GlobalSettings::registerSetting(const QString &key, const QVariant::Type type, const QVariant &def)
{
	Q_ASSERT_X(type == def.type() || def.isNull(), "GlobalSettings::registerSetting", "Default value type doesn't match item type");
	m_items[key] = Item(key, type, def);
	m_items[key].value = def;
}

void GlobalSettings::set(const QString &key, const QVariant &value)
{
	Q_ASSERT_X(m_items.contains(key), "GlobalSettings::get", qPrintable("Setting key " + key + " has not been registered"));
	Item &item = m_items[key];
	Q_ASSERT_X(item.type == value.type(), "GlobalSettings::set", "Attempting to set invalid type");
	item.value = value;
	scheduleSave();
}
void GlobalSettings::reset(const QString &key)
{
	Q_ASSERT_X(m_items.contains(key), "GlobalSettings::get", qPrintable("Setting key " + key + " has not been registered"));
	Item &item = m_items[key];
	item.value = item.def;
	scheduleSave();
}
QVariant GlobalSettings::get(const QString &key) const
{
	Q_ASSERT_X(m_items.contains(key), "GlobalSettings::get", qPrintable("Setting key " + key + " has not been registered"));
	return m_items[key].value;
}

void GlobalSettings::doLoad(const QByteArray &data)
{
	using namespace Json;
	const QJsonObject root = ensureObject(ensureDocument(data));
	for (auto it = m_items.begin(); it != m_items.end(); ++it)
	{
		Item &item = it.value();
		item.value = ensureIsType<QVariant>(root, it.key(), item.def);
	}
}
QByteArray GlobalSettings::doSave() const
{
	using namespace Json;
	QJsonObject root;
	for (auto it = m_items.constBegin(); it != m_items.constEnd(); ++it)
	{
		const Item &item = it.value();
		if (item.def != item.value)
		{
			root.insert(it.key(), toJson(item.value));
		}
	}
	return toBinary(root);
}
