// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QMetaType>
#include <QEnableSharedFromThis>

#include "core/packages/Version.h"

class BaseVersion : public QEnableSharedFromThis<BaseVersion>
{
public:
	virtual ~BaseVersion() {}

	virtual Version id() const = 0;
	virtual QString type() const = 0;

	virtual void load(const QJsonObject &obj) = 0;
	virtual QJsonObject save() const = 0;

	virtual bool operator>(const QSharedPointer<BaseVersion> &other) const { return id() > other->id(); }
	virtual bool operator==(const QSharedPointer<BaseVersion> &other) const { return id() == other->id(); }
};
Q_DECLARE_METATYPE(QWeakPointer<BaseVersion>)
