// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QString>
#include <QMap>

#include "core/util/Singleton.h"

class Package;
class PackageQuery;
class Version;
class Task;
class BaseVersionList;
class BaseComponent;
class Container;

class BasePackageHandler
{
public:
	virtual ~BasePackageHandler() {}

	virtual Task *createFetchInformationTask(const Package &package) const = 0;
	virtual BaseVersionList *getVersionsList(const Package &package) const = 0;
	virtual BaseComponent *createComponent(const Package &package, Container *container) const = 0;
	virtual QList<PackageQuery> getPackageDependencies(const Package &package, const Version &version) const = 0;
};

class PackageHandlerRegistry
{
public:
	explicit PackageHandlerRegistry();

	void registerHandler(const QString &ns, BasePackageHandler *handler);
	void registerHandler(const QStringList &ns, BasePackageHandler *handler);

	BasePackageHandler *handler(const Package &package) const;

private:
	QMap<QString, BasePackageHandler *> m_handlers;
};
DECLARE_SINGLETON(PackageHandlerRegistry, g_packages)
