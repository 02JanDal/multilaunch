// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QString>
#include <QMetaType>

#include "core/util/Exception.h"

DECLARE_EXCEPTION(Version);

class Version
{
public:
	explicit Version();

	QString toString() const;
	bool isValid(const bool strictParsing = false) const;
	bool isInfinity() const;

	bool operator<(const Version &other) const;
	bool operator<=(const Version &other) const;
	bool operator>(const Version &other) const;
	bool operator>=(const Version &other) const;
	bool operator==(const Version &other) const;
	bool operator!=(const Version &other) const;

	static Version fromString(const QString &string, const bool strictParsing = false);

private:
	QString m_rawString;
	QVector<unsigned int> m_sections;
	QString m_appendix;
};
Q_DECLARE_METATYPE(Version)

class VersionInterval
{
public:
	explicit VersionInterval(const Version &lower, const Version &higher, const bool lowerInclusive, const bool higherInclusive);
	explicit VersionInterval(const Version &exact);
	explicit VersionInterval();

	bool contains(const Version &other) const;

	bool operator==(const VersionInterval &other) const;

	/// @throws VersionException
	static VersionInterval fromString(const QString &string);
	/// @throws VersionException
	static QList<VersionInterval> fromStringMultiple(const QString &string);

	QString toString() const;

private:
	Version m_lower;
	Version m_higher;
	bool m_lowerInclusive;
	bool m_higherInclusive;
};
Q_DECLARE_METATYPE(VersionInterval)
