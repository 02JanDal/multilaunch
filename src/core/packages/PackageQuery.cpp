// Licensed under the Apache-2.0 license. See README.md for details.

#include "PackageQuery.h"

PackageQuery::PackageQuery(const Package &package, const QList<VersionInterval> &versions)
	: m_package(package), m_versions(versions)
{
}
PackageQuery::PackageQuery()
{
}

PackageQuery PackageQuery::intersected(const PackageQuery &other) const
{
	Q_ASSERT(m_package == other.package());
	return PackageQuery(m_package, m_versions + other.versions()); // TODO actual intersecting
}

QString PackageQuery::toString() const
{
	if (m_versions.isEmpty())
	{
		return m_package.toString();
	}
	else
	{
		QStringList vLists;
		for (const auto versions : m_versions)
		{
			vLists.append(versions.toString());
		}
		return m_package.toString() + ':' + vLists.join(';');
	}
}

PackageQuery PackageQuery::fromString(const QString &string)
{
	const QStringList sections = string.split(':', QString::KeepEmptyParts);
	switch (sections.size())
	{
	case 0: return PackageQuery();
	case 1: return PackageQuery(Package(sections.at(0), QString()), QList<VersionInterval>());
	case 2: return PackageQuery(Package(sections.at(0), sections.at(1)), QList<VersionInterval>());
	case 3:
	default:
		return PackageQuery(Package(sections.at(0), sections.at(1)), VersionInterval::fromStringMultiple(sections.at(2)));
	}
}

QDebug operator<<(QDebug dbg, const PackageQuery &query)
{
	dbg << QString("PackageQuery(%1)").arg(query.toString()).toLatin1().constData();
	return dbg;
}
