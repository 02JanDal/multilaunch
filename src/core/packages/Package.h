// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QString>
#include <QMetaType>

class Package
{
public:
	explicit Package(const QString &ns, const QString &id = QString());
	explicit Package();

	QString ns() const { return m_ns; }
	QString id() const { return m_id; }

	bool isValid() const { return !m_ns.isEmpty(); }

	bool operator==(const Package &other) const;
	bool operator<(const Package &other) const;

	QString toString() const;
	static Package fromString(const QString &string);

private:
	QString m_ns;
	QString m_id;
};
QDebug operator<<(QDebug dbg, const Package &pkg);
Q_DECLARE_METATYPE(Package)
