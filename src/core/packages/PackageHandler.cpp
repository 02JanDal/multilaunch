// Licensed under the Apache-2.0 license. See README.md for details.

#include "PackageHandler.h"

#include <QStringList>

#include "Package.h"

DEFINE_SINGLETON(g_packages)

PackageHandlerRegistry::PackageHandlerRegistry()
{
}

void PackageHandlerRegistry::registerHandler(const QString &ns, BasePackageHandler *handler)
{
	m_handlers.insert(ns, handler);
}
void PackageHandlerRegistry::registerHandler(const QStringList &ns, BasePackageHandler *handler)
{
	for (const auto namespcs : ns)
	{
		m_handlers.insert(namespcs, handler);
	}
}
BasePackageHandler *PackageHandlerRegistry::handler(const Package &package) const
{
	return m_handlers.value(package.ns());
}
