// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QList>
#include "Package.h"
#include "Version.h"

class PackageQuery
{
public:
	explicit PackageQuery(const Package &package, const QList<VersionInterval> &versions = QList<VersionInterval>());
	explicit PackageQuery();

	Package package() const { return m_package; }
	void setPackage(const Package &package) { m_package = package; }

	QList<VersionInterval> versions() const { return m_versions; }
	void setVersions(const QList<VersionInterval> &versions) { m_versions = versions; }

	PackageQuery intersected(const PackageQuery &other) const;

	QString toString() const;
	static PackageQuery fromString(const QString &string);

private:
	Package m_package;
	QList<VersionInterval> m_versions;
};
QDebug operator<<(QDebug dbg, const PackageQuery &query);
Q_DECLARE_METATYPE(PackageQuery)
