// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "PackageHandler.h"

class CorePackageHandler : public BasePackageHandler
{
public:
	Task *createFetchInformationTask(const Package &package) const override;
	BaseVersionList *getVersionsList(const Package &package) const override;
	BaseComponent *createComponent(const Package &package, Container *container) const override;
	QList<PackageQuery> getPackageDependencies(const Package &package, const Version &version) const override;
};
