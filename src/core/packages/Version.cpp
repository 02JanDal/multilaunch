// Licensed under the Apache-2.0 license. See README.md for details.

#include "Version.h"

#include <QRegularExpression>

static unsigned int appendixToNumber(const QString &appendix)
{
	const QString tmp = QString(appendix).remove(QRegularExpression("\\d*$"));
	if (tmp == "alpha")
	{
		return 1;
	}
	else if (tmp == "beta")
	{
		return 2;
	}
	else if (tmp == "pre" || tmp == "rc")
	{
		return 3;
	}
	else
	{
		return 0;
	}
}
template<typename Comparator, typename StringComp>
static inline bool compareVersions(const QVector<unsigned int> &a, const QString &aAppendix, const QVector<unsigned int> &b,
								   const QString &bAppendix, Comparator comp, StringComp strComp)
{
	const int numIterations = qMax(a.size(), b.size());
	for (int i = 0; i < numIterations; ++i)
	{
		unsigned int aNum, bNum;
		if (a.size() <= i)
		{
			aNum = 0;
		}
		else
		{
			aNum = a.at(i);
		}
		if (b.size() <= i)
		{
			bNum = 0;
		}
		else
		{
			bNum = b.at(i);
		}
		if (aNum != bNum)
		{
			return comp(aNum, bNum);
		}
	}
	const unsigned int appTextA = appendixToNumber(aAppendix);
	const unsigned int appTextB = appendixToNumber(bAppendix);
	if (appTextA > 0 && appTextB > 0)
	{
		if (appTextA == appTextB)
		{
			QRegularExpression exp("\\d$");
			return comp(exp.match(aAppendix).captured().toUInt(), exp.match(bAppendix).captured().toUInt());
		}
		else
		{
			return comp(appTextA, appTextB);
		}
	}
	else
	{
		if (aAppendix.isEmpty() && !bAppendix.isEmpty())
		{
			return comp(1, 0);
		}
		else if (!aAppendix.isEmpty() && bAppendix.isEmpty())
		{
			return comp(0, 1);
		}
		else
		{
			return strComp(aAppendix, bAppendix);
		}
	}
}

Version::Version()
	: m_sections(QVector<unsigned int>() << UINT_MAX)
{
}

Version Version::fromString(const QString &string, const bool strictParsing)
{
	Version out;
	out.m_rawString = string;

	if (string.isEmpty())
	{
		return out;
	}

	QString cleaned = QString(string).replace('_', '-');
	const int appendixStart = cleaned.indexOf('-');
	if (appendixStart > 0)
	{
		out.m_appendix = cleaned.mid(appendixStart).remove('-');
		cleaned = cleaned.left(appendixStart);
	}
	const QStringList sections = cleaned.split('.');
	out.m_sections.resize(sections.size());
	for (int i = 0; i < sections.size(); ++i)
	{
		bool ok = false;
		out.m_sections[i] = sections.at(i).toUInt(&ok);
		if (!ok)
		{
			if (strictParsing)
			{
				throw VersionException("Version section cannot be converted to an integer");
			}
			else
			{
				Version raw;
				raw.m_rawString = string;
				return raw;
			}
		}
	}
	return out;
}

QString Version::toString() const
{
	return m_rawString;
}
bool Version::isValid(const bool strictParsing) const
{
	if (strictParsing)
	{
		return !m_sections.isEmpty();
	}
	else
	{
		return !m_rawString.isEmpty();
	}
}
bool Version::isInfinity() const
{
	return m_sections.size() == 1 && m_sections.first() == UINT_MAX;
}

bool Version::operator<(const Version &other) const
{
	if (isValid(true) && other.isValid(true))
	{
		return compareVersions(m_sections, m_appendix, other.m_sections, other.m_appendix, std::less<unsigned int>(), std::less<QString>());
	}
	else
	{
		return m_rawString < other.m_rawString;
	}
}
bool Version::operator<=(const Version &other) const
{
	if (isValid(true) && other.isValid(true))
	{
		return compareVersions(m_sections, m_appendix, other.m_sections, other.m_appendix, std::less_equal<unsigned int>(), std::less_equal<QString>());
	}
	else
	{
		return m_rawString <= other.m_rawString;
	}
}
bool Version::operator>(const Version &other) const
{
	if (isValid(true) && other.isValid(true))
	{
		return compareVersions(m_sections, m_appendix, other.m_sections, other.m_appendix, std::greater<unsigned int>(), std::greater<QString>());
	}
	else
	{
		return m_rawString > other.m_rawString;
	}
}
bool Version::operator>=(const Version &other) const
{
	if (isValid(true) && other.isValid(true))
	{
		return compareVersions(m_sections, m_appendix, other.m_sections, other.m_appendix, std::greater_equal<unsigned int>(), std::greater_equal<QString>());
	}
	else
	{
		return m_rawString >= other.m_rawString;
	}
}
bool Version::operator==(const Version &other) const
{
	if (isValid(true) && other.isValid(true))
	{
		return compareVersions(m_sections, m_appendix, other.m_sections, other.m_appendix, std::equal_to<unsigned int>(), std::equal_to<QString>());
	}
	else
	{
		return m_rawString == other.m_rawString;
	}
}
bool Version::operator!=(const Version &other) const
{
	if (isValid(true) && other.isValid(true))
	{
		return compareVersions(m_sections, m_appendix, other.m_sections, other.m_appendix, std::not_equal_to<unsigned int>(), std::not_equal_to<QString>());
	}
	else
	{
		return m_rawString != other.m_rawString;
	}
}

VersionInterval::VersionInterval(const Version &lower, const Version &higher, const bool lowerInclusive, const bool higherInclusive)
	: m_lower(lower), m_higher(higher), m_lowerInclusive(lowerInclusive), m_higherInclusive(higherInclusive)
{
}
VersionInterval::VersionInterval(const Version &exact)
	: m_lower(exact), m_higher(exact), m_lowerInclusive(true), m_higherInclusive(true)
{
}
VersionInterval::VersionInterval()
{
}

bool VersionInterval::operator==(const VersionInterval &other) const
{
	return m_lowerInclusive == other.m_lowerInclusive &&
			m_higherInclusive == other.m_higherInclusive &&
			m_lower == other.m_lower && m_higher == other.m_higher;
}

VersionInterval VersionInterval::fromString(const QString &string)
{
	if (string.isEmpty())
	{
		return VersionInterval();
	}
	else if (!string.contains(','))
	{
		return VersionInterval(Version::fromString(string, true));
	}

	if (string.size() < 4)
	{
		throw VersionException("Interval is not valid: Doesn't contain ',' or is less than 4 characters (\"" + string + "\")");
	}

	VersionInterval out;
	const QString lower = string.left(string.indexOf(','));
	const QString higher = string.mid(string.indexOf(',') + 1);
	if (lower.isEmpty() || higher.isEmpty())
	{
		throw VersionException("Interval lower or higher section are empty");
	}

	if (lower.startsWith('['))
	{
		out.m_lowerInclusive = true;
	}
	else if (lower.startsWith('('))
	{
		out.m_lowerInclusive = false;
	}
	else
	{
		throw VersionException(QString("Interval starting character is not valid (expected: [ or (, actual: %1)").arg(lower.at(0)));
	}
	out.m_lower = Version::fromString(QString(lower).remove(0, 1), true);

	if (higher.endsWith(']'))
	{
		out.m_higherInclusive = true;
	}
	else if (higher.endsWith(')'))
	{
		out.m_higherInclusive = false;
	}
	else
	{
		throw VersionException(QString("Interval starting character is not valid (expected: [ or (, actual: %1)").arg(lower.at(higher.size() - 1)));
	}
	out.m_higher = Version::fromString(QString(higher).remove(higher.size() - 1, 1), true);

	return out;
}
QList<VersionInterval> VersionInterval::fromStringMultiple(const QString &string)
{
	QList<VersionInterval> intervals;
	for (const auto part : string.split(';'))
	{
		intervals.append(fromString(part));
	}
	return intervals;
}

QString VersionInterval::toString() const
{
	if (m_lower == m_higher && m_lowerInclusive && m_higherInclusive)
	{
		return m_lower.toString();
	}
	else
	{
		QString out;
		out += m_lowerInclusive ? '[' : '(';
		if (!m_lower.isInfinity())
		{
			out += m_lower.toString();
		}
		out += ',';
		if (!m_higher.isInfinity())
		{
			out += m_higher.toString();
		}
		out += m_higherInclusive ? ']' : ')';
		return out;
	}
}

bool VersionInterval::contains(const Version &other) const
{
	// check lower bounds
	if (!m_lower.isInfinity())
	{
		if (m_lowerInclusive)
		{
			if (m_lower > other)
			{
				return false;
			}
		}
		else if (m_lower >= other)
		{
			return false;
		}
	}

	// check higher bounds
	if (!m_higher.isInfinity())
	{
		if (m_higherInclusive)
		{
			if (m_higher < other)
			{
				return false;
			}
		}
		else if (m_higher <= other)
		{
			return false;
		}
	}

	return true;
}
