// Licensed under the Apache-2.0 license. See README.md for details.

#include "CorePackageHandler.h"

#include "core/Container.h"
#include "core/components/FolderComponent.h"

Task *CorePackageHandler::createFetchInformationTask(const Package &package) const
{
	return nullptr;
}

BaseVersionList *CorePackageHandler::getVersionsList(const Package &package) const
{
	return nullptr;
}

BaseComponent *CorePackageHandler::createComponent(const Package &package, Container *container) const
{
	if (package.ns() == "core.folder")
	{
		FolderComponent *comp = new FolderComponent(container);
		comp->setDir(container->rootDir().absoluteFilePath(package.id()));
		return comp;
	}
	else
	{
		return nullptr;
	}
}

QList<PackageQuery> CorePackageHandler::getPackageDependencies(const Package &package, const Version &version) const
{
	return QList<PackageQuery>();
}
