// Licensed under the Apache-2.0 license. See README.md for details.

#include "Package.h"

Package::Package(const QString &ns, const QString &id)
	: m_ns(ns), m_id(id)
{
}
Package::Package()
{
}

bool Package::operator==(const Package &other) const
{
	return m_ns == other.m_ns && m_id == other.m_id;
}
bool Package::operator<(const Package &other) const
{
	return (m_ns + ':' + m_id) < (other.m_ns + ':' + other.m_id);
}

QString Package::toString() const
{
	if (m_ns.isEmpty() && m_id.isEmpty())
	{
		return QString();
	}
	else if (m_id.isEmpty())
	{
		return m_ns;
	}
	else
	{
		return m_ns + ':' + m_id;
	}
}
Package Package::fromString(const QString &string)
{
	const int midIndex = string.indexOf(':');
	if (midIndex > 0)
	{
		return Package(string.left(midIndex), string.mid(midIndex + 1));
	}
	else
	{
		return Package(string, QString());
	}
}

QDebug operator<<(QDebug dbg, const Package &pkg)
{
	dbg << QString("Package(%1)").arg(pkg.toString()).toLatin1().constData();
	return dbg;
}
