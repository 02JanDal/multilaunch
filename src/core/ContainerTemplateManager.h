// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/util/AbstractCommonModel.h"
#include "core/util/Singleton.h"

class QDir;
class Container;
class BaseVersion;
class BaseVersionList;

class BaseContainerTemplate
{
public:
	virtual ~BaseContainerTemplate() {}

	virtual QString name() const = 0;
	virtual QString icon() const = 0;
	virtual Container *createContainer(const QDir &root, const QSharedPointer<BaseVersion> &version) const = 0;
	virtual BaseVersionList *versionsList() const = 0;
};

class ContainerTemplateManager : public AbstractCommonModel<BaseContainerTemplate *>
{
public:
	explicit ContainerTemplateManager();

	void registerTemplate(BaseContainerTemplate *containerTemplate);
};
DECLARE_SINGLETON(ContainerTemplateManager, g_containerTemplates)
