// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/util/Singleton.h"

class QCoreApplication;

class Cli
{
public:
	void handle(QCoreApplication &app);
};
DECLARE_SINGLETON(Cli, g_cli)
