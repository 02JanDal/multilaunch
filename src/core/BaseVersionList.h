// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QAbstractListModel>
#include <QSharedPointer>

#include "core/util/BaseConfigObject.h"

class BaseVersion;
class Version;
class Task;
class Container;

class IVersionFactory
{
public:
	virtual ~IVersionFactory() {}

	virtual BaseVersion *create() const = 0;
};

template<typename T>
class VersionFactory : public IVersionFactory
{
public:
	BaseVersion *create() const override { return new T; }
};

class BaseVersionList : public QAbstractListModel, public BaseConfigObject
{
	Q_OBJECT
public:
	explicit BaseVersionList(const QString &savePath, IVersionFactory *factory, QObject *parent = nullptr);

	int rowCount(const QModelIndex &parent) const override;
	int columnCount(const QModelIndex &parent) const override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;
	QVariant data(const QModelIndex &index, int role) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

	// TODO replace by depres
	virtual bool shouldShow(const int index, Container *container) const { return true; }

	virtual Task *createRefreshTask() = 0;

	int size() const { return m_versions.size(); }
	int indexOf(const QSharedPointer<BaseVersion> &version) const;
	QSharedPointer<BaseVersion> at(const int index) const { return m_versions.at(index); }
	QSharedPointer<BaseVersion> findVersion(const Version &id) const;
	QList<Version> versions() const;

protected:
	void setVersions(const QList<QSharedPointer<BaseVersion>> &versions);

private:
	IVersionFactory *m_factory;
	QList<QSharedPointer<BaseVersion>> m_versions;

	void doLoad(const QByteArray &data) override;
	QByteArray doSave() const override;
};
