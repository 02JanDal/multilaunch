// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QAbstractItemModel>

#include "core/util/Singleton.h"
#include "core/util/BaseConfigObject.h"

class Container;

class ContainerModel : public QAbstractItemModel, public BaseConfigObject
{
	Q_OBJECT
public:
	explicit ContainerModel(QObject *parent = nullptr);
	~ContainerModel();

	QModelIndex index(int row, int column, const QModelIndex &parent) const override;
	QModelIndex parent(const QModelIndex &child) const override;
	int rowCount(const QModelIndex &parent) const override;
	int columnCount(const QModelIndex &parent) const override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;

	QVariant data(const QModelIndex &index, int role) const override;
	bool setData(const QModelIndex &index, const QVariant &value, int role) override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

	QStringList mimeTypes() const override;
	QMimeData *mimeData(const QModelIndexList &indexes) const override;
	bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const override;
	bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;
	Qt::DropActions supportedDropActions() const override;
	Qt::DropActions supportedDragActions() const override;

	void addGroup(const QString &name, const QString &group = QString());
	void addContainer(Container *container, const QString &group = QString());
	void deleteContainer(Container *container);
	void renameGroup(const QString &oldName, const QString &newName);
	void changeContainerGroup(Container *container, const QString &newGroup);

	Container *container(const QModelIndex &index) const;
	Container *container(const QUuid &uuid) const;
	QString group(const QModelIndex &index) const;
	QString group(Container *container) const;

	QList<Container *> containers() const;

	bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild) override;

private:
	struct Group
	{
		QString name;
	};
	struct Node
	{
		~Node() { if (group) delete group; }
		Container *container = nullptr;
		Group *group = nullptr;
		QList<Node *> children;
		Node *parent = nullptr;
	};
	Node *m_rootNode = nullptr;
	QMap<QString, Node *> m_groups;
	QMap<Container *, Node *> m_containers;

	QModelIndex index(Node *node) const;
	void addNode(Node *node, const QString &group);

	void doLoad(const QByteArray &data) override;
	QByteArray doSave() const override;
};
DECLARE_SINGLETON(ContainerModel, g_containerModel)
