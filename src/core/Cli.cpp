// Licensed under the Apache-2.0 license. See README.md for details.

#include "Cli.h"

#include <QCommandLineParser>
#include <QTextStream>
#include <QEventLoop>
#include <QTimer>
#include <cstdio>

#include "core/ContainerModel.h"
#include "core/Container.h"
#include "core/ContainerTemplateManager.h"
#include "core/BaseVersionList.h"
#include "core/packages/PackageHandler.h"
#include "core/tasks/Task.h"
#include "core/tasks/FlowTask.h"
#include "core/util/CliUtils.h"
#include "core/util/FileSystem.h"
#include "core/BaseProcess.h"

DEFINE_SINGLETON(g_cli)

static QTextStream *errPtr;

static void quietMessageHandler(const QtMsgType type, const QMessageLogContext &ctxt, const QString &msg)
{
	if (type != QtDebugMsg)
	{
		*errPtr << msg << endl;
	}
}

void Cli::handle(QCoreApplication &app)
{
	// TODO interactive CLI interface (ncurses?)

	QCommandLineParser parser;
	parser.addHelpOption();
	parser.addVersionOption();
	parser.setApplicationDescription("MultiLaunch is a launcher/manager for games");
	parser.addOption(QCommandLineOption("list-containers", "List all available containers"));
	parser.addOption(QCommandLineOption("container", "Which container to interact on (when no action is specified the container will be selected in the gui)",
										"UUID"));
	parser.addOption(QCommandLineOption("launch", "Launches the specified container"));
	parser.addOption(QCommandLineOption("abort", "Abort the process after a given period of time", "SECONDS"));
	parser.addOption(QCommandLineOption("list-packages", "List packages contained in the container"));
	parser.addOption(QCommandLineOption("add-package", "Adds a package to the container", "PACKAGE-QUERY"));
	parser.addOption(QCommandLineOption("remove-package", "Removes a package from the container", "PACKAGE"));
	parser.addOption(QCommandLineOption("remove", "Removes the container"));
	parser.addOption(QCommandLineOption("list-templates", "Returns a list of container templates"));
	parser.addOption(QCommandLineOption("create", "Creates a new container from a given template. Requires --template and --template-version", "NAME"));
	parser.addOption(QCommandLineOption("template", "Uses the specified template for --create(ing) a container or when listing --template-versions", "NAME"));
	parser.addOption(QCommandLineOption("template-version", "The version of the --template to use for --create", "VERSION"));
	parser.addOption(QCommandLineOption("group", "Which group to add the newly --create(d) template to", "GROUP"));
	parser.addOption(QCommandLineOption("template-versions", "Get a list of possible versions for a --template"));
	parser.addOption(QCommandLineOption("bare", "Outputs stuff in a less readable manner for further processing"));
	parser.addOption(QCommandLineOption("quiet", "Produce less output"));
	parser.process(app);

	QTextStream out(stdout);
	QTextStream err(stderr);
	errPtr = &err;

	const bool bare = parser.isSet("bare");

	if (parser.isSet("quiet"))
	{
		qInstallMessageHandler(&quietMessageHandler);
	}

	auto exit = [&out, &err](const int code = 0)
	{
		err << flush;
		out << flush;
		// ensure QCoreApplication::aboutToQuit gets emitted
		qApp->metaObject()->invokeMethod(qApp, "quit", Qt::QueuedConnection);
		qApp->exec();
		::exit(code);
	};
	auto error = [&out, &err, &exit](const QString &message)
	{
		err << message << endl << flush;
		exit(1);
	};
	auto runTask = [&error](Task *task)
	{
		QEventLoop loop;
		QObject::connect(task, &Task::finished, &loop, &QEventLoop::quit);
		task->start();
		if (task->isRunning())
		{
			loop.exec();
		}
		if (task->isFailure())
		{
			error(task->errorString());
		}
		delete task;
	};

	if (parser.isSet("list-containers"))
	{
		QList<QStringList> table;
		for (const auto container : g_containerModel->containers())
		{
			table.append(QStringList() << container->name()
						 << container->type()
						 << container->version().toString()
						 << container->uuid().toString().remove('{').remove('}'));
		}
		CliUtils::writeTable(out, bare, QStringList() << "Name" << "Type" << "Version" << "UUID", table, 4);
		exit();
	}
	if (parser.isSet("list-templates"))
	{
		QList<QStringList> table;
		for (int i = 0; i < g_containerTemplates->size(); ++i)
		{
			table.append(QStringList() << QString::number(i) << g_containerTemplates->get(i)->name());
		}
		CliUtils::writeTable(out, bare, QStringList() << "Index" << "Name", table, 2);
		exit();
	}
	if (parser.isSet("template-versions"))
	{
		if (!parser.isSet("template"))
		{
			error("--template-versions requires the --template argument");
		}
		BaseContainerTemplate *temp = nullptr;
		for (int i = 0; i < g_containerTemplates->size(); ++i)
		{
			if (g_containerTemplates->get(i)->name() == parser.value("template"))
			{
				temp = g_containerTemplates->get(i);
				break;
			}
		}
		if (!temp)
		{
			error("The given --template could not be found. Try --list-templates.");
		}
		BaseVersionList *list = temp->versionsList();
		runTask(list->createRefreshTask());
		CliUtils::writeTable(out, bare, list);
		exit();
	}

	if (parser.isSet("container") && parser.isSet("create"))
	{
		error("You can only specify either --container or --create");
	}
	if (parser.isSet("create") && !parser.isSet("template"))
	{
		error("The --create argument requires the --template argument");
	}
	if (parser.isSet("create") && !parser.isSet("template-version"))
	{
		error("The --create argument requires the --template-version argument");
	}

	Container *container = nullptr;
	if (parser.isSet("container"))
	{
		container = g_containerModel->container(QUuid('{' + parser.value("container") + '}'));
		if (!container)
		{
			error("The specified container UUID could not be found");
		}
	}
	if (parser.isSet("create") && parser.isSet("template") && parser.isSet("template-version"))
	{
		BaseContainerTemplate *temp = nullptr;
		for (int i = 0; i < g_containerTemplates->size(); ++i)
		{
			if (g_containerTemplates->get(i)->name() == parser.value("template"))
			{
				temp = g_containerTemplates->get(i);
				break;
			}
		}
		if (!temp)
		{
			error("The given --template could not be found. Try --list-templates.");
		}
		QSharedPointer<BaseVersion> version = temp->versionsList()->findVersion(Version::fromString(parser.value("template-version")));
		const QDir dir(QDir::current().absoluteFilePath("containers/%1").arg(parser.value("create").toLower()));
		FS::ensureExists(dir);
		container = temp->createContainer(dir, version);
		container->setName(parser.value("create"));
		g_containerModel->addContainer(container, parser.value("group"));
		out << container->uuid().toString().remove('{').remove('}') << endl;
		runTask(new FlowTask(FlowTask::Creation, container));
	}

	if (parser.isSet("launch") || parser.isSet("list-packages") || parser.isSet("add-package") || parser.isSet("remove-package") || parser.isSet("remove"))
	{
		if (!container)
		{
			error("One or more of the specified actions require the --container argument");
		}
	}

	if (parser.isSet("remove"))
	{
		out << "Deleting " << container->name() << "..." << endl;
		g_containerModel->deleteContainer(container);
		exit();
	}
	for (const auto query : parser.values("add-package"))
	{
		const PackageQuery q = PackageQuery::fromString(query);
		BasePackageHandler *handler = g_packages->handler(q.package());
		if (handler)
		{
			BaseComponent *component = nullptr;
			for (const auto comp : container->allChildren())
			{
				if (comp->package() == q.package())
				{
					component = comp;
				}
			}
			if (!component)
			{
				component = handler->createComponent(q.package(), container);
				container->addChild(component);
			}
			if (!component)
			{
				error("Invalid package namespace/ID (package handler didn't create a component) for " + query);
			}
			runTask(handler->createFetchInformationTask(q.package()));
			if (!q.versions().isEmpty())
			{
				// get and clean a list of versions
				BaseVersionList *list = handler->getVersionsList(q.package());
				QList<Version> versions;
				if (list)
				{
					// TODO merge with code in DependencyResolutionTask
					versions = list->versions();
					QMutableListIterator<Version> i(versions);
					while (i.hasNext())
					{
						const Version v = i.next();
						bool isContained = false;
						for (const auto interval : q.versions())
						{
							if (interval.contains(v))
							{
								isContained = true;
								break;
							}
						}
						if (!isContained)
						{
							i.remove();
						}
					}
				}
				else
				{
					versions = QList<Version>() << Version();
				}

				if (versions.isEmpty())
				{
					error("No available versions matched for " + query);
				}
				else
				{
					std::sort(versions.begin(), versions.end());
					if (versions.first().isValid())
					{
						component->setVersion(handler->getVersionsList(q.package())->findVersion(versions.first()));
					}
				}
			}
		}
		else
		{
			error("Invalid package namespace/ID (no known package handler) for " + query);
		}
	}
	for (const auto package : parser.values("remove-package"))
	{
		const Package pkg = Package::fromString(package);
		BaseComponent *component = nullptr;
		for (const auto comp : container->allChildren())
		{
			if (comp->package() == pkg)
			{
				component = comp;
			}
		}
		if (!component)
		{
			error(package + " is not contained in " + container->name());
		}
		if (!component->canRemove())
		{
			error(package + " is not removable");
		}
		component->remove();
	}
	if (parser.isSet("list-packages"))
	{
		QList<QStringList> table;
		for (const auto comp : container->allChildren())
		{
			if (comp->package().isValid())
			{
				table.append(QStringList() << comp->package().toString() << comp->version().toString());
			}
		}
		CliUtils::writeTable(out, bare, QStringList() << "Package" << "Version", table, 2);
		exit();
	}
	if (parser.isSet("launch"))
	{
		FlowTask *task = new FlowTask(FlowTask::Launch, container);
		QObject::connect(task, &FlowTask::interactionRequired, [&out, &error, task, &parser](const Stage stage, IStageData *data)
		{
			if (stage == Stage::UpdateCheck)
			{
				task->interactionFinished(true);
			}
			else if (stage == Stage::Launch)
			{
				BaseLaunchData *d = dynamic_cast<BaseLaunchData *>(data);
				QObject::connect(d->process, &BaseProcess::log, [&out](const QString &message, const BaseProcess::LogLevel level)
				{
					CliUtils::writeLogFormatted(out, message, level);
				});
				d->process->start();
				if (parser.isSet("abort"))
				{
					QTimer::singleShot(parser.value("abort").toInt() * 1000, d->process, &BaseProcess::abort);
				}
			}
		});
		runTask(task);
		exit();
	}

	if (parser.isSet("create"))
	{
		exit();
	}

	if (container)
	{
		// TODO select 'container' in the gui
	}
}
