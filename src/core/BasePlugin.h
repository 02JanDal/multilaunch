// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#define QT_STATICPLUGIN
#include <QtPlugin>

template<typename T>
class QList;
class IPage;
class IPagePart;

class BasePlugin
{
public:
	virtual ~BasePlugin() {}

	virtual void init() = 0;

	virtual QList<IPage *> createGlobalPages() const = 0;
	virtual QList<IPagePart *> createGlobalPageParts() const = 0;
};
Q_DECLARE_INTERFACE(BasePlugin, "jan.dalheimer.MultiLaunch.BasePlugin")
