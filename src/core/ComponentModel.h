// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QAbstractItemModel>

class BaseComponent;

class ComponentModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	explicit ComponentModel(BaseComponent *root, QObject *parent = nullptr);

	QModelIndex index(int row, int column, const QModelIndex &parent) const override;
	QModelIndex parent(const QModelIndex &child) const override;
	int rowCount(const QModelIndex &parent) const override;
	int columnCount(const QModelIndex &parent) const override;
	QVariant data(const QModelIndex &index, int role) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;

	BaseComponent *component(const QModelIndex &index) const;

private slots:
	void componentChildrenChanged();

	void componentNameChanged();
	void componentTypeChanged();
	void componentVersionChanged();

private:
	BaseComponent *m_root;
	QList<BaseComponent *> m_setupComponents;
	void ensureIsSetup(BaseComponent *component);

	QModelIndex index(BaseComponent *component, const int column = 0) const;
};
