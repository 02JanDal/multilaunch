// Licensed under the Apache-2.0 license. See README.md for details.

#include "AuthComponent.h"

#include "core/accounts/AccountModel.h"
#include "core/accounts/BaseAccountType.h"
#include "core/tasks/AuthTask.h"

AuthComponent::AuthComponent(const QString &type, BaseComponent *parent)
	: BaseComponent(parent), m_type(type)
{
}

QString AuthComponent::name() const
{
	return g_accounts->type(m_type)->text();
}
Task *AuthComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::PreLaunch)
	{
		return new AuthTask(m_type, data->container, dynamic_cast<PreLaunchData *>(data));
	}
	else
	{
		return nullptr;
	}
}
