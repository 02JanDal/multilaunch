// Licensed under the Apache-2.0 license. See README.md for details.

#include "FolderComponent.h"

#include "core/util/FileSystem.h"
#include "core/util/Json.h"
#include "core/Container.h"
#include "core/IPage.h"
#include "core/Gui.h"

FolderComponent::FolderComponent(BaseComponent *parent)
	: BaseComponent(parent)
{
}

void FolderComponent::remove()
{
	BaseComponent::remove();
	FS::remove(m_dir);
}

void FolderComponent::setDir(const QDir &dir)
{
	setPackage(Package("core.folder", dir.dirName()));
	setName(dir.dirName());
	m_dir = dir;
	if (!FS::exists(m_dir))
	{
		FS::ensureExists(m_dir);
	}
}

QJsonObject FolderComponent::save() const
{
	QJsonObject obj = BaseComponent::save();
	obj.insert("directory", Json::toJson(container()->rootDir().relativeFilePath(m_dir.absolutePath())));
	return obj;
}
void FolderComponent::load(const QJsonObject &obj)
{
	setDir(container()->rootDir().absoluteFilePath(Json::ensureIsType<QString>(obj, "directory")));
	BaseComponent::load(obj);
}

QList<IPage *> FolderComponent::createPages(const bool isRunning)
{
	if (!isRunning)
	{
		return QList<IPage *>() << g_gui->wait<IPage *>("CreateFolderPage", this);
	}
	else
	{
		return QList<IPage *>();
	}
}
