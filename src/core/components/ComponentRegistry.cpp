// Licensed under the Apache-2.0 license. See README.md for details.

#include "ComponentRegistry.h"

#include "FolderComponent.h"
#include "FileDownloadComponent.h"

DEFINE_SINGLETON(g_componentRegistry)

ComponentRegistry::ComponentRegistry()
{
	registerComponent<FolderComponent>("Core.Folder");
	registerComponent<FileDownloadComponent>("Core.FileDownload");
}

BaseComponent *ComponentRegistry::create(const QString &id, BaseComponent *parent) const
{
	Q_ASSERT(m_factories.contains(id));
	return m_factories[id]->create(parent);
}
