// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "BaseComponent.h"
#include "core/util/Singleton.h"

#include <QHash>

class IComponentFactory
{
public:
	virtual ~IComponentFactory() {}
	virtual BaseComponent *create(BaseComponent *parent) = 0;
};

template<typename T>
class ComponentFactory : public IComponentFactory
{
public:
	explicit ComponentFactory() {}

	BaseComponent *create(BaseComponent *parent) override
	{
		return new T(parent);
	}
};

class ComponentRegistry
{
public:
	explicit ComponentRegistry();

	template<typename T>
	void registerComponent(const QString &id)
	{
		m_factories.insert(id, new ComponentFactory<T>());
	}

	BaseComponent *create(const QString &id, BaseComponent *parent) const;

private:
	QHash<QString, IComponentFactory *> m_factories;
};
DECLARE_SINGLETON(ComponentRegistry, g_componentRegistry)
