// Licensed under the Apache-2.0 license. See README.md for details.

#include "FileDownloadComponent.h"

#include "core/network/CachedDownloadTask.h"

FileDownloadComponent::FileDownloadComponent(BaseComponent *parent)
	: BaseComponent(parent)
{
}

Task *FileDownloadComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::Download)
	{
		CachedDownloadTask *task = new CachedDownloadTask(tr("%1").arg(name()), m_group, m_id, m_url);
		task->setValidator(m_validator);
		return task;
	}
	else
	{
		return nullptr;
	}
}
