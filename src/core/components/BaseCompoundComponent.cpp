// Licensed under the Apache-2.0 license. See README.md for details.

#include "BaseCompoundComponent.h"

#include <QJsonObject>
#include <QJsonArray>

#include "core/tasks/GroupedTask.h"
#include "core/util/Json.h"
#include "ComponentRegistry.h"

BaseCompoundComponent::BaseCompoundComponent(BaseComponent *parent)
	: BaseComponent(parent)
{
}

Task *BaseCompoundComponent::createTask(const Stage stage, IStageData *data)
{
	QList<Task *> tasks;
	for (const auto child : m_children)
	{
		Task *task = child->createTask(stage, data);
		if (task)
		{
			task->setObjectName("Task for " + child->name());
			tasks.append(task);
		}
	}
	if (tasks.isEmpty())
	{
		return nullptr;
	}
	Task *out = new GroupedTask(tasks);
	out->setObjectName("Compound task for " + name());
	return out;
}

QList<IPage *> BaseCompoundComponent::createPages(const bool isRunning)
{
	QList<IPage *> pages;
	for (const auto child : m_children)
	{
		pages.append(child->createPages(isRunning));
	}
	return pages;
}
QList<IPagePart *> BaseCompoundComponent::createPageParts(const bool isRunning)
{
	QList<IPagePart *> parts;
	for (const auto child : m_children)
	{
		parts.append(child->createPageParts(isRunning));
	}
	return parts;
}

QJsonObject BaseCompoundComponent::save() const
{
	QJsonArray array;
	for (const auto child : m_children)
	{
		if (!child->isImplicit())
		{
			QJsonObject c = child->save();
			c.insert("id", child->id());
			array.append(c);
		}
	}
	QJsonObject obj = BaseComponent::save();
	obj.insert("children", array);
	return obj;
}
void BaseCompoundComponent::load(const QJsonObject &obj)
{
	using namespace Json;
	QList<BaseComponent *> comps;
	for (const auto childVal : ensureArray(obj, "children"))
	{
		const QJsonObject childObj = ensureObject(childVal);
		BaseComponent *comp = g_componentRegistry->create(ensureIsType<QString>(childObj, "id"), this);
		comp->setParentComponent(this);
		comp->load(childObj);
		comps.append(comp);
	}
	QList<BaseComponent *> implicitChildren; // storage of implicit children since the list gets overwritten
	for (auto child : m_children)
	{
		if (!child->isImplicit())
		{
			// implicit children don't get saved, so we don't want to remove them
			delete child;
		}
		else
		{
			implicitChildren.append(child);
		}
	}
	m_children = comps + implicitChildren; // add back the implicit children
	BaseComponent::load(obj);
	emit childrenChanged();
}

QList<BaseComponent *> BaseCompoundComponent::allChildren()
{
	QList<BaseComponent *> comps;
	comps.append(this);
	for (const auto child : m_children)
	{
		comps.append(child->allChildren());
	}
	return comps;
}

void BaseCompoundComponent::setChildren(const QList<BaseComponent *> &children)
{
	for (auto child : m_children)
	{
		child->setParentComponent(nullptr);
	}
	m_children = children;
	for (auto child : m_children)
	{
		child->setParentComponent(this);
	}
	emit childrenChanged();
}
void BaseCompoundComponent::addChild(BaseComponent *child)
{
	m_children.prepend(child);
	child->setParentComponent(this);
	emit childrenChanged();
}
void BaseCompoundComponent::removeChild(BaseComponent *child)
{
	m_children.removeAll(child);
	child->setParentComponent(nullptr);
	emit childrenChanged();
}
