// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QObject>
#include <QHash>
#include <QStringList>
#include <QSharedPointer>
#include <QUuid>

#include "core/packages/PackageQuery.h"

enum class Stage
{
	Start,

	Creation,

	UpdateCheck,
	Depres, // hardcoded
	PreDownload,
	Download,
	Installation,

	PreLaunch,
	Launch,
	PostLaunch,

	Removal,

	End
};
QString stageName(const Stage stage);
static inline uint qHash(const Stage stage)
{
	return qHash(int(stage));
}

class Task;
class QJsonObject;
class IPage;
class IPagePart;
class BaseProcess;
class BaseAccount;
class Container;
class BaseVersionList;
class BaseVersion;

class IStageData
{
public:
	virtual ~IStageData() {}

	Container *container;
	QHash<Stage, IStageData *> otherStages;
};
class UpdateCheckData : public IStageData
{
public:
	virtual ~UpdateCheckData() {}

	bool needsUpdate = false;
};
class PreLaunchData : public IStageData
{
public:
	virtual ~PreLaunchData() {}

	QHash<QString, BaseAccount *> accounts;
};
class BaseLaunchData : public IStageData
{
public:
	virtual ~BaseLaunchData() {}

	BaseProcess *process = nullptr;
};

class BaseComponent : public QObject
{
	Q_OBJECT
public:
	explicit BaseComponent(BaseComponent *parent);
	virtual ~BaseComponent() {}

	QUuid uuid() const { return m_uuid; }
	QList<QUuid> addedBy() const { return m_addedBy; }
	void addAddedBy(const BaseComponent *component) { m_addedBy.append(component->uuid()); }
	void removeAddedBy(const BaseComponent *component) { m_addedBy.removeAll(component->uuid()); }
	BaseComponent *findChildByUuid(const QUuid &uuid) const;

	virtual QString id() const = 0;
	virtual QString name() const = 0;
	virtual QString type() const = 0;
	virtual bool canBeRoot() const { return false; }
	virtual IStageData *createStageData(const Stage stage) const;
	virtual bool isImplicit() const { return !parent()->inherits("Container"); }
	virtual Task *createTask(const Stage stage, IStageData *data = nullptr) = 0;

	Package package() const { return m_package; }

	Version version() const { return m_version; }
	virtual QSharedPointer<BaseVersion> versionObject() const;
	virtual bool canChangeVersion() const { return false; }
	virtual void setVersion(const QSharedPointer<BaseVersion> &version);

	virtual QList<IPage *> createPages(const bool isRunning) { return QList<IPage *>(); }
	virtual QList<IPagePart *> createPageParts(const bool isRunning) { return QList<IPagePart *>(); }

	virtual void load(const QJsonObject &obj);
	virtual QJsonObject save() const;

	Container *container() const;
	BaseComponent *parent() const { return m_parent; }
	virtual QList<BaseComponent*> children() const;
	virtual QList<BaseComponent *> allChildren();

	virtual bool canRemove() const { return false; }
	virtual void remove();

signals:
	void nameChanged();
	void typeChanged();
	void versionChanged();

protected:
	void setPackage(const Package &package) { m_package = package; }
	void setVersionInternal(const Version &version)
	{
		m_version = version;
		emit versionChanged();
	}
	virtual void parentChangeNotification() {}

private:
	friend class Container;
	friend class BaseCompoundComponent;
	void setParentComponent(BaseComponent *parent) { m_parent = parent; parentChangeNotification(); }

private:
	BaseComponent *m_parent = nullptr;
	Package m_package;
	Version m_version;
	QUuid m_uuid;
	QList<QUuid> m_addedBy;
};
