// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "BaseComponent.h"

#include <QUrl>

class INetworkValidator;

class FileDownloadComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit FileDownloadComponent(BaseComponent *parent);

	QString id() const override { return "Core.FileDownload"; }
	QString name() const override { return m_url.toString(); }
	QString type() const override { return tr("File Download"); }
	bool isImplicit() const override { return true; }
	Task *createTask(const Stage stage, IStageData *data = nullptr) override;

	void setUrl(const QUrl &url) { m_url = url; }
	QUrl url() const { return m_url; }

	void setDownloadGroup(const QString &group) { m_group = group; }
	QString downloadGroup() const { return m_group; }

	void setDownloadId(const QString &id) { m_id = id; }
	QString downloadId() const { return m_id; }

	void setValidator(INetworkValidator *validator) { m_validator = validator; }

private:
	QUrl m_url;
	QString m_group;
	QString m_id;

	INetworkValidator *m_validator = nullptr;
};
