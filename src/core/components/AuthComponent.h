// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "BaseComponent.h"

class AuthComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit AuthComponent(const QString &type, BaseComponent *parent = nullptr);

	QString id() const override { return "Core.Auth"; }
	QString name() const override;
	QString type() const override { return tr("Authentication"); }
	bool isImplicit() const override { return true; }
	Task *createTask(const Stage stage, IStageData *data) override;

private:
	const QString m_type;
};
