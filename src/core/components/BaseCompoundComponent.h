// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "BaseComponent.h"

class BaseCompoundComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit BaseCompoundComponent(BaseComponent *parent);
	~BaseCompoundComponent() {}

	virtual QList<BaseComponent *> children() const override { return m_children; }
	virtual Task *createTask(const Stage stage, IStageData *data = nullptr) override;

	virtual QList<IPage *> createPages(const bool isRunning) override;
	virtual QList<IPagePart *> createPageParts(const bool isRunning) override;

	QJsonObject save() const override;
	void load(const QJsonObject &obj) override;

	QList<BaseComponent *> allChildren() override;

protected:
	friend class BaseComponent;
	void setChildren(const QList<BaseComponent*> &children);
	void addChild(BaseComponent *child);
	void removeChild(BaseComponent *child);

signals:
	void childrenChanged();

private:
	QList<BaseComponent *> m_children;
};
