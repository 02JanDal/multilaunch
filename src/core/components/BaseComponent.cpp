// Licensed under the Apache-2.0 license. See README.md for details.

#include "BaseComponent.h"

#include "core/util/Json.h"
#include "core/BaseVersionList.h"
#include "core/BaseVersion.h"
#include "core/packages/PackageHandler.h"
#include "core/Container.h"

BaseComponent::BaseComponent(BaseComponent *parent)
	: QObject(parent), m_parent(parent), m_uuid(QUuid::createUuid())
{
}

BaseComponent *BaseComponent::findChildByUuid(const QUuid &uuid) const
{
	if (m_uuid == uuid)
	{
		return const_cast<BaseComponent *>(this);
	}
	for (auto child : children())
	{
		BaseComponent *comp = child->findChildByUuid(uuid);
		if (comp)
		{
			return comp;
		}
	}
	return nullptr;
}

IStageData *BaseComponent::createStageData(const Stage stage) const
{
	if (stage == Stage::UpdateCheck)
	{
		return new UpdateCheckData;
	}
	else if (stage == Stage::PreLaunch)
	{
		return new PreLaunchData;
	}
	else
	{
		return nullptr;
	}
}

QSharedPointer<BaseVersion> BaseComponent::versionObject() const
{
	BaseVersionList *list = g_packages->handler(m_package)->getVersionsList(m_package);
	if (list)
	{
		return list->findVersion(version());
	}
	else
	{
		return QSharedPointer<BaseVersion>();
	}
}

void BaseComponent::setVersion(const QSharedPointer<BaseVersion> &version)
{
	setVersionInternal(version->id());
}

void BaseComponent::load(const QJsonObject &obj)
{
	m_version = Version::fromString(Json::ensureIsType<QString>(obj, "version", QString()));
	m_package = Package::fromString(Json::ensureIsType<QString>(obj, "package", QString()));
	m_uuid = Json::ensureIsType<QUuid>(obj, "uuid");
	m_addedBy = Json::ensureIsArrayOf<QUuid>(obj, "addedBy");
}
QJsonObject BaseComponent::save() const
{
	QJsonObject obj;
	obj.insert("version", m_version.toString());
	obj.insert("package", m_package.toString());
	obj.insert("uuid", Json::toJson(m_uuid));
	obj.insert("addedBy", Json::toJsonArray(m_addedBy));
	return obj;
}

Container *BaseComponent::container() const
{
	if (!inherits("Container"))
	{
		return m_parent ? m_parent->container() : nullptr;
	}
	else
	{
		return static_cast<Container *>(const_cast<BaseComponent *>(this));
	}
}

QList<BaseComponent*> BaseComponent::children() const
{
	return QList<BaseComponent *>();
}

QList<BaseComponent *> BaseComponent::allChildren()
{
	return QList<BaseComponent *>() << this;
}

void BaseComponent::remove()
{
	if (BaseCompoundComponent *compound = qobject_cast<BaseCompoundComponent *>(m_parent))
	{
		compound->removeChild(this);
	}
	deleteLater();
}

QString stageName(const Stage stage)
{
	switch (stage)
	{
	case Stage::Start: return "Start";
	case Stage::Creation: return "Creation";
	case Stage::Depres: return "Depres";
	case Stage::UpdateCheck: return "UpdateCheck";
	case Stage::PreDownload: return "PreDownoad";
	case Stage::Download: return "Download";
	case Stage::Installation: return "Installation";
	case Stage::PreLaunch: return "PreLaunch";
	case Stage::Launch: return "Launch";
	case Stage::PostLaunch: return "PostLaunch";
	case Stage::Removal: return "Removal";
	case Stage::End: return "End";
	}
}
