// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "BaseComponent.h"

#include <QDir>

class FileComponent;

class FolderComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit FolderComponent(BaseComponent *parent);

	QDir dir() const { return m_dir; }
	void setDir(const QDir &dir);
	void setName(const QString &name) { m_name = name; }

	QString id() const override { return "Core.Folder"; }
	QString name() const override { return m_name; }
	QString type() const override { return tr("Folder"); }
	bool isImplicit() const override { return parent()->inherits("FolderComponent"); }
	Task *createTask(const Stage stage, IStageData *data = nullptr) override { return nullptr; }

	QJsonObject save() const override;
	void load(const QJsonObject &obj) override;

	bool canRemove() const { return true; }
	void remove() override;

	QList<IPage *> createPages(const bool isRunning) override;

private:
	QDir m_dir;
	QString m_name;
};
