// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseCompoundComponent.h"

#include <QDir>
#include <QUuid>

#include "core/util/BaseConfigObject.h"

class Task;

class Container : public BaseCompoundComponent, public BaseConfigObject
{
	Q_OBJECT
public:
	explicit Container(const QUuid &uuid, const QDir &dir, BaseComponent *root); // create new
	explicit Container(const QUuid &uuid, const QDir &dir); // load existing
	~Container();

	QUuid uuid() const { return m_uuid; }
	BaseComponent *rootComponent() const { return m_rootComponent; }

	QString id() const { return "Core.Container"; }
	QString name() const { return m_name; }
	QString iconKey() const { return m_iconKey; }
	QString icon() const;
	Version version() const;
	QString type() const;

	QDir rootDir() const { return m_dir; }

	void load(const QJsonObject &obj);
	QJsonObject save() const;
	Task *createTask(const Stage stage, IStageData *data) override;
	QList<IPage *> createPages(const bool isRunning) override;
	IStageData *createStageData(const Stage stage) const override;

	bool canRemove() const { return true; }
	void remove();

	using BaseCompoundComponent::addChild;

	/// Does a topological sort of direct children, to ensure that launching etc. happens correctly
	void sortChildren();

public slots:
	void setName(const QString &name);
	void setIconKey(const QString &icon);

signals:
	void nameChanged();
	void iconKeyChanged();
	void versionChanged();
	void typeChanged();

private:
	QUuid m_uuid;
	QDir m_dir;
	QString m_name;
	QString m_iconKey;
	BaseComponent *m_rootComponent = nullptr;

protected:
	QByteArray doSave() const;
	void doLoad(const QByteArray &data);
};
