// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/util/BaseConfigObject.h"
#include "core/util/Singleton.h"

#include <QVariant>

class GlobalSettings : public BaseConfigObject
{
public:
	explicit GlobalSettings();

	void registerSetting(const QString &key, const QVariant::Type type, const QVariant &def = QVariant());

	void set(const QString &key, const QVariant &value);
	void reset(const QString &key);
	QVariant get(const QString &key) const;

	template<typename T>
	inline T get(const QString &key) const
	{
		return get(key).value<T>();
	}

private:
	struct Item
	{
		explicit Item(const QString &k, const QVariant::Type t, const QVariant &d)
			: key(k), type(t), def(d) {}
		explicit Item() : key(QString()), type(QVariant::Invalid), def(QVariant()) {}
		QString key;
		QVariant::Type type;
		QVariant def;
		QVariant value;
	};
	QMap<QString, Item> m_items;

	void doLoad(const QByteArray &data) override;
	QByteArray doSave() const override;
};
DECLARE_SINGLETON(GlobalSettings, g_settings)
