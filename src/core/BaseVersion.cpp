// Licensed under the Apache-2.0 license. See README.md for details.

#include "BaseVersion.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"
static int baseVersionMetaTypeId = qRegisterMetaType<QWeakPointer<BaseVersion>>("QWeakPointer<BaseVersion>");
#pragma clang diagnostic pop
