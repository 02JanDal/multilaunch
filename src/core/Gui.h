// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "LogicalGui.h"
#include "util/Singleton.h"

class Gui : public Bindable
{
public:
	using Bindable::wait;
	using Bindable::request;
};

DECLARE_SINGLETON(Gui, g_gui)
