// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QAbstractListModel>

#include "core/util/Singleton.h"
#include "core/util/BaseConfigObject.h"
#include "core/util/AbstractCommonModel.h"

class BaseAccount;
class BaseAccountType;
class Container;
class AccountTypesModel;

class AccountModel : public QAbstractListModel, public BaseConfigObject
{
	Q_OBJECT
public:
	explicit AccountModel(QObject *parent = nullptr);
	~AccountModel();

	void registerType(BaseAccountType *type);
	BaseAccountType *type(const QString &id) const;
	QStringList types() const;

	BaseAccount *get(const QModelIndex &index) const;
	BaseAccount *get(const QString &type, const Container *container = nullptr) const;
	void setGlobalDefault(BaseAccount *account);
	void setContainerDefault(Container *container, BaseAccount *account);

	int rowCount(const QModelIndex &parent) const override;
	int columnCount(const QModelIndex &parent) const override;
	QVariant data(const QModelIndex &index, int role) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

	QAbstractItemModel *typesModel() const;

public slots:
	void registerAccount(BaseAccount *account);
	void unregisterAccount(BaseAccount *account);
	void accountChanged(BaseAccount *account);

protected:
	void doLoad(const QByteArray &data) override;
	QByteArray doSave() const override;

private:
	//   type          container
	QMap<QString, QMap<QUuid, BaseAccount *>> m_defaults;
	QList<BaseAccount *> m_accounts;

	QMap<QString, BaseAccountType *> m_types;
	AccountTypesModel *m_typesModel;
};
DECLARE_SINGLETON(AccountModel, g_accounts)
