// Licensed under the Apache-2.0 license. See README.md for details.

#include "AccountModel.h"

#include <QTimer>

#include "core/util/Json.h"
#include "core/Container.h"
#include "BaseAccount.h"
#include "BaseAccountType.h"

DEFINE_SINGLETON(g_accounts)

class AccountTypesModel : public AbstractCommonModel<BaseAccountType *>
{
public:
	explicit AccountTypesModel() : AbstractCommonModel<BaseAccountType *>(Qt::Vertical)
	{
		addEntry<QString>(&BaseAccountType::id, 0, Qt::UserRole);
		addEntry<QString>(&BaseAccountType::text, 0, Qt::DisplayRole);
		addEntry<QString>(&BaseAccountType::icon, 0, Qt::DecorationRole);
	}
};

AccountModel::AccountModel(QObject *parent)
	: QAbstractListModel(parent), BaseConfigObject("accounts.json")
{
	m_typesModel = new AccountTypesModel;

	QTimer::singleShot(0, [this](){loadNow();});
}
AccountModel::~AccountModel()
{
	qDeleteAll(m_accounts);
	delete m_typesModel;
}

void AccountModel::registerType(BaseAccountType *type)
{
	m_types.insert(type->id(), type);
	m_typesModel->append(type);
}
BaseAccountType *AccountModel::type(const QString &id) const
{
	return m_types.value(id);
}
QStringList AccountModel::types() const
{
	return m_types.keys();
}

BaseAccount *AccountModel::get(const QModelIndex &index) const
{
	return index.isValid() ? m_accounts.at(index.row()) : nullptr;
}
BaseAccount *AccountModel::get(const QString &type, const Container *container) const
{
	QMap<QUuid, BaseAccount *> defaults = m_defaults[type];
	// container default?
	if (container)
	{
		if (defaults.contains(container->uuid()))
		{
			return defaults[container->uuid()];
		}
	}
	// global default?
	if (defaults.contains(QUuid()) && defaults.value(QUuid()))
	{
		return defaults[QUuid()];
	}
	return nullptr;
}
void AccountModel::setGlobalDefault(BaseAccount *account)
{
	m_defaults[account->type()][QUuid()] = account;
	scheduleSave();
}
void AccountModel::setContainerDefault(Container *container, BaseAccount *account)
{
	m_defaults[account->type()][container->uuid()] = account;
	scheduleSave();
}

int AccountModel::rowCount(const QModelIndex &parent) const
{
	return m_accounts.size();
}
int AccountModel::columnCount(const QModelIndex &parent) const
{
	return 2;
}
QVariant AccountModel::data(const QModelIndex &index, int role) const
{
	BaseAccount *account = m_accounts.at(index.row());
	if (role == Qt::DisplayRole)
	{
		if (index.column() == 0)
		{
			return account->username();
		}
		else
		{
			return m_types.contains(account->type()) ? m_types[account->type()]->text() : account->type();
		}
	}
	else if (role == Qt::DecorationRole)
	{
		if (index.column() == 0)
		{
			return account->avatar();
		}
		else
		{
			return m_types.contains(account->type()) ? m_types[account->type()]->icon() : QVariant();
		}
	}
	else if (role == Qt::UserRole)
	{
		if (index.column() == 1)
		{
			return account->type();
		}
	}
	return QVariant();
}
QVariant AccountModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
	{
		if (section == 0)
		{
			return tr("Username");
		}
		else
		{
			return tr("Type");
		}
	}
	return QVariant();
}

QAbstractItemModel *AccountModel::typesModel() const
{
	return m_typesModel->model();
}

void AccountModel::registerAccount(BaseAccount *account)
{
	beginInsertRows(QModelIndex(), m_accounts.size(), m_accounts.size());
	m_accounts.append(account);
	endInsertRows();
	scheduleSave();
}
void AccountModel::unregisterAccount(BaseAccount *account)
{
	Q_ASSERT(m_accounts.contains(account));
	beginRemoveRows(QModelIndex(), m_accounts.indexOf(account), m_accounts.indexOf(account));
	m_accounts.removeOne(account);
	const QStringList types = m_defaults.keys();
	for (const auto type : types)
	{
		const QList<QUuid> containers = m_defaults[type].keys(account);
		for (const auto container : containers)
		{
			m_defaults[type].remove(container);
		}
		if (m_defaults[type].isEmpty())
		{
			m_defaults.remove(type);
		}
	}
	endRemoveRows();
	scheduleSave();
}
void AccountModel::accountChanged(BaseAccount *account)
{
	const int row = m_accounts.indexOf(account);
	emit dataChanged(index(row, 0), index(row, 1));
	scheduleSave();
}

void AccountModel::doLoad(const QByteArray &data)
{
	using namespace Json;
	const QJsonObject root = ensureObject(ensureDocument(data));

	QList<BaseAccount *> accs;
	QMap<QString, QMap<QUuid, BaseAccount *>> defs;

	const QJsonArray accounts = ensureArray(root, "accounts");
	for (const auto accountVal : accounts)
	{
		const QJsonObject account = ensureObject(accountVal);
		const QString type = ensureIsType<QString>(account, "type");
		if (!m_types.contains(type))
		{
			qWarning() << "Unable to load account of type" << type << "(unknown factory)";
		}
		else
		{
			BaseAccount *acc = m_types[type]->createAccount();
			acc->load(account);
			accs.append(acc);
		}
	}

	const QJsonArray defaults = ensureArray(root, "defaults");
	for (const auto defVal : defaults)
	{
		const QJsonObject def = ensureObject(defVal);
		const int index = ensureIsType<int>(def, "account");
		if (index >= 0 && index < accs.size())
		{
			defs[ensureIsType<QString>(def, "type")][ensureIsType<QUuid>(def, "container")]
					= accs.at(index);
		}
	}

	beginResetModel();
	m_defaults = defs;
	qDeleteAll(m_accounts);
	m_accounts = accs;
	endResetModel();
}
QByteArray AccountModel::doSave() const
{
	using namespace Json;
	QJsonArray accounts;
	for (const auto account : m_accounts)
	{
		QJsonObject obj = account->save();
		obj.insert("type", account->type());
		accounts.append(obj);
	}
	QJsonArray defaults;
	for (auto it = m_defaults.constBegin(); it != m_defaults.constEnd(); ++it)
	{
		for (auto it2 = it.value().constBegin(); it2 != it.value().constEnd(); ++it2)
		{
			QJsonObject obj;
			obj.insert("type", it.key());
			obj.insert("container", toJson(it2.key()));
			obj.insert("account", m_accounts.indexOf(it2.value()));
			defaults.append(obj);
		}
	}

	QJsonObject root;
	root.insert("accounts", accounts);
	root.insert("defaults", defaults);
	return toBinary(root);
}
