// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QString>
#include <QMap>
#include <QMetaType>

class Task;
class QJsonObject;

class BaseAccount
{
public:
	virtual ~BaseAccount() {}

	virtual QString avatar() const { return QString(); }
	virtual QString bigAvatar() const { return avatar(); }

	virtual QString type() const = 0;

	virtual Task *createLoginTask(const QString &username, const QString &password) = 0;
	virtual Task *createCheckTask() = 0;
	virtual Task *createLogoutTask() = 0;

	QString username() const { return m_username; }
	void setUsername(const QString &username);

	virtual QString loginUsername() const { return username(); }

	bool hasToken(const QString &key) const { return m_tokens.contains(key); }
	QString token(const QString &key) const { return m_tokens[key]; }
	void setToken(const QString &key, const QString &token);

	virtual void load(const QJsonObject &obj);
	virtual QJsonObject save() const;

private:
	QString m_username;
	QMap<QString, QString> m_tokens;
};
Q_DECLARE_METATYPE(BaseAccount *)
