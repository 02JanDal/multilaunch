// Licensed under the Apache-2.0 license. See README.md for details.

#include "ContainerModel.h"

#include "core/util/Json.h"
#include "Container.h"

DEFINE_SINGLETON(g_containerModel)

ContainerModel::ContainerModel(QObject *parent)
	: QAbstractItemModel(parent), BaseConfigObject("containers.json")
{
	loadNow();
	if (!m_rootNode)
	{
		m_rootNode = new Node;
		m_rootNode->group = new Group;
		m_groups.insert(QString(), m_rootNode);
	}
}
ContainerModel::~ContainerModel()
{
	saveNow();
}

QModelIndex ContainerModel::index(int row, int column, const QModelIndex &parent) const
{
	Q_ASSERT(hasIndex(row, column, parent));
	Node *parentNode = static_cast<Node *>(parent.internalPointer());
	if (!parentNode)
	{
		parentNode = m_rootNode;
	}
	return createIndex(row, column, parentNode->children.at(row));
}
QModelIndex ContainerModel::parent(const QModelIndex &child) const
{
	Node *node = static_cast<Node *>(child.internalPointer());
	if (node->parent)
	{
		return index(node->parent);
	}
	else
	{
		return QModelIndex();
	}
}
int ContainerModel::rowCount(const QModelIndex &parent) const
{
	Node *node = static_cast<Node *>(parent.internalPointer());
	if (!node || !parent.isValid())
	{
		node = m_rootNode;
	}
	return node->children.size();
}
int ContainerModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 3;
}
Qt::ItemFlags ContainerModel::flags(const QModelIndex &index) const
{
	Node *node = static_cast<Node *>(index.internalPointer());
	if (index.column() == 0 && !node->group)
	{
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable; // | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
	}
	else
	{
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled; // | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
	}
}

QVariant ContainerModel::data(const QModelIndex &index, int role) const
{
	Node *node = static_cast<Node *>(index.internalPointer());
	Q_ASSERT(node);
	if (node->group)
	{
		if (role == Qt::DisplayRole && index.column() == 0)
		{
			return node->group->name;
		}
	}
	else
	{
		switch (index.column())
		{
		case 0:
			if (role == Qt::DisplayRole || role == Qt::EditRole)
			{
				return node->container->name();
			}
			else if (role == Qt::DecorationRole)
			{
				return node->container->icon();
			}
			break;
		case 1:
			if (role == Qt::DisplayRole || role == Qt::EditRole)
			{
				return node->container->type();
			}
			break;
		case 2:
			if (role == Qt::DisplayRole || role == Qt::EditRole)
			{
				return node->container->version().toString();
			}
			break;
		}
	}
	return QVariant();
}
bool ContainerModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	Node *node = static_cast<Node *>(index.internalPointer());
	if (role != Qt::EditRole || value.toString().isEmpty() || node->group)
	{
		return false;
	}
	switch (index.column())
	{
	case 0:
		node->container->setName(value.toString());
		return true;
	}
	return false;
}
QVariant ContainerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
	{
		switch (section)
		{
		case 0: return tr("Name");
		case 1: return tr("Type");
		case 2: return tr("Version");
		}
	}
	return QVariant();
}

// TODO container d&d
QStringList ContainerModel::mimeTypes() const
{
	return QStringList();
}
QMimeData *ContainerModel::mimeData(const QModelIndexList &indexes) const
{
	Q_UNUSED(indexes);
	return nullptr;
}
bool ContainerModel::canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const
{
	Q_UNUSED(data);
	Q_UNUSED(action);
	Q_UNUSED(row);
	Q_UNUSED(column);
	Q_UNUSED(parent);
	return false;
}
bool ContainerModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
	Q_UNUSED(data);
	Q_UNUSED(action);
	Q_UNUSED(row);
	Q_UNUSED(column);
	Q_UNUSED(parent);
	return false;
}
Qt::DropActions ContainerModel::supportedDropActions() const
{
	return Qt::IgnoreAction;
}
Qt::DropActions ContainerModel::supportedDragActions() const
{
	return Qt::IgnoreAction;
}

void ContainerModel::addGroup(const QString &name, const QString &group)
{
	if (m_groups.contains(name))
	{
		return;
	}
	Node *node = new Node;
	node->group = new Group{name};
	addNode(node, group);
	m_groups.insert(name, node);
}
void ContainerModel::addContainer(Container *container, const QString &group)
{
	if (m_containers.contains(container))
	{
		return;
	}
	Node *node = new Node;
	node->container = container;
	container->setParent(this);
	addNode(node, group);
	m_containers.insert(container, node);

	connect(container, &Container::nameChanged, [this, node]()
	{
		const QModelIndex i = index(node);
		emit dataChanged(i.sibling(i.row(), 0), i.sibling(i.row(), 0),
						 QVector<int>() << Qt::DisplayRole);
	});
	connect(container, &Container::iconKeyChanged, [this, node]()
	{
		const QModelIndex i = index(node);
		emit dataChanged(i.sibling(i.row(), 0), i.sibling(i.row(), 0),
						 QVector<int>() << Qt::DecorationRole);
	});
	connect(container, &Container::typeChanged, [this, node]()
	{
		const QModelIndex i = index(node);
		emit dataChanged(i.sibling(i.row(), 1), i.sibling(i.row(), 1),
						 QVector<int>() << Qt::DisplayRole);
	});
	connect(container, &Container::versionChanged, [this, node]()
	{
		const QModelIndex i = index(node);
		emit dataChanged(i.sibling(i.row(), 2), i.sibling(i.row(), 2),
						 QVector<int>() << Qt::DisplayRole);
	});
}
void ContainerModel::deleteContainer(Container *container)
{
	container->remove();
	Node *node = m_containers[container];
	beginRemoveRows(index(node->parent), node->parent->children.indexOf(node), node->parent->children.indexOf(node));
	m_containers.remove(container);
	node->parent->children.removeAll(node);
	endRemoveRows();
	delete container;
	scheduleSave();
}
void ContainerModel::renameGroup(const QString &oldName, const QString &newName)
{
	Node *node = m_groups[oldName];
	if (node && node->group)
	{
		node->group->name = newName;
		const QModelIndex i = index(node);
		emit dataChanged(i.sibling(i.row(), 0), i.sibling(i.row(), columnCount(QModelIndex()) - 1),
						 QVector<int>() << Qt::DisplayRole);
		scheduleSave();
	}
}
void ContainerModel::changeContainerGroup(Container *container, const QString &newGroup)
{
	addGroup(newGroup);
	Node *node = m_containers[container];
	moveRow(index(node->parent), index(node).row(), index(m_groups[newGroup]), 0);
}

Container *ContainerModel::container(const QModelIndex &index) const
{
	return index.isValid() ? static_cast<Node *>(index.internalPointer())->container
						   : nullptr;
}
Container *ContainerModel::container(const QUuid &uuid) const
{
	for (auto container : m_containers.keys())
	{
		if (container->uuid() == uuid)
		{
			return container;
		}
	}
	return nullptr;
}
QString ContainerModel::group(const QModelIndex &index) const
{
	Group *g = static_cast<Node *>(index.internalPointer())->group;
	if (g)
	{
		return g->name;
	}
	else
	{
		return QString();
	}
}
QString ContainerModel::group(Container *container) const
{
	Node *parent = m_containers[container]->parent;
	if (parent && parent->group)
	{
		return parent->group->name;
	}
	else
	{
		return QString();
	}
}

QList<Container *> ContainerModel::containers() const
{
	return m_containers.keys();
}

bool ContainerModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild)
{
	Node *sourceParentNode = static_cast<Node *>(sourceParent.internalPointer());
	if (!sourceParentNode)
	{
		sourceParentNode = m_rootNode;
	}
	Node *destParentNode = static_cast<Node *>(destinationParent.internalPointer());
	if (!destParentNode)
	{
		destParentNode = m_rootNode;
	}
	if (destParentNode->container)
	{
		// can't add children to containers (yet?)
		return false;
	}
	beginMoveRows(sourceParent, sourceRow, sourceRow + count, destinationParent, destinationChild);
	for (int i = 0; i < count; ++i)
	{
		Node *node = sourceParentNode->children.takeAt(sourceRow);
		node->parent = destParentNode;
		destParentNode->children.append(node);
	}
	endMoveRows();
	return false;
}

QModelIndex ContainerModel::index(ContainerModel::Node *node) const
{
	if (node->parent)
	{
		return createIndex(node->parent->children.indexOf(node), 0, node);
	}
	else
	{
		return createIndex(0, 0, node);
	}
}

void ContainerModel::addNode(ContainerModel::Node *node, const QString &group)
{
	if (!group.isEmpty())
	{
		addGroup(group);
	}
	Node *parent = m_groups[group];
	node->parent = parent;

	beginResetModel();
	parent->children.append(node);
	endResetModel();

	scheduleSave();
}

void ContainerModel::doLoad(const QByteArray &data)
{
	beginResetModel();
	for (auto node : m_groups)
	{
		for (auto child : node->children)
		{
			if (child->container)
			{
				delete child->container;
			}
		}
		delete node;
	}
	m_groups.clear();
	m_rootNode = new Node;
	m_rootNode->group = new Group{""};
	m_groups.insert("", m_rootNode);
	endResetModel();

	using namespace Json;
	const QJsonObject obj = ensureObject(ensureDocument(data));
	for (const auto groupVal : ensureArray(obj, "groups"))
	{
		const QJsonObject groupObj = ensureObject(groupVal);
		const QString name = ensureIsType<QString>(groupObj, "name");
		if (!name.isEmpty())
		{
			addGroup(name, ensureIsType<QString>(groupObj, "parent"));
		}
	}
	for (const auto containerVal : ensureArray(obj, "containers"))
	{
		const QJsonObject containerObj = ensureObject(containerVal);
		const QDir dir = ensureIsType<QDir>(containerObj, "directory");
		if (dir.exists())
		{
			addContainer(new Container(ensureIsType<QUuid>(containerObj, "uuid"), dir), ensureIsType<QString>(containerObj, "group"));
		}
	}
}
QByteArray ContainerModel::doSave() const
{
	using namespace Json;
	QJsonArray groups;
	for (auto it = m_groups.constBegin(); it != m_groups.constEnd(); ++it)
	{
		if (!it.value()->group)
		{
			break;
		}
		if (it.value()->group->name.isEmpty())
		{
			continue;
		}
		QJsonObject obj;
		obj.insert("name", it.value()->group->name);
		obj.insert("parent", it.value()->parent ? it.value()->parent->group->name : "");
		groups.append(obj);
	}
	QJsonArray containers;
	for (auto it = m_containers.constBegin(); it != m_containers.constEnd(); ++it)
	{
		QJsonObject obj;
		obj.insert("directory", toJson(it.key()->rootDir()));
		obj.insert("group", it.value()->parent->group->name);
		obj.insert("uuid", toJson(it.key()->uuid()));
		containers.append(obj);
	}
	QJsonObject obj;
	obj.insert("groups", groups);
	obj.insert("containers", containers);
	return toBinary(obj);
}
