// Licensed under the Apache-2.0 license. See README.md for details.

#include "StandardProcess.h"

#include <QProcess>

StandardProcess::StandardProcess(BaseLaunchData *data, QObject *parent)
	: BaseProcess(data, parent)
{
	m_process = new QProcess(this);
	connect(m_process, &QProcess::readyReadStandardOutput, this, &StandardProcess::handleProcessOutput);
	connect(m_process, &QProcess::readyReadStandardError, this, &StandardProcess::handleProcessOutput);
	connectProcess(m_process);
}

void StandardProcess::start()
{
	StandardLaunchData *data = dynamic_cast<StandardLaunchData *>(m_data);
	if (!data)
	{
		throw Exception("StandardProcess invoked with invalid launch data type");
	}
	m_process->setProgram(data->executable);
	m_process->setArguments(data->arguments);
	m_process->setWorkingDirectory(data->workingDirectory);
	emit log(tr("Program: %1").arg(data->executable), Extra);
	emit log(tr("Arguments: %1").arg(data->arguments.join(' ')), Extra);
	emit log(tr("Working Directory: %1").arg(data->workingDirectory));
	emit log();
	m_process->start();
}
void StandardProcess::abort()
{
	m_process->terminate();
}

void StandardProcess::handleProcessOutput()
{
	const QStringList output = QString::fromUtf8(m_process->readAllStandardOutput()).split('\n');
	for (const auto line : output)
	{
		emit log(line, Info);
	}

	const QStringList error = QString::fromUtf8(m_process->readAllStandardError()).split('\n');
	for (const auto line : error)
	{
		emit log(line, Error);
	}
}
