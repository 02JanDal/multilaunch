// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QObject>

class BaseLaunchData;
class QProcess;

class BaseProcess : public QObject
{
	Q_OBJECT
	Q_ENUMS(ExitState)
public:
	explicit BaseProcess(BaseLaunchData *data, QObject *parent = nullptr);

	enum State
	{
		Pending,
		Starting,
		Running,
		Finished
	};

	enum ExitState
	{
		UserAborted,
		GracefullyFinished,
		Crashed,
		NotExited
	};

	enum LogLevel
	{
		Debug,
		Trace,
		Info,
		Warn,
		Error,
		Fatal,

		Extra
	};

	ExitState exitState() const { return m_exitState; }

public slots:
	virtual void start() = 0;
	virtual void abort() = 0;

signals:
	void log(const QString &msg = QString(), const LogLevel level = Extra);
	void stateChanged(const State newState);

protected:
	BaseLaunchData *m_data;
	ExitState m_exitState = NotExited;

	void connectProcess(QProcess *process);
};
