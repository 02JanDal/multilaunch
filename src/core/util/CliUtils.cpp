// Licensed under the Apache-2.0 license. See README.md for details.

#include "CliUtils.h"

#include <QTextStream>
#include <QVector>
#include <QAbstractItemModel>

#include "core/BaseProcess.h"

#if defined(Q_OS_UNIX)
# define RESET   "\033[0m"
# define BLACK   "\033[30m"
# define RED     "\033[31m"
# define GREEN   "\033[32m"
# define YELLOW  "\033[33m"
# define BLUE    "\033[34m"
# define MAGENTA "\033[35m"
# define CYAN    "\033[36m"
# define WHITE   "\033[37m"
# define BOLD   "\033[1m"
#else
# define RESET ""
# define BLACK ""
# define RED ""
# define GREEN ""
# define YELLOW ""
# define BLUE ""
# define MAGENTA ""
# define CYAN ""
# define WHITE ""
# define BOLD ""
#endif

void CliUtils::writeTable(QTextStream &out, const bool bare, const QStringList &header, const QList<QStringList> &table, const int columns)
{
	Q_ASSERT(header.size() == columns);
	QVector<int> widths(columns, 0);
	QList<QStringList> allRows = QList<QStringList>(table) << header;
	for (const auto row : allRows)
	{
		for (int i = 0; i < columns; ++i)
		{
			if (i < row.size())
			{
				widths[i] = std::max(widths[i], row.at(i).size());
			}
		}
	}
	auto separator = [bare, &out, columns, widths]()
	{
		if (!bare)
		{
			out << '+';
			for (int i = 0; i < columns; ++i)
			{
				out << "-" << QString().fill('-', widths.at(i)) << "-+";
			}
			out << endl;
		}
	};
	separator();
	if (!bare)
	{
		out << "|";
	}
	for (int i = 0; i < columns; ++i)
	{
		if (!bare)
		{
			out << " ";
		}
		out << BOLD << header.at(i) << RESET << QString().fill(' ', widths.at(i) - header.at(i).size() + (bare ? 1 : 0));
		if (!bare)
		{
			out << " |";
		}
	}
	out << endl;
	separator();
	for (const auto row : table)
	{
		if (!bare)
		{
			out << "|";
		}
		for (int i = 0; i < columns; ++i)
		{
			const QString cell = i < row.size() ? row.at(i) : "";
			if (!bare)
			{
				out << " ";
			}
			out << cell << QString().fill(' ', widths.at(i) - cell.size() + (bare ? 1 : 0));
			if (!bare)
			{
				out << " |";
			}
		}
		out << endl;
	}
	separator();
}
void CliUtils::writeTable(QTextStream &out, const bool bare, QAbstractItemModel *model)
{
	QStringList header;
	for (int i = 0; i < model->columnCount(); ++i)
	{
		header.append(model->headerData(i, Qt::Horizontal).toString());
	}
	QList<QStringList> table;
	for (int i = 0; i < model->rowCount(); ++i)
	{
		QStringList row;
		for (int j = 0; j < model->columnCount(); ++j)
		{
			row.append(model->index(i, j).data().toString());
		}
		table.append(row);
	}
	writeTable(out, bare, header, table, model->columnCount());
}

void CliUtils::writeLogFormatted(QTextStream &out, const QString &message, const int level)
{
	switch (level)
	{
	case BaseProcess::Debug: out << WHITE; break;
	case BaseProcess::Trace: out << WHITE; break;
	case BaseProcess::Info: out << CYAN; break;
	case BaseProcess::Warn: out << YELLOW; break;
	case BaseProcess::Error: out << RED; break;
	case BaseProcess::Fatal: out << BOLD << RED; break;
	case BaseProcess::Extra: out << BLUE; break;
	}
	out << message << RESET << endl;
}
