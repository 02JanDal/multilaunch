// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

template<typename T, T Default>
class Optional
{
public:
	Optional(const T &value)
		: m_value(value) {}
	Optional() {}

	operator T() const { return m_value; }
	T get() const { return m_value; }

	bool isDefault() const { return m_value == Default; }

	static inline T defaultValue() { return Default; }

private:
	T m_value = Default;
};
