// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QMutex>

template<typename T>
class Singleton
{
public:
	// TODO make threadsafe
	// http://preshing.com/20130930/double-checked-locking-is-fixed-in-cpp11/
	static T *get()
	{
		static T *instance = nullptr;
		if (!instance)
		{
			instance = new T;
		}
		return instance;
	}

	T *operator->() const
	{
		return get();
	}

	operator T*() const
	{
		return get();
	}
};

#define DECLARE_SINGLETON(TYPE, NAME) extern Singleton<TYPE> NAME;
#define DEFINE_SINGLETON(NAME) decltype(NAME) NAME;
