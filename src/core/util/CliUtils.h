// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QString>
#include <QList>

class QTextStream;
class QAbstractItemModel;

namespace CliUtils
{
void writeTable(QTextStream &out, const bool bare, const QStringList &header, const QList<QStringList> &table, const int columns);
void writeTable(QTextStream &out, const bool bare, QAbstractItemModel *model);
void writeLogFormatted(QTextStream &out, const QString &message, const int level);
}
