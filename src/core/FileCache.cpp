// Licensed under the Apache-2.0 license. See README.md for details.

#include "FileCache.h"

#include <QDir>
#include <QCryptographicHash>
#include "core/util/Json.h"

DEFINE_SINGLETON(g_fileCache)

FileCache::FileCache()
	: BaseConfigObject("filecache.dat")
{
	loadNow();
}
FileCache::~FileCache()
{
	saveNow();
}

QByteArray FileCache::doSave() const
{
	using namespace Json;

	QJsonArray root;
	for (const auto entry : m_entries)
	{
		QJsonObject obj;
		obj.insert("group", entry.group);
		obj.insert("id", entry.id);
		obj.insert("onDisk", QDir::current().relativeFilePath(entry.onDisk.absoluteFilePath()));
		obj.insert("origin", toJson(entry.origin));
		obj.insert("etag", entry.etag);
		obj.insert("lastModifiedTimestamp", QString::number(entry.lastModifiedTimestamp));
		obj.insert("hash", toJson(entry.hash));
		root.append(obj);
	}
	return toBinary(root);
}
void FileCache::doLoad(const QByteArray &data)
{
	using namespace Json;

	m_entries.clear();
	const QJsonArray root = ensureArray(Json::ensureDocument(data));
	for (const auto value : root)
	{
		const QJsonObject obj = ensureObject(value);
		Entry entry;
		entry.group = ensureIsType<QString>(obj, "group");
		entry.id = ensureIsType<QString>(obj, "id");
		entry.onDisk = QFileInfo(ensureIsType<QString>(obj, "onDisk"));
		entry.origin = ensureIsType<QUrl>(obj, "origin");
		entry.etag = ensureIsType<QString>(obj, "etag");
		entry.lastModifiedTimestamp = ensureIsType<QString>(obj, "lastModifiedTimestamp").toLongLong();
		entry.hash = ensureIsType<QByteArray>(obj, "hash");
		if (isValid(entry))
		{
			m_entries.append(entry);
		}
	}
}

QByteArray FileCache::computeHash(QIODevice *device)
{
	QCryptographicHash hash(QCryptographicHash::Sha1);
	hash.addData(device);
	return hash.result();
}

FileCache::Entry FileCache::getEntry(const QString &group, const QString &id) const
{
	for (const auto entry : m_entries)
	{
		if (entry.group == group && entry.id == id)
		{
			return entry;
		}
	}
	return Entry();
}
void FileCache::setEntry(const Entry &entry)
{
	Q_ASSERT(!entry.group.isNull());
	Q_ASSERT(!entry.id.isNull());
	QMutableListIterator<Entry> it(m_entries);
	while (it.hasNext())
	{
		Entry ent = it.next();
		if (ent.group == entry.group && ent.id == entry.id)
		{
			it.setValue(entry);
			return;
		}
	}
	m_entries.append(entry);
	scheduleSave();
}
QString FileCache::file(const QString &group, const QString &id) const
{
	return QDir::current().relativeFilePath(getEntry(group, id).onDisk.absoluteFilePath());
}

QString FileCache::fileName(const QString &group, const QString &id) const
{
	return group + '/' + id;
}

void FileCache::addToCache(const QString &group, const QString &id, const QString &filename)
{
	Entry entry;
	entry.group = group;
	entry.id = id;
	entry.onDisk = filename.isEmpty() ? fileName(group, id) : filename;

	QFile file(entry.onDisk.filePath());
	if (!file.open(QFile::ReadOnly))
	{
		qWarning() << "While trying to add file to cache: Unable to read" << file.fileName() << ":" << file.errorString();
		return;
	}
	entry.hash = computeHash(&file);
	entry.lastModifiedTimestamp = entry.onDisk.lastModified().toUTC().toMSecsSinceEpoch();
	if (isValid(entry))
	{
		setEntry(entry);
	}
}

bool FileCache::isValid(const FileCache::Entry &entry)
{
	if (entry.id.isEmpty() || entry.group.isEmpty() || entry.hash.isEmpty() || !entry.onDisk.exists())
	{
		return false;
	}
	if (entry.onDisk.lastModified().toUTC().toMSecsSinceEpoch() == entry.lastModifiedTimestamp)
	{
		// timestamp matches, assume no hash calculation necessary
		return true;
	}
	QFile file(entry.onDisk.absoluteFilePath());
	if (!file.open(QFile::ReadOnly))
	{
		return false;
	}
	if (entry.hash != computeHash(&file))
	{
		// the file was changed. remember it by reseting the timestamp
		Entry ent = entry;
		ent.lastModifiedTimestamp = -1;
		setEntry(ent);
		return false;
	}
	else
	{
		return true;
	}
}

bool FileCache::Entry::isValid() const
{
	return g_fileCache->isValid(*this);
}
