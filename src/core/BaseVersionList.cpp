// Licensed under the Apache-2.0 license. See README.md for details.

#include "BaseVersionList.h"

#include "core/util/Json.h"

#include "BaseVersion.h"

BaseVersionList::BaseVersionList(const QString &savePath, IVersionFactory *factory, QObject *parent)
	: QAbstractListModel(parent), BaseConfigObject(savePath), m_factory(factory)
{
	loadNow();
}

int BaseVersionList::rowCount(const QModelIndex &parent) const
{
	return m_versions.size();
}
int BaseVersionList::columnCount(const QModelIndex &parent) const
{
	return 2;
}

Qt::ItemFlags BaseVersionList::flags(const QModelIndex &index) const
{
	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}
QVariant BaseVersionList::data(const QModelIndex &index, int role) const
{
	QSharedPointer<BaseVersion> version = m_versions.at(index.row());
	if (role == Qt::DisplayRole)
	{
		switch (index.column())
		{
		case 0: return version->id().toString();
		case 1: return version->type();
		}
	}
	return QVariant();
}
QVariant BaseVersionList::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
	{
		switch (section)
		{
		case 0: return tr("Version");
		case 1: return tr("Type");
		}
	}
	return QVariant();
}

int BaseVersionList::indexOf(const QSharedPointer<BaseVersion> &version) const
{
	for (int i = 0; i < m_versions.size(); ++i)
	{
		if (version->operator ==(m_versions.at(i)))
		{
			return i;
		}
	}
	return -1;
}

QSharedPointer<BaseVersion> BaseVersionList::findVersion(const Version &id) const
{
	for (auto version : m_versions)
	{
		if (version->id() == id)
		{
			return version;
		}
	}
	return QSharedPointer<BaseVersion>();
}

QList<Version> BaseVersionList::versions() const
{
	QList<Version> out;
	for (const auto version : m_versions)
	{
		out.append(version->id());
	}
	return out;
}

void BaseVersionList::setVersions(const QList<QSharedPointer<BaseVersion> > &versions)
{
	beginResetModel();
	m_versions = versions;
	std::sort(m_versions.begin(), m_versions.end(), [](const QWeakPointer<BaseVersion> &a, const QWeakPointer<BaseVersion> &b)
	{
		return a.lock()->operator>(b.lock());
	});
	endResetModel();
	scheduleSave();
}

void BaseVersionList::doLoad(const QByteArray &data)
{
	const QJsonArray root = Json::ensureArray(Json::ensureDocument(data));
	QList<QSharedPointer<BaseVersion>> versions;
	for (const auto obj : Json::ensureIsArrayOf<QJsonObject>(root))
	{
		BaseVersion *version = m_factory->create();
		version->load(obj);
		versions.append(QSharedPointer<BaseVersion>(version));
	}
	setVersions(versions);
}
QByteArray BaseVersionList::doSave() const
{
	QJsonArray array;
	for (const auto version : m_versions)
	{
		array.append(version->save());
	}
	return Json::toBinary(array);
}
