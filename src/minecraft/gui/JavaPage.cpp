// Licensed under the Apache-2.0 license. See README.md for details.

#include "JavaPage.h"

#include <QFormLayout>

#include "core/GlobalSettings.h"
#include "gui/widgets/VersionSelector.h"
#include "minecraft/java/JavaVersionList.h"

JavaPageWidget::JavaPageWidget(QWidget *parent)
	: QWidget(parent)
{
	m_javaVersion = new VersionSelector(g_javaVersions.get(), this);

	QFormLayout *layout = new QFormLayout(this);
	layout->addRow(tr("Java"), m_javaVersion);
}

void JavaPageWidget::init()
{
	m_javaVersion->setVersion(g_javaVersions->find(g_settings->get<QString>("java.defaultPath")));
}
void JavaPageWidget::apply()
{
	const QSharedPointer<JavaVersion> version = m_javaVersion->version().lock().dynamicCast<JavaVersion>();
	if (version)
	{
		g_settings->set("java.defaultPath", version->javaPath());
	}
	else
	{
		g_settings->reset("java.defaultPath");
	}
}
bool JavaPageWidget::hasChanges() const
{
	const QSharedPointer<JavaVersion> version = m_javaVersion->version().lock().dynamicCast<JavaVersion>();
	if (version.isNull())
	{
		return !g_settings->get<QString>("java.defaultPath").isEmpty();
	}
	else
	{
		return g_settings->get<QString>("java.defaultPath") != version->javaPath();
	}
}
