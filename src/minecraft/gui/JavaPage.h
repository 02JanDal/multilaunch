// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QWidget>

#include "core/IPage.h"

class VersionSelector;

class JavaPageWidget : public QWidget
{
	Q_OBJECT
public:
	explicit JavaPageWidget(QWidget *parent = nullptr);

	void init();
	void apply();
	bool hasChanges() const;

private:
	VersionSelector *m_javaVersion;
};

class JavaPage : public IPage
{
public:
	QString id() const override { return "java"; }
	QString name() const override { return JavaPageWidget::tr("Java"); }
	QString header() const override { return name(); }
	QString icon() const override { return "java"; }
	QObject *createWidget() const override { return new JavaPageWidget; }
	void initializePage(QObject *widget) override { qobject_cast<JavaPageWidget *>(widget)->init(); }
	void applyPage(QObject *widget) override { qobject_cast<JavaPageWidget *>(widget)->apply(); }
	bool hasChanges(QObject *widget) const override { return qobject_cast<JavaPageWidget *>(widget)->hasChanges(); }
};
