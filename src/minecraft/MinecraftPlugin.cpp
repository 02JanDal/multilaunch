// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftPlugin.h"

#include "core/ContainerTemplateManager.h"
#include "core/Container.h"
#include "core/components/ComponentRegistry.h"
#include "core/accounts/AccountModel.h"
#include "core/GlobalSettings.h"
#include "minecraft/minecraft/MinecraftComponent.h"
#include "minecraft/minecraft/MinecraftArgumentsComponent.h"
#include "minecraft/minecraft/MinecraftAssetsComponent.h"
#include "minecraft/minecraft/MinecraftAccount.h"
#include "minecraft/minecraft/MinecraftPackageHandler.h"
#include "minecraft/minecraft/MinecraftVersionList.h"
#include "minecraft/minecraft/MinecraftTweakerComponent.h"
#include "minecraft/java/ClasspathComponent.h"
#include "minecraft/java/JavaArgumentsComponent.h"
#include "minecraft/java/JavaLibraryComponent.h"
#include "minecraft/java/JavaNativesComponent.h"
#include "minecraft/java/JavaMainClassComponent.h"
#include "minecraft/forge/ForgeComponent.h"
#include "minecraft/forge/ForgeVersionList.h"
#include "minecraft/forge/FMLVersionList.h"
#include "minecraft/forge/ForgeVersion.h"
#include "minecraft/forge/ForgePackageHandler.h"
#include "minecraft/gui/JavaPage.h"

class MinecraftTemplate : public BaseContainerTemplate
{
public:
	QString name() const override { return QObject::tr("Minecraft"); }
	QString icon() const override { return "minecraft"; }
	Container *createContainer(const QDir &root, const QSharedPointer<BaseVersion> &version) const override
	{
		MinecraftComponent *component = new MinecraftComponent(nullptr);
		component->setVersion(version);
		return new Container(QUuid::createUuid(), root, component);
	}
	BaseVersionList *versionsList() const override
	{
		return g_minecraftList;
	}
};
class ForgeTemplate : public BaseContainerTemplate
{
public:
	QString name() const override { return QObject::tr("Minecraft + Forge"); }
	QString icon() const override { return "forge"; }
	Container *createContainer(const QDir &root, const QSharedPointer<BaseVersion> &version) const override
	{
		const QSharedPointer<ForgeVersion> forge = version.dynamicCast<ForgeVersion>();
		MinecraftComponent *mcComponent = new MinecraftComponent(nullptr);
		mcComponent->setVersion(g_minecraftList->findVersion(forge->minecraftVersion()));

		Container *container = new Container(QUuid::createUuid(), root, mcComponent);

		ForgeComponent *forgeComponent = new ForgeComponent(container);
		forgeComponent->setVersion(version);
		container->addChild(forgeComponent);

		return container;
	}
	BaseVersionList *versionsList() const override
	{
		return g_forgeVersions;
	}
};
class FMLTemplate : public BaseContainerTemplate
{
public:
	QString name() const override { return QObject::tr("Minecraft + FML"); }
	QString icon() const override { return "forge"; }
	Container *createContainer(const QDir &root, const QSharedPointer<BaseVersion> &version) const override
	{
		const QSharedPointer<ForgeVersion> forge = version.dynamicCast<ForgeVersion>();
		MinecraftComponent *mcComponent = new MinecraftComponent(nullptr);
		mcComponent->setVersion(g_minecraftList->findVersion(forge->minecraftVersion()));

		Container *container = new Container(QUuid::createUuid(), root, mcComponent);

		ForgeComponent *fmlComponent = new FMLComponent(container);
		fmlComponent->setVersion(version);
		container->addChild(fmlComponent);

		return container;
	}
	BaseVersionList *versionsList() const override
	{
		return g_fmlVersions;
	}
};

void MinecraftPlugin::init()
{
	g_componentRegistry->registerComponent<ClasspathComponent>("Java.Classpath");
	g_componentRegistry->registerComponent<JavaArgumentsComponent>("Java.Arguments");
	g_componentRegistry->registerComponent<JavaLibraryComponent>("Java.Library");
	g_componentRegistry->registerComponent<JavaNativesComponent>("Java.Natives");
	g_componentRegistry->registerComponent<JavaMainClassComponent>("Java.MainClass");
	g_componentRegistry->registerComponent<MinecraftArgumentsComponent>("Minecraft.Arguments");
	g_componentRegistry->registerComponent<MinecraftAssetsComponent>("Minecraft.Assets");
	g_componentRegistry->registerComponent<MinecraftComponent>("Minecraft.Minecraft");
	g_componentRegistry->registerComponent<MinecraftTweakerComponent>("Minecraft.Tweaker");
	g_componentRegistry->registerComponent<ForgeComponent>("Minecraft.Forge");
	g_componentRegistry->registerComponent<ForgeServerComponent>("Minecraft.Forge.Server");
	g_componentRegistry->registerComponent<FMLComponent>("Minecraft.FML");
	g_componentRegistry->registerComponent<FMLServerComponent>("Minecraft.FML.Server");
	g_accounts->registerType(new MinecraftAccountType);
	g_containerTemplates->registerTemplate(new MinecraftTemplate);
	g_containerTemplates->registerTemplate(new ForgeTemplate);
	g_containerTemplates->registerTemplate(new FMLTemplate);
	g_settings->registerSetting("java.defaultPath", QVariant::String);

	g_packages->registerHandler(QStringList() << "minecraft" << "java.arguments", new MinecraftPackageHandler);
	g_packages->registerHandler(QStringList() << "minecraft.forge" << "minecraft.fml", new ForgePackageHandler);
}

QList<IPage *> MinecraftPlugin::createGlobalPages() const
{
	return QList<IPage *>() << new JavaPage;
}
QList<IPagePart *> MinecraftPlugin::createGlobalPageParts() const
{
	return QList<IPagePart *>();
}
