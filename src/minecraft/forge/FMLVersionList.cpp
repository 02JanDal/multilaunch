// Licensed under the Apache-2.0 license. See README.md for details.

#include "FMLVersionList.h"

#include "core/Container.h"
#include "ForgeFetchVersionsTask.h"
#include "ForgeVersion.h"

DEFINE_SINGLETON(g_fmlVersions)

FMLVersionList::FMLVersionList(QObject *parent)
	: BaseVersionList("minecraft/forge/fml.versions.json", new VersionFactory<ForgeVersion>, parent)
{
}

bool FMLVersionList::shouldShow(const int index, Container *container) const
{
	if (container)
	{
		if (container->rootComponent() && container->rootComponent()->inherits("MinecraftComponent"))
		{
			const QSharedPointer<ForgeVersion> version = at(index).dynamicCast<ForgeVersion>();
			if (version->minecraftVersion() != container->rootComponent()->version())
			{
				return false;
			}
		}
	}
	return true;
}
Task *FMLVersionList::createRefreshTask()
{
	ForgeFetchVersionsTask *task = new ForgeFetchVersionsTask("fml", "http://files.minecraftforge.net/maven/net/minecraftforge/fml/json");
	connect(task, &ForgeFetchVersionsTask::result, this, &FMLVersionList::setVersions);
	return task;
}
