// Licensed under the Apache-2.0 license. See README.md for details.

#include "ForgeFetchVersionsTask.h"

#include "core/network/CachedDownloadTask.h"
#include "core/util/Json.h"
#include "ForgeVersion.h"

ForgeFetchVersionsTask::ForgeFetchVersionsTask(const QString &artifact, const QString &url, QObject *parent)
	: StandardTask(parent), m_artifact(artifact), m_url(url) {}

void ForgeFetchVersionsTask::run()
{
	const QByteArray data = networkGetCached("Minecraft Forge Versions List", "lists", "minecraft." + m_artifact + ".json",
											 QUrl(m_url),
											 false, new JsonValidator);

	using namespace Json;
	const QJsonObject root = ensureObject(ensureDocument(data));

	// promotions
	QMap<int, QString> promotions;
	const QJsonObject promos = ensureObject(root, "promos");
	for (auto it = promos.constBegin(); it != promos.constEnd(); ++it)
	{
		promotions.insert(ensureIsType<int>(it.value()), it.key());
	}

	// common thingies
	const QUrl baseUrl = ensureIsType<QString>(root, "webpath") + '/';
	const QString artifact = ensureIsType<QString>(root, "artifact");

	const QJsonObject versions = ensureObject(root, "number");
	QList<QSharedPointer<BaseVersion>> res;
	for (auto it = versions.constBegin(); it != versions.constEnd(); ++it)
	{
		const QJsonObject obj = ensureObject(it.value());

		const QString branch = !obj.value("branch").isNull() ? ensureIsType<QString>(obj, "branch", QString()) : QString();

		QString promotion = promotions.value(ensureIsType<int>(obj, "build"));
		promotion = promotion.remove(ensureIsType<QString>(obj, "mcversion"));
		if (!branch.isNull())
		{
			promotion = promotion.remove(branch);
		}
		promotion = promotion.remove(QRegExp("^\\-")).remove(QRegExp("\\-$"));

		QHash<QString, ForgeVersion::File> files;
		for (const auto array : ensureIsArrayOf<QJsonArray>(obj, "files"))
		{
			if (array.size() != 3)
			{
				throw Exception("files array does not have 3 elements");
			}
			ForgeVersion::File file;
			file.ending = ensureIsType<QString>(array.at(0));
			file.type = ensureIsType<QString>(array.at(1));
			file.hash = ensureIsType<QByteArray>(array.at(2));
			QString version = ensureIsType<QString>(obj, "mcversion") + '-' + ensureIsType<QString>(obj, "version");
			if (!branch.isNull())
			{
				version += '-' + branch;
			}
			file.url = baseUrl.resolved(QString("%1/%2-%1-%3.%4").arg(
											version, artifact, file.type, file.ending));
			files.insert(file.type, file);
		}

		res.append(QSharedPointer<ForgeVersion>::create(
						  ensureIsType<QString>(obj, "version"),
						  ensureIsType<QString>(obj, "mcversion"),
						  promotion,
						  int(ensureIsType<double>(obj, "modified")),
						  files
						  ));
	}

	emit result(res);
	setSuccess();
}
