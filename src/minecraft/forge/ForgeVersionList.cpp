// Licensed under the Apache-2.0 license. See README.md for details.

#include "ForgeVersionList.h"

#include "core/Container.h"
#include "ForgeFetchVersionsTask.h"
#include "ForgeVersion.h"

DEFINE_SINGLETON(g_forgeVersions)

ForgeVersionList::ForgeVersionList(QObject *parent)
	: BaseVersionList("minecraft/forge/versions.json", new VersionFactory<ForgeVersion>, parent)
{
}

bool ForgeVersionList::shouldShow(const int index, Container *container) const
{
	if (container)
	{
		if (container->rootComponent() && container->rootComponent()->inherits("MinecraftComponent"))
		{
			const QSharedPointer<ForgeVersion> version = at(index).dynamicCast<ForgeVersion>();
			if (version->minecraftVersion() != container->rootComponent()->version())
			{
				return false;
			}
		}
	}
	return true;
}
Task *ForgeVersionList::createRefreshTask()
{
	ForgeFetchVersionsTask *task = new ForgeFetchVersionsTask("forge", "http://files.minecraftforge.net/maven/net/minecraftforge/forge/json");
	connect(task, &ForgeFetchVersionsTask::result, this, &ForgeVersionList::setVersions);
	return task;
}
