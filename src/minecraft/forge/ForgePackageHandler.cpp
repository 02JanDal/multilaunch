// Licensed under the Apache-2.0 license. See README.md for details.

#include "ForgePackageHandler.h"

#include "core/Container.h"
#include "ForgeVersionList.h"
#include "FMLVersionList.h"
#include "ForgeVersion.h"
#include "ForgeComponent.h"

Task *ForgePackageHandler::createFetchInformationTask(const Package &package) const
{
	BaseVersionList *list = getVersionsList(package);
	if (list)
	{
		return list->createRefreshTask();
	}
	else
	{
		return nullptr;
	}
}
BaseVersionList *ForgePackageHandler::getVersionsList(const Package &package) const
{
	if (package == Package("minecraft.forge", "forge") || package == Package("minecraft.forge", "forge.server"))
	{
		return g_forgeVersions;
	}
	else if (package == Package("minecraft.forge", "fml") || package == Package("minecraft.forge", "fml.server"))
	{
		return g_fmlVersions;
	}
	else
	{
		return nullptr;
	}
}
BaseComponent *ForgePackageHandler::createComponent(const Package &package, Container *container) const
{
	if (package == Package("minecraft.forge", "forge"))
	{
		return new ForgeComponent(container);
	}
	else if (package == Package("minecraft.forge", "fml"))
	{
		return new FMLComponent(container);
	}
	else if (package == Package("minecraft.forge", "forge.server"))
	{
		return new ForgeServerComponent(container);
	}
	else if (package == Package("minecraft.forge", "fml.server"))
	{
		return new FMLServerComponent(container);
	}
	else
	{
		return nullptr;
	}
}

QList<PackageQuery> ForgePackageHandler::getPackageDependencies(const Package &package, const Version &version) const
{
	const QSharedPointer<ForgeVersion> forge = getVersionsList(package)->findVersion(version).dynamicCast<ForgeVersion>();
	if (forge)
	{
		QList<PackageQuery> deps;
		deps << PackageQuery(Package("minecraft", "minecraft"), QList<VersionInterval>() << VersionInterval(forge->minecraftVersion()));
		deps << PackageQuery(Package("core.folder", "mods")) << PackageQuery(Package("core.folder", "coremods")) << PackageQuery(Package("core.folder", "config"));
		return deps;
	}
	else
	{
		return QList<PackageQuery>();
	}
}
