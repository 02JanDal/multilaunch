// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/tasks/StandardTask.h"

class BaseVersionList;
class BaseVersion;

class ForgeFetchVersionsTask : public StandardTask
{
	Q_OBJECT
public:
	explicit ForgeFetchVersionsTask(const QString &artifact, const QString &url, QObject *parent = nullptr);

signals:
	void result(const QList<QSharedPointer<BaseVersion>> &versions);

private:
	void run() override;

	QString m_artifact;
	QString m_url;
};
