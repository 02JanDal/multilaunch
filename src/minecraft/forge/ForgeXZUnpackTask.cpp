// Licensed under the Apache-2.0 license. See README.md for details.

#include "ForgeXZUnpackTask.h"

#include <KCompressionDevice>
#include <QTemporaryFile>

#include "core/util/Exception.h"
#include "core/FileCache.h"
#include "minecraft/java/JavaLibraryComponent.h"
#include "extra/pack200/unpack200.h"

ForgeXZUnpackTask::ForgeXZUnpackTask(JavaLibraryComponent *component)
	: Task(nullptr), m_component(component)
{
}

void ForgeXZUnpackTask::run()
{
	if (g_fileCache->getEntry("java/libraries", m_component->identifier().toPath()).isValid())
	{
		setSuccess();
		return;
	}

	const QString xzFilename = g_fileCache->fileName("java/libraries", m_component->identifier().toPath() + ".pack.xz");
	KCompressionDevice xz(xzFilename, KCompressionDevice::Xz);
	if (!xz.open(QFile::ReadOnly))
	{
		throw Exception(QString("Unable to open XZ archive %1: %2").arg(xzFilename, xz.errorString()));
	}

	QTemporaryFile packFile;
	packFile.setAutoRemove(false);
	if (!packFile.open())
	{
		throw Exception(QString("Unable to open temporary pack200 file %1: %2").arg(packFile.fileName(), packFile.errorString()));
	}

	packFile.write(xz.readAll());
	packFile.flush();
	packFile.seek(0);

	QFile resultFile(g_fileCache->fileName("java/libraries", m_component->identifier().toPath()));
	if (!resultFile.open(QFile::WriteOnly))
	{
		throw Exception(QString("Unable to open result file %1: %2").arg(resultFile.fileName(), resultFile.errorString()));
	}

	FILE *inFile = fdopen(packFile.handle(), "r");
	FILE *outFile = fdopen(resultFile.handle(), "w");

	try
	{
		unpack_200(inFile, outFile);
	}
	catch (std::exception &e)
	{
		throw Exception(QString("Error while unpacking %1: %2").arg(packFile.fileName(), e.what()));
	}

	setSuccess();
}
