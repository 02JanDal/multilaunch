// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/BaseVersionList.h"
#include "core/util/Singleton.h"

class ForgeVersionList : public BaseVersionList
{
	Q_OBJECT
	friend class ForgeFetchVersionsTask;
public:
	explicit ForgeVersionList(QObject *parent = nullptr);

	bool shouldShow(const int index, Container *container) const override;

	Task *createRefreshTask() override;
};
DECLARE_SINGLETON(ForgeVersionList, g_forgeVersions)
