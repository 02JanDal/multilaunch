// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseCompoundComponent.h"

class FileDownloadComponent;

class ForgeComponent : public BaseCompoundComponent
{
	Q_OBJECT
public:
	explicit ForgeComponent(BaseComponent *parent = nullptr);

	QString id() const override { return "Minecraft.Forge"; }
	QString name() const override { return tr("Minecraft Forge"); }
	QString type() const override { return tr("Minecraft Forge"); }
	bool canChangeVersion() const override { return true; }
	bool canRemove() const override { return true; }

	Task *createTask(const Stage stage, IStageData *data) override;

	// for usage by ForgeTask only!!!
	void setComponents(const QList<BaseComponent *> &components);

private:
	FileDownloadComponent *m_dlComponent;
};

class ForgeServerComponent : public ForgeComponent
{
	Q_OBJECT
public:
	explicit ForgeServerComponent(BaseComponent *parent = nullptr);

	QString id() const override { return "Minecraft.Forge.Server"; }
};
class FMLComponent : public ForgeComponent
{
	Q_OBJECT
public:
	explicit FMLComponent(BaseComponent *parent = nullptr);

	QString id() const override { return "Minecraft.FML"; }
	QString name() const override { return tr("FML"); }
	QString type() const override { return tr("FML"); }
};
class FMLServerComponent : public ForgeComponent
{
	Q_OBJECT
public:
	explicit FMLServerComponent(BaseComponent *parent = nullptr);

	QString id() const override { return "Minecraft.FML.Server"; }
	QString name() const override { return tr("FML"); }
	QString type() const override { return tr("FML"); }
};
