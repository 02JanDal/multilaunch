// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/tasks/Task.h"

class JavaLibraryComponent;

class ForgeXZUnpackTask : public Task
{
	Q_OBJECT
public:
	explicit ForgeXZUnpackTask(JavaLibraryComponent *component);

private:
	void run() override;

	JavaLibraryComponent *m_component;
};
