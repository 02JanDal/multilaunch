// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/BaseVersion.h"

#include <QUrl>

class ForgeVersion : public BaseVersion
{
public:
	struct File
	{
		QUrl url;
		QString ending;
		QByteArray hash;
		QString type;
	};

	explicit ForgeVersion(const QString &version, const QString &mcVersion, const QString &type, const int timestamp, const QHash<QString, File> &files)
		: m_version(Version::fromString(version)), m_mcVersion(Version::fromString(mcVersion)), m_type(type), m_timestamp(timestamp), m_files(files)
	{
	}
	explicit ForgeVersion() {}

	Version id() const override { return m_version; }
	QString type() const override { return m_type; }
	Version minecraftVersion() const { return m_mcVersion; }
	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;
	bool operator >(const QSharedPointer<BaseVersion> &other) const override
	{
		const QSharedPointer<ForgeVersion> forge = other.dynamicCast<ForgeVersion>();
		if (forge)
		{
			return m_timestamp > forge->m_timestamp;
		}
		return false;
	}
	bool operator ==(const QSharedPointer<BaseVersion> &other) const override
	{
		const QSharedPointer<ForgeVersion> forge = other.dynamicCast<ForgeVersion>();
		if (forge)
		{
			return m_timestamp == forge->m_timestamp && m_version == forge->m_version && m_mcVersion == forge->m_mcVersion;
		}
		return false;
	}

	QHash<QString, File> files() const { return m_files; }
	bool usesInstaller() const { return m_files.contains("installer"); }
	QString mainFile() const;

private:
	Version m_version;
	Version m_mcVersion;
	QString m_type;
	int m_timestamp;
	QHash<QString, File> m_files;
};
