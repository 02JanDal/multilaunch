// Licensed under the Apache-2.0 license. See README.md for details.

#include "ForgeComponent.h"

#include <QRegularExpression>
#include <KZip>

#include "core/components/FileDownloadComponent.h"
#include "core/network/CachedDownloadTask.h"
#include "core/tasks/StandardTask.h"
#include "core/util/Json.h"
#include "core/util/FileSystem.h"
#include "core/FileCache.h"
#include "minecraft/java/MavenIdentifier.h"
#include "minecraft/java/JavaMainClassComponent.h"
#include "minecraft/java/JavaLibraryComponent.h"
#include "minecraft/minecraft/MinecraftTweakerComponent.h"
#include "ForgeVersionList.h"
#include "ForgeVersion.h"

class ForgeTask : public StandardTask
{
	Q_OBJECT
public:
	explicit ForgeTask(ForgeComponent *component, QObject *parent = nullptr)
		: StandardTask(parent), m_component(component)
	{
	}

private:
	void run() override
	{
		const QSharedPointer<ForgeVersion> version = m_component->versionObject().dynamicCast<ForgeVersion>();
		const QString downloadGroup = version->usesInstaller() ? "minecraft/forge/installers" : "minecraft/forge/jars";
		const ForgeVersion::File file = version->files()[version->mainFile()];

		networkGetCached("Minecraft Forge Main Download", downloadGroup, file.url.fileName(), file.url, false, new MD5HashValidator(file.hash));

		if (version->usesInstaller())
		{
			KZip *zip = new KZip(downloadGroup + '/' + file.url.fileName());
			if (!zip->open(QIODevice::ReadOnly))
			{
				throw Exception(QString("Unable to open %1: %2").arg(zip->fileName(), zip->device()->errorString()));
			}
			const KArchiveDirectory *dir = zip->directory();
			const KArchiveEntry *entry = dir->entry("install_profile.json");
			if (const KArchiveFile *archiveFile = dynamic_cast<const KArchiveFile *>(entry))
			{
				using namespace Json;
				const QJsonObject root = ensureObject(ensureDocument(archiveFile->data()));

				// a lot of this is the same as in MinecraftTask, and may at some point be merged with it
				{
					const QJsonObject info = ensureObject(root, "versionInfo");

					QList<BaseComponent *> components;

					// mainClass
					{
						JavaMainClassComponent *comp = new JavaMainClassComponent(m_component);
						comp->setClass(ensureIsType<QString>(info, "mainClass"));
						components.append(comp);
					}

					// find tweakers
					{
						QRegularExpression exp("\\-\\-tweakClass ([\\.a-zA-Z0-9_]*)");
						QRegularExpressionMatchIterator it = exp.globalMatch(ensureIsType<QString>(info, "minecraftArguments"));
						while (it.hasNext())
						{
							MinecraftTweakerComponent *comp = new MinecraftTweakerComponent(m_component);
							comp->setTweakerClass(it.next().captured(1));
							components.append(comp);
						}
					}

					const QList<QJsonObject> libraries = ensureIsArrayOf<QJsonObject>(info, "libraries");
					for (const auto lib : libraries)
					{
						// TODO this doesn't handle rules, natives, sidedness etc.

						const QString name = ensureIsType<QString>(lib, "name");
						const MavenIdentifier ident(name);

						// a little blacklist
						if (ident.artifact() == "authlib" || ident.artifact() == "realms" || lib.contains("natives"))
						{
							continue;
						}

						JavaLibraryComponent *library = new JavaLibraryComponent(m_component);
						if (lib.contains("url"))
						{
							library->setBaseUrl(ensureIsType<QUrl>(lib, "url"));
						}
						library->setIdentifier(name);

						// some special casing for forge, because it requires to end with -<type>
						if ((ident.group() == "net.minecraftforge" && ident.artifact() == "forge")
								|| (ident.group() == "cpw.mods" && ident.artifact() == "fml"))
						{
							MavenIdentifier id = ident;
							id.setClassifier("universal");
							library->setIdentifier(id.toString());
						}
						else if (ident.group() == "net.minecraftforge" && ident.artifact() == "minecraftforge")
						{
							MavenIdentifier id;
							id.setGroup("net.minecraftforge");
							id.setArtifact("forge");
							id.setVersion(version->id().toString() + '-' + version->minecraftVersion().toString());
							id.setClassifier("universal");
							library->setIdentifier(id.toString());
						}

						if (ident.group().startsWith("org.scala-lang") || ident.group().startsWith("com.typesafe"))
						{
							library->setHint(JavaLibraryComponent::ForgePackXZ);
						}

						components.append(library);
					}

					m_component->setComponents(components);
				}

				// since we already have the universal jar we can extract it so we don't have to redownload it
				{
					const QJsonObject obj = ensureObject(root, "install");
					const KArchiveFile *archiveLibFile = dynamic_cast<const KArchiveFile *>(dir->entry(ensureIsType<QString>(obj, "filePath")));
					if (archiveLibFile)
					{
						const QString group = "java/libraries";
						const QString id = MavenIdentifier(ensureIsType<QString>(obj, "path")).toPath();
						if (!g_fileCache->getEntry(group, id).isValid())
						{
							const QString filename = g_fileCache->fileName(group, id);
							FS::touch(filename);
							if (archiveLibFile->copyTo(filename))
							{
								g_fileCache->addToCache(group, id);
							}
							else
							{
								qWarning("Couldn't extract Forge/FML universal jar from installer");
							}
						}
					}
				}
			}
		}
		else
		{
			// TODO jarmods
		}
		setSuccess();
	}

	ForgeComponent *m_component;
};

ForgeComponent::ForgeComponent(BaseComponent *parent)
	: BaseCompoundComponent(parent)
{
	setPackage(Package("minecraft.forge", "forge"));
}
FMLComponent::FMLComponent(BaseComponent *parent)
	: ForgeComponent(parent)
{
	setPackage(Package("minecraft.forge", "fml"));
}
ForgeServerComponent::ForgeServerComponent(BaseComponent *parent)
	: ForgeComponent(parent)
{
	setPackage(Package("minecraft.forge", "forge.server"));
}
FMLServerComponent::FMLServerComponent(BaseComponent *parent)
	: ForgeComponent(parent)
{
	setPackage(Package("minecraft.forge", "fml.server"));
}

Task *ForgeComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::PreDownload)
	{
		return new ForgeTask(this, this);
	}
	else
	{
		return BaseCompoundComponent::createTask(stage, data);
	}
}

void ForgeComponent::setComponents(const QList<BaseComponent *> &components)
{
	setChildren(components);
}

#include "ForgeComponent.moc"
