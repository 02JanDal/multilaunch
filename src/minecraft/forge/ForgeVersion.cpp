// Licensed under the Apache-2.0 license. See README.md for details.

#include "ForgeVersion.h"

#include "core/util/Json.h"

void ForgeVersion::load(const QJsonObject &obj)
{
	using namespace Json;
	m_version = Version::fromString(ensureIsType<QString>(obj, "version"));
	m_mcVersion = Version::fromString(ensureIsType<QString>(obj, "mcVersion"));
	m_type = ensureIsType<QString>(obj, "type");
	m_timestamp = ensureIsType<int>(obj, "timestamp");
	for (const auto o : ensureIsArrayOf<QJsonObject>(obj, "files"))
	{
		File file;
		file.ending = ensureIsType<QString>(o, "ending");
		file.hash = ensureIsType<QByteArray>(o, "hash");
		file.type = ensureIsType<QString>(o, "type");
		file.url = ensureIsType<QUrl>(o, "url");
		m_files.insert(file.type, file);
	}
}
QJsonObject ForgeVersion::save() const
{
	QJsonObject obj;
	obj.insert("version", m_version.toString());
	obj.insert("mcVersion", m_mcVersion.toString());
	obj.insert("type", m_type);
	obj.insert("timestamp", m_timestamp);
	QJsonArray arr;
	for (const auto file : m_files)
	{
		QJsonObject o;
		o.insert("ending", file.ending);
		o.insert("hash", Json::toJson(file.hash));
		o.insert("type", file.type);
		o.insert("url", Json::toJson(file.url));
		arr.append(o);
	}
	obj.insert("files", arr);
	return obj;
}

QString ForgeVersion::mainFile() const
{
	if (m_files.contains("installer"))
	{
		return "installer";
	}
	else if (m_files.contains("universal"))
	{
		return "universal";
	}
	else if (m_files.contains("client"))
	{
		return "client";
	}
	else
	{
		return QString();
	}
}
