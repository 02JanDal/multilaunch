// Licensed under the Apache-2.0 license. See README.md for details.

#include "ClasspathComponent.h"

#include "minecraft/java/JavaLaunchData.h"
#include "core/tasks/LambdaTask.h"
#include "core/packages/Version.h"

ClasspathComponent::ClasspathComponent(BaseComponent *parent)
	: BaseComponent(parent)
{
}

Task *ClasspathComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::Launch)
	{
		return new LambdaTask<std::function<void()>>([this, data]() {
			JavaLaunchData *jData = static_cast<JavaLaunchData *>(data);
			if (!m_identifier.isValid())
			{
				jData->classpath.prepend(m_file);
			}
			else
			{
				MavenIdentifier existing;
				for (const auto entry : jData->classpathEntries.keys())
				{
					if (m_identifier.group() == entry.group() && m_identifier.artifact() == entry.artifact())
					{
						existing = entry;
						break;
					}
				}
				if (existing.isValid())
				{
					if (Version::fromString(existing.version()) < Version::fromString(m_identifier.version()))
					{
						jData->classpath.replace(jData->classpath.indexOf(jData->classpathEntries[existing]), m_file);
						jData->classpathEntries[m_identifier] = m_file;
						jData->classpathEntries.remove(existing);
					}
				}
				else
				{
					jData->classpathEntries[m_identifier] = m_file;
					jData->classpath.prepend(m_file);
				}
			}
		});
	}
	else
	{
		return nullptr;
	}
}
