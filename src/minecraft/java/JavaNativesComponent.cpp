// Licensed under the Apache-2.0 license. See README.md for details.

#include "JavaNativesComponent.h"

#include <QJsonObject>
#include <QJsonArray>
#include <KZip>

#include "core/components/FileDownloadComponent.h"
#include "minecraft/java/MavenIdentifier.h"
#include "core/util/Json.h"
#include "core/util/FileSystem.h"
#include "core/tasks/Task.h"
#include "core/tasks/LambdaTask.h"
#include "minecraft/java/JavaLaunchData.h"
#include "core/Container.h"

// TODO extract functionality in common with JavaLibraryComponent

class NativesExtractionTask : public Task
{
	Q_OBJECT
public:
	explicit NativesExtractionTask(JavaNativesComponent *component, IStageData *data, QObject *parent = nullptr)
		: Task(parent), m_component(component), m_data(data)
	{
	}

private:
	void run() override
	{
		const QDir nativesDir = "tmp/java/natives/" + m_data->container->uuid().toString().remove('{').remove('}');
		FS::ensureExists(nativesDir);
		const auto natives = m_component->natives();
		for (auto it = natives.constBegin(); it != natives.constEnd(); ++it)
		{
			if (LibraryPlatforms(it.key()) & currentLibraryPlatform)
			{
				KZip *zip = new KZip("java/libraries/" + m_component->nativeId(it.value()));
				if (!zip->open(QIODevice::ReadOnly))
				{
					throw Exception(tr("Unable to open natives archive %1: %2").arg(zip->fileName(), zip->device()->errorString()));
				}
				const KArchiveDirectory *root = zip->directory();
				for (const auto e : root->entries())
				{
					const KArchiveEntry *entry = root->entry(e);
					if (entry->isFile())
					{
						setStatus(tr("Extracting %1").arg(entry->name()));
						static_cast<const KArchiveFile *>(entry)->copyTo(nativesDir.absolutePath());
					}
				}
				delete zip;
			}
		}
		setSuccess();
	}

	JavaNativesComponent *m_component;
	IStageData *m_data;
};
class NativesCleanupTask : public Task
{
	Q_OBJECT
public:
	explicit NativesCleanupTask(IStageData *data, QObject *parent = nullptr)
		: Task(parent), m_data(data)
	{
	}

private:
	void run() override
	{
		const QDir nativesDir = "tmp/java/natives/" + m_data->container->uuid().toString().remove('{').remove('}');
		FS::remove(nativesDir);
		FS::removeEmptyRecursive(QDir("tmp"));
		setSuccess();
	}

	IStageData *m_data;
};

JavaNativesComponent::JavaNativesComponent(BaseComponent *parent)
	: BaseCompoundComponent(parent)
{
}

Task *JavaNativesComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::PreLaunch)
	{
		return new NativesExtractionTask(this, data);
	}
	else if (stage == Stage::Launch)
	{
		return new LambdaTask<std::function<void()>>([data]()
		{
			dynamic_cast<JavaLaunchData *>(data)->properties["java.library.path"] =
					QDir("tmp/java/natives/").absoluteFilePath(data->container->uuid().toString().remove('{').remove('}'));
		});
	}
	else if (stage == Stage::PostLaunch)
	{
		return new NativesCleanupTask(data);
	}
	else
	{
		return BaseCompoundComponent::createTask(stage, data);
	}
}

void JavaNativesComponent::setIdentifier(const QString &identifier)
{
	m_identifier = MavenIdentifier(identifier);
	setVersionInternal(Version::fromString(m_identifier.version()));
	setBaseUrl(QUrl("https://libraries.minecraft.net/"));
}
void JavaNativesComponent::setBaseUrl(const QUrl &url)
{
	m_baseUrl = url;
}

void JavaNativesComponent::setNatives(const QHash<LibraryPlatform, QString> &natives)
{
	for (auto comp : m_nativeDlComps)
	{
		removeChild(comp);
		delete comp;
	}
	m_nativeDlComps.clear();

	m_natives = natives;

	for (auto it = m_natives.constBegin(); it != m_natives.constEnd(); ++it)
	{
		if (LibraryPlatforms(it.key()) & currentLibraryPlatform)
		{
			FileDownloadComponent *comp = new FileDownloadComponent(this);
			comp->setDownloadGroup("java/libraries");
			comp->setDownloadId(nativeId(it.value()));
			comp->setUrl(m_baseUrl.resolved(comp->downloadId()));
			m_nativeDlComps.append(comp);
			addChild(comp);
		}
	}
}
void JavaNativesComponent::setAllowedOn(const LibraryPlatforms &platforms)
{
	m_allowedOn = platforms;

	if (!(m_allowedOn & currentLibraryPlatform))
	{
		setChildren(QList<BaseComponent *>());
	}
}

void JavaNativesComponent::load(const QJsonObject &obj)
{
	using namespace Json;
	setIdentifier(ensureIsType<QString>(obj, "name"));
	if (obj.contains("url"))
	{
		setBaseUrl(ensureIsType<QUrl>(obj, "url"));
	}
	if (obj.contains("natives"))
	{
		const QJsonObject nat = ensureObject(obj, "natives");
		QHash<LibraryPlatform, QString> natives;
		for (auto it = nat.constBegin(); it != nat.constEnd(); ++it)
		{
			natives.insert(parsePlatformString(it.key()), ensureIsType<QString>(it.value()));
		}
		setNatives(natives);
	}
	if (obj.contains("allowedOn"))
	{
		LibraryPlatforms platforms;
		for (const auto plat : ensureIsArrayOf<QString>(obj, "allowedOn"))
		{
			platforms |= parsePlatformString(plat);
		}
		setAllowedOn(platforms);
	}

	BaseCompoundComponent::load(obj);
}
QJsonObject JavaNativesComponent::save() const
{
	using namespace Json;
	QJsonObject obj = BaseCompoundComponent::save();
	obj.insert("name", m_identifier.toString());
	if (m_baseUrl.isValid())
	{
		obj.insert("url", toJson(m_baseUrl));
	}
	if (m_allowedOn != LibraryPlatforms(LibraryPlatform::All))
	{
		QStringList allowedOn;
		{
			if (m_allowedOn & LibraryPlatform::Linux)
			{
				allowedOn.append(platformToString(LibraryPlatform::Linux));
			}
			else
			{
				if (m_allowedOn & LibraryPlatform::Linux32)
				{
					allowedOn.append(platformToString(LibraryPlatform::Linux32));
				}
				if (m_allowedOn & LibraryPlatform::Linux64)
				{
					allowedOn.append(platformToString(LibraryPlatform::Linux64));
				}
			}
			if (m_allowedOn & LibraryPlatform::Win)
			{
				allowedOn.append(platformToString(LibraryPlatform::Win));
			}
			else
			{
				if (m_allowedOn & LibraryPlatform::Win32)
				{
					allowedOn.append(platformToString(LibraryPlatform::Win32));
				}
				if (m_allowedOn & LibraryPlatform::Win64)
				{
					allowedOn.append(platformToString(LibraryPlatform::Win64));
				}
			}
			if (m_allowedOn & LibraryPlatform::OSX)
			{
				allowedOn.append(platformToString(LibraryPlatform::OSX));
			}
			else
			{
				if (m_allowedOn & LibraryPlatform::OSX32)
				{
					allowedOn.append(platformToString(LibraryPlatform::OSX32));
				}
				if (m_allowedOn & LibraryPlatform::OSX64)
				{
					allowedOn.append(platformToString(LibraryPlatform::OSX64));
				}
			}
		}
		obj.insert("allowedOn", QJsonArray::fromStringList(allowedOn));
	}
	if (!m_natives.isEmpty())
	{
		QJsonObject natives;
		for (auto it = m_natives.constBegin(); it != m_natives.constEnd(); ++it)
		{
			natives.insert(platformToString(it.key()), it.value());
		}
		obj.insert("natives", natives);
	}
	return obj;
}

QString JavaNativesComponent::nativeId(const QString &platform) const
{
	return m_identifier.toPath().replace(".jar", "-" + platform + ".jar");
}

#include "JavaNativesComponent.moc"
