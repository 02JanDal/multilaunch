// Licensed under the Apache-2.0 license. See README.md for details.

#include "JavaUtils.h"

#include <QProcess>
#include <QDir>
#include <QStandardPaths>

QMap<QString, QString> getProperties(const QByteArray &javaOutput)
{
	const QStringList lines = QString::fromLocal8Bit(javaOutput).split(QRegExp("[\r\n]"), QString::SkipEmptyParts);
	QMap<QString, QString> out;
	for (const auto line : lines)
	{
		if (line.isEmpty() || !line.at(0).isLetter())
		{
			continue;
		}
		const int equalIndex = line.indexOf('=');
		out.insert(line.left(equalIndex), line.mid(equalIndex + 1));
	}
	return out;
}

QStringList getPossibleJavaExecutables()
{
	QStringList out;
#if defined(Q_OS_WIN)
	out << "C:/Program Files/Java/jre7/bin/javaw.exe"
		<< "C:/Program Files/Java/jre6/bin/javaw.exe"
		<< "C:/Program Files (x86)/Java/jre7/bin/javaw.exe"
		<< "C:/Program Files (x86)/Java/jre6/bin/javaw.exe";
	const QString javaw = QStandardPaths::findExecutable("javaw");
	if (!javaw.isEmpty())
	{
		out << javaw;
	}
#elif defined(Q_OS_LINUX)
	out << "/opt/java/bin/java"
		<< "/usr/bin/java";
	for (const auto entry : QDir("/usr/lib/jvm").entryList(QDir::Dirs | QDir::NoDotAndDotDot))
	{
		out << "/usr/lib/jvm/" + entry + "/bin/java";
		out << "/usr/lib/jvm/" + entry + "/jre/bin/java";
	}
#elif defined(Q_OS_OSX)
	out << "/Applications/Xcode.app/Contents/Applications/Application Loader.app/Contents/MacOS/itms/java/bin/java"
		<< "/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java"
		<< "/System/Library/Frameworks/JavaVM.framework/Versions/Current/Commands/java";
	for (const auto entry : QDir("/Library/Java/JavaVirtualMachines/").entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot))
	{
		out << entry.absoluteDir().absoluteFilePath("Contents/Home/bin/java")
			<< entry.absoluteDir().absoluteFilePath("Contents/Home/jre/bin/java");
	}
	for (const auto entry : QDir("/System/Library/Java/JavaVirtualMachines/").entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot))
	{
		out << entry.absoluteDir().absoluteFilePath("Contents/Home/bin/java")
			<< entry.absoluteDir().absoluteFilePath("Contents/Command/java");
	}
#else
#warning Unknown operating system. Only Javas found in PATH will be used.
#endif
	const QString java = QStandardPaths::findExecutable("java");
	if (!java.isEmpty())
	{
		out << java;
	}
	return out;
}
