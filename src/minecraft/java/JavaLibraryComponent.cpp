// Licensed under the Apache-2.0 license. See README.md for details.

#include "JavaLibraryComponent.h"

#include <QJsonObject>
#include <QJsonArray>

#include "minecraft/java/ClasspathComponent.h"
#include "minecraft/java/MavenIdentifier.h"
#include "minecraft/forge/ForgeXZUnpackTask.h"
#include "core/components/FileDownloadComponent.h"
#include "core/util/Json.h"
#include "core/util/FileSystem.h"
#include "core/tasks/Task.h"
#include "core/tasks/GroupedTask.h"

JavaLibraryComponent::JavaLibraryComponent(BaseComponent *parent)
	: BaseCompoundComponent(parent)
{
	m_cpComp = new ClasspathComponent(this);
	m_dlComp = new FileDownloadComponent(this);
	m_baseUrl = QUrl("https://libraries.minecraft.net/");
	setChildren({m_cpComp, m_dlComp});
}

Task *JavaLibraryComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::Installation)
	{
		Task *upstream = BaseCompoundComponent::createTask(stage, data);
		if (m_hint == ForgePackXZ)
		{
			return new GroupedTask({upstream, new ForgeXZUnpackTask(this)});
		}
		else
		{
			return upstream;
		}
	}
	else
	{
		return BaseCompoundComponent::createTask(stage, data);
	}
}

void JavaLibraryComponent::setIdentifier(const QString &identifier)
{
	m_identifier = MavenIdentifier(identifier);
	setVersionInternal(Version::fromString(m_identifier.version()));

	m_dlComp->setDownloadGroup("java/libraries");
	updateUrl();
}
void JavaLibraryComponent::setBaseUrl(const QUrl &url)
{
	m_baseUrl = url;
	m_absoluteUrl = QUrl();
	updateUrl();
}
void JavaLibraryComponent::setAbsoluteUrl(const QUrl &url)
{
	m_absoluteUrl = url;
	m_baseUrl = QUrl("https://libraries.minecraft.net/");
	updateUrl();
}

void JavaLibraryComponent::setHint(const Hint hint)
{
	m_hint = hint;
	updateUrl();
}

void JavaLibraryComponent::setAllowedOn(const LibraryPlatforms &platforms)
{
	m_allowedOn = platforms;

	if (!(m_allowedOn & currentLibraryPlatform))
	{
		setChildren(QList<BaseComponent *>());
	}
}

void JavaLibraryComponent::load(const QJsonObject &obj)
{
	setChildren({m_cpComp, m_dlComp});

	using namespace Json;
	setIdentifier(ensureIsType<QString>(obj, "name"));
	if (obj.contains("url"))
	{
		setBaseUrl(ensureIsType<QUrl>(obj, "url"));
	}
	if (obj.contains("absoluteUrl"))
	{
		setAbsoluteUrl(ensureIsType<QUrl>(obj, "absoluteUrl"));
	}
	if (obj.contains("allowedOn"))
	{
		LibraryPlatforms platforms;
		for (const auto plat : ensureIsArrayOf<QString>(obj, "allowedOn"))
		{
			platforms |= parsePlatformString(plat);
		}
		setAllowedOn(platforms);
	}

	BaseCompoundComponent::load(obj);
}
QJsonObject JavaLibraryComponent::save() const
{
	using namespace Json;
	QJsonObject obj = BaseCompoundComponent::save();
	obj.insert("name", m_identifier.toString());
	if (m_baseUrl.isValid())
	{
		obj.insert("url", toJson(m_baseUrl));
	}
	if (m_absoluteUrl.isValid())
	{
		obj.insert("absoluteUrl", toJson(m_absoluteUrl));
	}
	if (m_allowedOn != LibraryPlatforms(LibraryPlatform::All))
	{
		QStringList allowedOn;
		{
			if (m_allowedOn & LibraryPlatform::Linux)
			{
				allowedOn.append(platformToString(LibraryPlatform::Linux));
			}
			else
			{
				if (m_allowedOn & LibraryPlatform::Linux32)
				{
					allowedOn.append(platformToString(LibraryPlatform::Linux32));
				}
				if (m_allowedOn & LibraryPlatform::Linux64)
				{
					allowedOn.append(platformToString(LibraryPlatform::Linux64));
				}
			}
			if (m_allowedOn & LibraryPlatform::Win)
			{
				allowedOn.append(platformToString(LibraryPlatform::Win));
			}
			else
			{
				if (m_allowedOn & LibraryPlatform::Win32)
				{
					allowedOn.append(platformToString(LibraryPlatform::Win32));
				}
				if (m_allowedOn & LibraryPlatform::Win64)
				{
					allowedOn.append(platformToString(LibraryPlatform::Win64));
				}
			}
			if (m_allowedOn & LibraryPlatform::OSX)
			{
				allowedOn.append(platformToString(LibraryPlatform::OSX));
			}
			else
			{
				if (m_allowedOn & LibraryPlatform::OSX32)
				{
					allowedOn.append(platformToString(LibraryPlatform::OSX32));
				}
				if (m_allowedOn & LibraryPlatform::OSX64)
				{
					allowedOn.append(platformToString(LibraryPlatform::OSX64));
				}
			}
		}
		obj.insert("allowedOn", QJsonArray::fromStringList(allowedOn));
	}
	return obj;
}

void JavaLibraryComponent::updateUrl()
{
	m_cpComp->setFile("java/libraries/" + m_identifier.toPath());
	m_cpComp->setIdentifier(m_identifier);
	if (m_absoluteUrl.isValid())
	{
		m_dlComp->setUrl(m_absoluteUrl);
		m_dlComp->setDownloadId(m_identifier.toPath());
	}
	else
	{
		if (m_hint == ForgePackXZ)
		{
			m_dlComp->setUrl(m_baseUrl.resolved(m_identifier.toPath() + ".pack.xz"));
			m_dlComp->setDownloadId(m_identifier.toPath() + ".pack.xz");
		}
		else
		{
			m_dlComp->setUrl(m_baseUrl.resolved(m_identifier.toPath()));
			m_dlComp->setDownloadId(m_identifier.toPath());
		}
	}
}

#include "JavaLibraryComponent.moc"
