// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseComponent.h"
#include "MavenIdentifier.h"

class ClasspathComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit ClasspathComponent(BaseComponent *parent);

	QString id() const override { return "Java.Classpath"; }
	QString name() const override { return m_file; }
	QString type() const override { return tr("Classpath entry"); }
	bool isImplicit() const override { return true; }
	Task *createTask(const Stage stage, IStageData *data = nullptr) override;

	void setFile(const QString &file) { m_file = file; }
	QString file() const { return m_file; }

	void setIdentifier(const MavenIdentifier &identifier) { m_identifier = identifier; }
	MavenIdentifier identifier() const { return m_identifier; }

private:
	QString m_file;
	MavenIdentifier m_identifier;
};
