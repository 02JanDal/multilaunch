// Licensed under the Apache-2.0 license. See README.md for details.

#include "JavaLibraryPlatform.h"

#include <QString>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
typedef QHash<LibraryPlatform, QString> AsdfType;
Q_GLOBAL_STATIC_WITH_ARGS(AsdfType, libraryPlatformToStringMapping, ({{LibraryPlatform::Linux, "linux"},
																	  {LibraryPlatform::Linux32, "linux32"},
																	  {LibraryPlatform::Linux64, "linux64"},
																	  {LibraryPlatform::Win, "windows"},
																	  {LibraryPlatform::Win32, "windows32"},
																	  {LibraryPlatform::Win64, "windows64"},
																	  {LibraryPlatform::OSX, "osx"},
																	  {LibraryPlatform::OSX32, "osx32"},
																	  {LibraryPlatform::OSX64, "osx64"},
																	  {LibraryPlatform::All, "all"}}))
#pragma clang diagnostic pop
LibraryPlatform parsePlatformString(const QString &string)
{
	return libraryPlatformToStringMapping->key(string);
}
QString platformToString(const LibraryPlatform platform)
{
	return libraryPlatformToStringMapping->value(platform);
}
