// Licensed under the Apache-2.0 license. See README.md for details.

#include "JavaVersionList.h"

#include "core/tasks/StandardTask.h"
#include "core/util/Json.h"
#include "JavaUtils.h"

class JavaDetectionTask : public StandardTask
{
public:
	explicit JavaDetectionTask(QObject *parent = nullptr) : StandardTask(parent)
	{
	}

private:
	void run() override
	{
		// start by cleaning duplicates (mainly because of symlinks)
		QStringList paths;
		for (const auto possible : getPossibleJavaExecutables())
		{
			const QString followed = QFileInfo(possible).canonicalFilePath();
			if (QFileInfo(followed).exists())
			{
				paths << followed;
			}
		}
		paths.removeDuplicates();

		setStatus(tr("Querying Javas..."));
		setProgress(101);
		QList<QSharedPointer<BaseVersion>> javas;
		for (const auto path : paths)
		{
			try
			{
				setStatus(tr("Trying %1").arg(path));
				const QMap<QString, QString> props = getProperties(runProcess(
					path, QStringList()
							  << "-jar"
							  << QDir::current().absoluteFilePath("java/jars/JavaQuery.jar"),
					QDir::current()));
				javas.append(QSharedPointer<JavaVersion>(new JavaVersion(
					props["java.version"], path, QDir(props["java.home"]).absolutePath(),
					props["java.vm.name"], props["os.arch"], props)));
			}
			catch (...)
			{
				// that java didn't work
			}
		}

		g_javaVersions->setVersions(javas);
		setProgress(100);
		setSuccess();
	}
};

DEFINE_SINGLETON(g_javaVersions)

JavaVersionList::JavaVersionList(QObject *parent)
	: BaseVersionList("java/versions.json", new VersionFactory<JavaVersion>, parent)
{
}

int JavaVersionList::columnCount(const QModelIndex &parent) const
{
	return 4;
}
QVariant JavaVersionList::data(const QModelIndex &index, int role) const
{
	const QSharedPointer<JavaVersion> version = at(index.row()).dynamicCast<JavaVersion>();
	if (role == Qt::DisplayRole)
	{
		switch (index.column())
		{
		case 0: return version->vmName();
		case 1: return version->id().toString();
		case 2: return version->type();
		case 3: return version->directory().absolutePath();
		}
	}
	return QVariant();
}
QVariant JavaVersionList::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
	{
		switch (section)
		{
		case 0: return tr("VM Name");
		case 1: return tr("Version");
		case 2: return tr("Arch");
		case 3: return tr("Directory");
		}
	}
	return QVariant();
}

QSharedPointer<JavaVersion> JavaVersionList::find(const QString &dir) const
{
	for (int i = 0; i < size(); ++i)
	{
		QSharedPointer<JavaVersion> version = at(i).dynamicCast<JavaVersion>();
		if (version->javaPath() == dir)
		{
			return version;
		}
	}
	return QSharedPointer<JavaVersion>();
}

Task *JavaVersionList::createRefreshTask()
{
	return new JavaDetectionTask;
}


JavaVersion::JavaVersion(const QString &version, const QString &javaPath, const QDir &directory,
						 const QString &vmName,const QString &arch, const QMap<QString, QString> &properties)
	: m_version(Version::fromString(version)), m_javaPath(javaPath), m_dir(directory), m_vmName(vmName), m_arch(arch), m_properties(properties)
{
}
JavaVersion::JavaVersion()
{
}
bool JavaVersion::is64bit() const
{
	return m_arch == "x86_64" || m_arch == "amd64";
}
void JavaVersion::load(const QJsonObject &obj)
{
	using namespace Json;
	m_version = Version::fromString(ensureIsType<QString>(obj, "version"));
	m_javaPath = ensureIsType<QString>(obj, "javaPath");
	m_dir = ensureIsType<QDir>(obj, "javaDir");
	m_vmName = ensureIsType<QString>(obj, "vmName");
	m_arch = ensureIsType<QString>(obj, "arch");
	m_properties.clear();
	for (auto it = ensureObject(obj, "properties").constBegin();
		 it != ensureObject(obj, "properties").constEnd(); ++it)
	{
		m_properties.insert(it.key(), ensureIsType<QString>(it.value()));
	}
}
QJsonObject JavaVersion::save() const
{
	QJsonObject obj;
	obj.insert("version", m_version.toString());
	obj.insert("javaPath", m_javaPath);
	obj.insert("javaDir", Json::toJson(m_dir));
	obj.insert("vmName", m_vmName);
	obj.insert("arch", m_arch);
	QJsonObject properties;
	for (auto it = m_properties.constBegin(); it != m_properties.constEnd(); ++it)
	{
		properties.insert(it.key(), it.value());
	}
	obj.insert("properties", properties);
	return obj;
}
