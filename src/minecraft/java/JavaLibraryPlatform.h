// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QFlag>
#include <QHash>

enum class LibraryPlatform
{
	Linux32 = 0x01,
	Linux64 = 0x02,
	Linux = LibraryPlatform::Linux32 | LibraryPlatform::Linux64,

	Win32 = 0x04,
	Win64 = 0x08,
	Win = LibraryPlatform::Win32 | LibraryPlatform::Win64,

	OSX32 = 0x10,
	OSX64 = 0x20,
	OSX = LibraryPlatform::OSX32 | LibraryPlatform::OSX64,

	Invalid = 0x00,
	All = LibraryPlatform::Linux | LibraryPlatform::Win | LibraryPlatform::OSX
};
Q_DECLARE_FLAGS(LibraryPlatforms, LibraryPlatform)
constexpr uint qHash(const LibraryPlatform &platform)
{
	return qHash(int(platform));
}
#if defined(Q_OS_WIN)
# define currentLibraryPlatform LibraryPlatform::Win
#elif defined(Q_OS_LINUX)
# define currentLibraryPlatform LibraryPlatform::Linux
#elif defined(Q_OS_OSX)
# define currentLibraryPlatform LibraryPlatform::OSX
#endif
LibraryPlatform parsePlatformString(const QString &string);
QString platformToString(const LibraryPlatform platform);
