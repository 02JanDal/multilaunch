// Licensed under the Apache-2.0 license. See README.md for details.

#include "MavenIdentifier.h"

#include <QRegularExpression>

MavenIdentifier::MavenIdentifier(const QString &identifier)
{
	parse(identifier);
}

QString MavenIdentifier::toString() const
{
	QString out = m_group + ':' + m_artifact + ':' + m_version;
	if (!m_classifier.isEmpty())
	{
		out.append(':' + m_classifier);
	}
	if (m_extension != "jar")
	{
		out.append('@' + m_extension);
	}
	return out;
}
QString MavenIdentifier::toPath() const
{
	QString out = QString("%1/%2/%3/%2-%3").arg(QString(m_group).replace('.', '/'), m_artifact, m_version);
	if (!m_classifier.isEmpty())
	{
		out.append('-' + m_classifier);
	}
	out.append('.' + m_extension);
	return out;
}

bool MavenIdentifier::isValid() const
{
	return !m_group.isNull() && !m_artifact.isNull() && !m_version.isNull();
}

void MavenIdentifier::parse(const QString &identifier)
{
	QRegularExpression exp("([^:@]+):([^:@]+):([^:@]+)(:(?<classifier>[^:@]+))?(@(?<extension>[^:@]+))?");
	QRegularExpressionMatch match = exp.match(identifier);
	if (!match.hasMatch())
	{
		return;
	}
	m_group = match.captured(1);
	m_artifact = match.captured(2);
	m_version = match.captured(3);
	m_classifier = match.captured("classifier");
	m_extension = match.captured("extension").isEmpty() ? m_extension : match.captured("extension");
}
