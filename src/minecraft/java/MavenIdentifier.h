// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QString>

class MavenIdentifier
{
public:
	explicit MavenIdentifier(const QString &identifier);
	explicit MavenIdentifier() {}

	bool operator==(const MavenIdentifier &other) const
	{
		return m_group == other.m_group &&
				m_artifact == other.m_artifact &&
				m_version == other.m_version &&
				m_classifier == other.m_classifier &&
				m_extension == other.m_extension;
	}
	bool operator<(const MavenIdentifier &other) const
	{
		return toString() < other.toString();
	}

	QString toString() const;
	QString toPath() const;

	bool isValid() const;

	QString group() const { return m_group; }
	QString artifact() const { return m_artifact; }
	QString version() const { return m_version; }
	QString classifier() const { return m_classifier; }
	QString extension() const { return m_extension; }

	void setGroup(const QString &group) { m_group = group; }
	void setArtifact(const QString &artifact) { m_artifact = artifact; }
	void setVersion(const QString &version) { m_version = version; }
	void setClassifier(const QString &classifier) { m_classifier = classifier; }
	void setExtension(const QString &extension) { m_extension = extension; }

private:
	QString m_group;
	QString m_artifact;
	QString m_version;
	QString m_classifier;
	QString m_extension = "jar";

	void parse(const QString &identifier);
};
