// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseCompoundComponent.h"

#include <QHash>
#include <QUrl>

#include "minecraft/java/MavenIdentifier.h"
#include "minecraft/java/JavaLibraryPlatform.h"

class FileDownloadComponent;

class JavaNativesComponent : public BaseCompoundComponent
{
	Q_OBJECT
public:
	explicit JavaNativesComponent(BaseComponent *parent);

	QString id() const override { return "Java.Natives"; }
	QString name() const override { return m_identifier.artifact(); }
	QString type() const override { return tr("Java Native Library"); }
	Task *createTask(const Stage stage, IStageData *data) override;
	bool isImplicit() const override { return !parent()->inherits("Container"); }

	void setIdentifier(const QString &identifier);
	MavenIdentifier identifier() const { return m_identifier; }
	void setBaseUrl(const QUrl &url);

	void setNatives(const QHash<LibraryPlatform, QString> &natives);
	QHash<LibraryPlatform, QString> natives() const { return m_natives; }

	void setAllowedOn(const LibraryPlatforms &platforms);
	LibraryPlatforms allowedOn() const { return m_allowedOn; }

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

	QString nativeId(const QString &platform) const;

private:
	MavenIdentifier m_identifier;
	QHash<LibraryPlatform, QString> m_natives;
	LibraryPlatforms m_allowedOn = LibraryPlatform::All;

	QUrl m_baseUrl;
	QList<FileDownloadComponent *> m_nativeDlComps;
};
