// Licensed under the Apache-2.0 license. See README.md for details.

#include "JavaArgumentsComponent.h"

#include <QGroupBox>
#include <QFormLayout>
#include <QComboBox>
#include <QVBoxLayout>
#include <QSpinBox>
#include <QPlainTextEdit>

#include "core/util/Json.h"
#include "core/tasks/LambdaTask.h"
#include "core/IPage.h"
#include "core/GlobalSettings.h"
#include "gui/widgets/VersionSelector.h"
#include "minecraft/java/JavaLaunchData.h"
#include "JavaVersionList.h"

class JavaExecutablePagePart : public IPagePart
{
public:
	explicit JavaExecutablePagePart(JavaArgumentsComponent *component)
		: m_component(component) {}

	QString pageId() const override { return "java"; }
	QString name() const override { return JavaArgumentsComponent::tr("Java Executable"); }
	QObject *createWidget() const override
	{
		QGroupBox *box = new QGroupBox;
		QVBoxLayout *layout = new QVBoxLayout(box);

		VersionSelector *selector = new VersionSelector(g_javaVersions.get(), box);
		selector->setNullText(JavaArgumentsComponent::tr("Using global default"));
		layout->addWidget(selector);

		return box;
	}
	void initializePart(QObject *widget) override
	{
		widget->findChild<VersionSelector *>()->setVersion(m_component->javaVersion());
	}
	void applyPart(QObject *widget) override
	{
		m_component->setJavaVersion(widget->findChild<VersionSelector *>()->version().lock().dynamicCast<JavaVersion>());
	}
	bool hasChanges(QObject *widget) override
	{
		const QSharedPointer<JavaVersion> old = m_component->javaVersion();
		const QSharedPointer<JavaVersion> nev = widget->findChild<VersionSelector *>()->version().lock().dynamicCast<JavaVersion>();
		if (old && nev)
		{
			return !old->operator ==(nev);
		}
		else
		{
			return !old.isNull() || !nev.isNull();
		}
	}

private:
	JavaArgumentsComponent *m_component;
};
class MemoryPagePart : public IPagePart
{
public:
	explicit MemoryPagePart(JavaArgumentsComponent *component)
		: m_component(component)
	{
	}

	QString pageId() const { return "java"; }
	QString name() const { return JavaArgumentsComponent::tr("Memory"); }
	QObject *createWidget() const
	{
		QGroupBox *box = new QGroupBox;
		box->setTitle(name());
		QFormLayout *layout = new QFormLayout(box);

		QSpinBox *minMem = new QSpinBox(box);
		minMem->setObjectName("MinMemorySpinBox");
		minMem->setMinimum(64);
		minMem->setMaximum(16384);
		minMem->setSuffix("MB");
		layout->addRow(JavaArgumentsComponent::tr("Min:"), minMem);
		QSpinBox *maxMem = new QSpinBox(box);
		maxMem->setObjectName("MaxMemorySpinBox");
		maxMem->setMinimum(64);
		maxMem->setMaximum(16384);
		maxMem->setSuffix("MB");
		layout->addRow(JavaArgumentsComponent::tr("Max:"), maxMem);
		QSpinBox *permGen = new QSpinBox(box);
		permGen->setObjectName("PermGenSpinBox");
		permGen->setMinimum(64);
		permGen->setMaximum(16384);
		permGen->setSuffix("MB");
		layout->addRow(JavaArgumentsComponent::tr("PermGen:"), permGen);

		return box;
	}
	void initializePart(QObject *widget)
	{
		widget->findChild<QSpinBox *>("MinMemorySpinBox")->setValue(m_component->minMemory());
		widget->findChild<QSpinBox *>("MaxMemorySpinBox")->setValue(m_component->maxMemory());
		widget->findChild<QSpinBox *>("PermGenSpinBox")->setValue(m_component->permGen());
	}
	void applyPart(QObject *widget)
	{
		m_component->setMinMemory(widget->findChild<QSpinBox *>("MinMemorySpinBox")->value());
		m_component->setMaxMemory(widget->findChild<QSpinBox *>("MaxMemorySpinBox")->value());
		m_component->setPermGen(widget->findChild<QSpinBox *>("PermGenSpinBox")->value());
	}
	bool hasChanges(QObject *widget)
	{
		return widget->findChild<QSpinBox *>("MinMemorySpinBox")->value() != m_component->minMemory() ||
				widget->findChild<QSpinBox *>("MaxMemorySpinBox")->value() != m_component->maxMemory() ||
				widget->findChild<QSpinBox *>("PermGenSpinBox")->value() != m_component->permGen();
	}

private:
	JavaArgumentsComponent *m_component;
};
class ArgumentsPagePart : public IPagePart
{
public:
	explicit ArgumentsPagePart(JavaArgumentsComponent *component)
		: m_component(component)
	{
	}

	QString pageId() const { return "java"; }
	QString name() const { return JavaArgumentsComponent::tr("Arguments"); }
	QObject *createWidget() const
	{
		QGroupBox *box = new QGroupBox;

		QPlainTextEdit *edit = new QPlainTextEdit(box);
		edit->setPlaceholderText(JavaArgumentsComponent::tr("Enter additional arguments you want to give to the JVM here"));
		box->setToolTip(edit->placeholderText());

		QVBoxLayout *layout = new QVBoxLayout(box);
		layout->addWidget(edit);

		return box;
	}
	void initializePart(QObject *widget)
	{
		widget->findChild<QPlainTextEdit *>()->setPlainText(m_component->arguments());
	}
	void applyPart(QObject *widget)
	{
		m_component->setArguments(widget->findChild<QPlainTextEdit *>()->toPlainText());
	}
	bool hasChanges(QObject *widget)
	{
		return widget->findChild<QPlainTextEdit *>()->toPlainText() != m_component->arguments();
	}

private:
	JavaArgumentsComponent *m_component;
};

JavaArgumentsComponent::JavaArgumentsComponent(BaseComponent *parent)
	: BaseComponent(parent)
{
	setPackage(Package("java.arguments"));
}

Task *JavaArgumentsComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::PreLaunch)
	{
		return g_javaVersions->createRefreshTask();
	}
	else if (stage == Stage::Launch)
	{
		return new LambdaTask<std::function<void()>>([this, data]()
		{
			dynamic_cast<JavaLaunchData *>(data)->javaArgs = m_args.split(' ')
					<< QString("-Xms%1M").arg(m_minMemory) << QString("-Xmx%1M").arg(m_maxMemory) << QString("-XX:PermSize=%1").arg(m_permGen);
			QSharedPointer<JavaVersion> version = g_javaVersions->find(m_javaDir);
			if (!version)
			{
				version = g_javaVersions->find(g_settings->get<QString>("java.defaultPath"));
			}
			if (version)
			{
				dynamic_cast<JavaLaunchData *>(data)->javaVersion = version;
			}
			else
			{
				throw Exception("No Java version selected for neither the container, nor on a global scope");
			}
		});
	}
	else
	{
		return nullptr;
	}
}

void JavaArgumentsComponent::load(const QJsonObject &obj)
{
	using namespace Json;
	m_maxMemory = ensureIsType<int>(obj, "maxMemory", m_maxMemory.defaultValue());
	m_minMemory = ensureIsType<int>(obj, "minMemory", m_minMemory.defaultValue());
	m_permGen = ensureIsType<int>(obj, "permGen", m_permGen.defaultValue());
	m_javaDir = ensureIsType<QString>(obj, "javaDir");
	m_args = ensureIsType<QString>(obj, "args");
	BaseComponent::load(obj);
}
QJsonObject JavaArgumentsComponent::save() const
{
	QJsonObject obj = BaseComponent::save();
	obj.insert("args", m_args);
	obj.insert("javaDir", m_javaDir);
	obj.insert("maxMemory", m_maxMemory.get());
	obj.insert("minMemory", m_minMemory.get());
	obj.insert("permGen", m_permGen.get());
	return obj;
}

void JavaArgumentsComponent::setJavaVersion(const QSharedPointer<JavaVersion> &version)
{
	if (version.isNull())
	{
		m_javaDir.clear();
	}
	else
	{
		m_javaDir = version->javaPath();
	}
}
QSharedPointer<JavaVersion> JavaArgumentsComponent::javaVersion() const
{
	return g_javaVersions->find(m_javaDir);
}

QList<IPagePart *> JavaArgumentsComponent::createPageParts(const bool isRunning)
{
	if (isRunning)
	{
		return QList<IPagePart *>();
	}
	else
	{
		return QList<IPagePart *>() << new JavaExecutablePagePart(this) << new MemoryPagePart(this) << new ArgumentsPagePart(this);
	}
}
