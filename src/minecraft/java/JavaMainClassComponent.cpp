// Licensed under the Apache-2.0 license. See README.md for details.

#include "JavaMainClassComponent.h"

#include "core/util/Json.h"
#include "core/tasks/LambdaTask.h"
#include "minecraft/java/JavaLaunchData.h"
#include "gui/dialogs/PageDialog.h"

class JavaPartsPage : public PagePartsPage
{
public:
	QString id() const { return "java"; }
	QString name() const { return QObject::tr("Java"); }
	QString header() const { return QObject::tr("Java"); }
	QString icon() const { return "java"; }
};

JavaMainClassComponent::JavaMainClassComponent(BaseComponent *parent)
	: BaseComponent(parent)
{
}

Task *JavaMainClassComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::Launch)
	{
		return new LambdaTask<std::function<void()>>([this, data]()
		{
			static_cast<JavaLaunchData *>(data)->mainClass = m_class;
		});
	}
	else
	{
		return nullptr;
	}
}

void JavaMainClassComponent::load(const QJsonObject &obj)
{
	m_class = Json::ensureIsType<QString>(obj, "class");

	BaseComponent::load(obj);
}
QJsonObject JavaMainClassComponent::save() const
{
	QJsonObject obj = BaseComponent::save();
	obj.insert("class", m_class);
	return obj;
}

QList<IPage *> JavaMainClassComponent::createPages(const bool isRunning)
{
	return QList<IPage *>() << new JavaPartsPage;
}
