// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseComponent.h"
#include "core/util/Optional.h"

class JavaVersion;

class JavaArgumentsComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit JavaArgumentsComponent(BaseComponent *parent);

	QString id() const override { return "Java.Arguments"; }
	QString name() const override { return m_args; }
	QString type() const override { return tr("Java Arguments"); }
	Task *createTask(const Stage stage, IStageData *data) override;

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

	void setArguments(const QString &args) { m_args = args; }
	QString arguments() const { return m_args; }

	void setMaxMemory(const int max) { m_maxMemory = max; }
	int maxMemory() const { return m_maxMemory; }

	void setMinMemory(const int min) { m_minMemory = min; }
	int minMemory() const { return m_minMemory; }

	void setPermGen(const int permGen) { m_permGen = permGen; }
	int permGen() const { return m_permGen; }

	void setJavaVersion(const QSharedPointer<JavaVersion> &version);
	QSharedPointer<JavaVersion> javaVersion() const;

	QList<IPagePart *> createPageParts(const bool isRunning) override;

private:
	QString m_javaDir;
	Optional<int, 512> m_maxMemory;
	Optional<int, 128> m_minMemory;
	Optional<int, 64> m_permGen;
	QString m_args;
};
