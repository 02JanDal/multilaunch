// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QMap>
#include <QStringList>

QMap<QString, QString> getProperties(const QByteArray &javaOutput);
QStringList getPossibleJavaExecutables();
