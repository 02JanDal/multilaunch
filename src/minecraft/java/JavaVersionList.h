// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QDir>

#include "core/BaseVersionList.h"
#include "core/BaseVersion.h"
#include "core/util/Singleton.h"

class JavaVersion : public BaseVersion
{
public:
	explicit JavaVersion(const QString &version, const QString &javaPath, const QDir &directory, const QString &vmName,
						 const QString &arch, const QMap<QString, QString> &properties);
	explicit JavaVersion();

	Version id() const override { return m_version; }
	QString javaPath() const { return m_javaPath; }
	QDir directory() const { return m_dir; }
	QString vmName() const { return m_vmName; }
	QString type() const override { return m_arch; }
	bool is64bit() const;
	bool is32bit() const { return !is64bit(); }
	QMap<QString, QString> properties() const { return m_properties; }

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

	bool operator>(const QSharedPointer<BaseVersion> &other) const override
	{
		const QSharedPointer<JavaVersion> version = other.dynamicCast<JavaVersion>();
		if (!version)
		{
			return false;
		}
		if (is64bit() == version->is64bit())
		{
			return m_version > version->id();
		}
		else
		{
			if (is64bit())
			{
				// we're 64bit => we're better
				return true;
			}
			else
			{
				// they're 64bit => they're better
				return false;
			}
		}
	}
	bool operator==(const QSharedPointer<BaseVersion> &other) const override
	{
		if (const QSharedPointer<JavaVersion> version = other.dynamicCast<JavaVersion>())
		{
			return version->m_dir == m_dir && version->m_version == m_version;
		}
		else
		{
			return false;
		}
	}

private:
	Version m_version;
	QString m_javaPath;
	QDir m_dir;
	QString m_vmName;
	QString m_arch;
	QMap<QString, QString> m_properties;
};
class JavaVersionList : public BaseVersionList
{
	Q_OBJECT
public:
	explicit JavaVersionList(QObject *parent = nullptr);

	int columnCount(const QModelIndex &parent) const override;
	QVariant data(const QModelIndex &index, int role) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

	QSharedPointer<JavaVersion> find(const QString &dir) const;

	Task *createRefreshTask() override;

private:
	friend class JavaDetectionTask;
};
DECLARE_SINGLETON(JavaVersionList, g_javaVersions)
