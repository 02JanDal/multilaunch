// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseCompoundComponent.h"

#include <QUrl>

#include "minecraft/java/MavenIdentifier.h"
#include "minecraft/java/JavaLibraryPlatform.h"

class ClasspathComponent;
class FileDownloadComponent;

class JavaLibraryComponent : public BaseCompoundComponent
{
	Q_OBJECT
public:
	explicit JavaLibraryComponent(BaseComponent *parent);

	enum Hint
	{
		NoHint,
		ForgePackXZ,
		Local
	};

	QString id() const override { return "Java.Library"; }
	QString name() const override { return m_identifier.artifact(); }
	QString type() const override { return tr("Java Library"); }
	bool isImplicit() const override { return !parent()->inherits("Container"); }

	Task *createTask(const Stage stage, IStageData *data) override;

	void setIdentifier(const QString &identifier);
	MavenIdentifier identifier() const { return m_identifier; }

	void setBaseUrl(const QUrl &url);
	QUrl baseUrl() const { return m_baseUrl; }
	void setAbsoluteUrl(const QUrl &url);
	QUrl absoluteUrl() const { return m_absoluteUrl; }

	void setAllowedOn(const LibraryPlatforms &platforms);
	LibraryPlatforms allowedOn() const { return m_allowedOn; }

	void setHint(const Hint hint);
	Hint hint() const { return m_hint; }

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

private:
	void updateUrl();

	MavenIdentifier m_identifier;
	LibraryPlatforms m_allowedOn = LibraryPlatform::All;

	QUrl m_baseUrl;
	QUrl m_absoluteUrl;

	Hint m_hint = NoHint;

	ClasspathComponent *m_cpComp;
	FileDownloadComponent *m_dlComp;
};
