// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseComponent.h"

#include <QStringList>
#include <QMap>

class JavaVersion;
class MavenIdentifier;

class JavaLaunchData : public BaseLaunchData
{
public:
	QSharedPointer<JavaVersion> javaVersion;
	QString workingDirectory;
	QList<QString> classpath;
	QMap<MavenIdentifier, QString> classpathEntries;
	QString mainClass;
	QStringList javaArgs;
	QMap<QString, QString> properties;
};
