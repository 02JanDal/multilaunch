// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseComponent.h"

class JavaMainClassComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit JavaMainClassComponent(BaseComponent *parent);

	QString id() const override { return "Java.MainClass"; }
	QString name() const override { return m_class; }
	QString type() const override { return tr("Java Main Class"); }
	Task *createTask(const Stage stage, IStageData *data) override;

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

	void setClass(const QString &clazz) { m_class = clazz; }
	QString clazz() const { return m_class; }

	QList<IPage *> createPages(const bool isRunning) override;

private:
	QString m_class;
};
