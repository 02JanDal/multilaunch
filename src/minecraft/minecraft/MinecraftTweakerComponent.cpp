// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftTweakerComponent.h"

#include "core/tasks/LambdaTask.h"
#include "MinecraftLaunchData.h"

MinecraftTweakerComponent::MinecraftTweakerComponent(BaseComponent *parent)
	: BaseComponent(parent)
{
}

Task *MinecraftTweakerComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::Launch)
	{
		return new LambdaTask<std::function<void()>>([this, data]()
		{
			MinecraftLaunchData *mcData = dynamic_cast<MinecraftLaunchData *>(data);
			if (!m_tweakerClass.isNull() && mcData)
			{
				mcData->mcArgs += " --tweakClass " + m_tweakerClass;
			}
		});
	}
	else
	{
		return nullptr;
	}
}
