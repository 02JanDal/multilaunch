// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QObject>

#include "core/accounts/BaseAccountType.h"
#include "core/accounts/BaseAccount.h"

class MinecraftAccount : public BaseAccount
{
public:
	explicit MinecraftAccount();

	QString avatar() const override;
	QString bigAvatar() const override;
	QString type() const override { return "minecraft"; }

	QString loginUsername() const override { return token("login_username"); }

	Task *createLoginTask(const QString &username, const QString &password) override;
	Task *createCheckTask() override;
	Task *createLogoutTask() override;
};

class MinecraftAccountType : public BaseAccountType
{
public:
	QString id() const override { return "minecraft"; }
	QString text() const override { return QObject::tr("Minecraft"); }
	QString icon() const override { return "minecraft"; }
	QString usernameText() const override { return QObject::tr("E-Mail/Username:"); }
	QString passwordText() const override { return QObject::tr("Password:"); }
	BaseAccount *createAccount() override { return new MinecraftAccount; }
	Type type() const override { return UsernamePassword; }
};
