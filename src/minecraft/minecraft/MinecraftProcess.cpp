// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftProcess.h"

#include <QProcess>
#include <QRegularExpression>

#include "core/accounts/BaseAccount.h"
#include "core/Container.h"
#include "MinecraftLaunchData.h"
#include "minecraft/java/JavaVersionList.h"

MinecraftProcess::MinecraftProcess(Container *container, BaseLaunchData *data, QObject *parent)
	: BaseProcess(data, parent), m_container(container)
{
	m_process = new QProcess(this);

	connect(m_process, &QProcess::readyReadStandardOutput, this, &MinecraftProcess::handleOutput);
	connect(m_process, &QProcess::readyReadStandardError, this, &MinecraftProcess::handleOutput);
	connectProcess(m_process);
}

void MinecraftProcess::start()
{
	emit stateChanged(Starting);
	MinecraftLaunchData *mcData = static_cast<MinecraftLaunchData *>(m_data);
	m_process->setWorkingDirectory(mcData->workingDirectory);
	m_process->setProgram(mcData->javaVersion->javaPath());
	m_process->setArguments(getArguments(mcData));
	emit log(tr("Java: %1 (%2, %3)").arg(mcData->javaVersion->vmName(), mcData->javaVersion->id().toString(), mcData->javaVersion->type()), Extra);
	emit log(tr("JVM Arguments: %1").arg(mcData->javaArgs.join(' ')), Extra);
	emit log(tr("Minecraft Arguments: %1").arg(getMinecraftArguments(mcData).join(' ')), Extra);
	emit log(tr("Classpath entries:"), Extra);
	for (const auto cp : mcData->classpath)
	{
		emit log("  " + QDir::current().absoluteFilePath(cp), Extra);
	}
	emit log(tr("Main class: %1").arg(mcData->mainClass), Extra);
	emit log();
	m_process->start();
}
void MinecraftProcess::abort()
{
	m_exitState = UserAborted;
	m_process->terminate();
}

void MinecraftProcess::handleOutput()
{
	QStringList outLines = QString(QString::fromLocal8Bit(m_process->readAllStandardOutput()).remove('\r')).split('\n');
	for (const auto line : outLines)
	{
		if (line.isEmpty())
		{
			continue;
		}
		handleLogLine(censor(line));
	}

	QStringList errLines = QString(QString::fromLocal8Bit(m_process->readAllStandardError()).remove('\r')).split('\n');
	for (const auto line : errLines)
	{
		if (line.isEmpty())
		{
			continue;
		}
		handleLogLine(censor(line));
	}
}

QString MinecraftProcess::censor(const QString &in) const
{
	QString out = in;
	for (auto it = m_censored.constBegin(); it != m_censored.constEnd(); ++it)
	{
		out.replace(it.key(), it.value());
	}
	return out;
}

void MinecraftProcess::handleLogLine(const QString &line)
{
	if (line.contains("/DEBUG]"))
	{
		emit log(line, Debug);
	}
	else if (line.contains("/TRACE]"))
	{
		emit log(line, Trace);
	}
	else if (line.contains("/INFO]"))
	{
		emit log(line, Info);
	}
	else if (line.contains("/WARN]"))
	{
		emit log(line, Warn);
	}
	else if (line.contains("/ERROR]"))
	{
		emit log(line, Error);
	}
	else if (line.contains("/FATAL]"))
	{
		emit log(line, Fatal);
	}
	else
	{
		emit log(line, Warn);
	}
}

QStringList MinecraftProcess::getMinecraftArguments(MinecraftLaunchData *data)
{
	BaseAccount *account = static_cast<PreLaunchData *>(data->otherStages[Stage::PreLaunch])->accounts["minecraft"];

	QMap<QString, QString> replacements;
	replacements["auth_username"] = account->username();
	replacements["auth_session"] = account->token("session");
	replacements["auth_access_token"] = account->token("access_token");
	replacements["auth_player_name"] = account->token("player_name");
	replacements["auth_uuid"] = account->token("uuid");
	replacements["user_properties"] = account->token("user_properties");
	replacements["user_type"] = account->token("user_type");

	replacements["profile_name"] = m_container->name();
	replacements["version_name"] = m_container->version().toString();

	replacements["game_directory"] = m_container->rootDir().absoluteFilePath("minecraft/");
	replacements["game_assets"] = QDir::current().absoluteFilePath("minecraft/assets/virtual");

	replacements["assets_root"] = QDir::current().absoluteFilePath("minecraft/assets/");
	replacements["assets_index_name"] = data->assets;

	m_censored.clear();
	m_censored.insert(account->token("session"), "<SESSION>");
	m_censored.insert(account->token("access_token"), "<ACCESS TOKEN>");
	m_censored.remove("");

	QString mcArgs = data->mcArgs;
	for (auto it = replacements.constBegin(); it != replacements.constEnd(); ++it)
	{
		mcArgs.replace("${" + it.key() + "}", it.value());
	}
	return mcArgs.split(' ');
}

QStringList MinecraftProcess::getArguments(MinecraftLaunchData *data)
{
	QStringList args = data->javaArgs;
	QStringList paths;
	for (const auto cp : data->classpath)
	{
		paths.append(QDir::current().absoluteFilePath(cp));
	}
	args << "-cp" << paths.join(data->javaVersion->properties().value("path.separator"));
	for (auto it = data->properties.constBegin(); it != data->properties.constEnd(); ++it)
	{
		args << QString("-D%1=%2").arg(it.key(), it.value());
	}
	args << data->mainClass;

	args << getMinecraftArguments(data);

	args.removeAll("");
	args.removeAll(" ");
	return args;
}
