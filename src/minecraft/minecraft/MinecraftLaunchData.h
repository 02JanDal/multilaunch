// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "minecraft/java/JavaLaunchData.h"

class MinecraftLaunchData : public JavaLaunchData
{
public:
	QString mcArgs;
	QString assets;
};
