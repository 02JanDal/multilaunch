// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftArgumentsComponent.h"

#include "core/util/Json.h"
#include "core/tasks/LambdaTask.h"
#include "MinecraftLaunchData.h"

MinecraftArgumentsComponent::MinecraftArgumentsComponent(BaseComponent *parent)
	: BaseComponent(parent)
{
}

Task *MinecraftArgumentsComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::Launch)
	{
		return new LambdaTask<std::function<void()>>([this, data]()
		{
			dynamic_cast<MinecraftLaunchData *>(data)->mcArgs = m_args;
		});
	}
	else
	{
		return nullptr;
	}
}

void MinecraftArgumentsComponent::load(const QJsonObject &obj)
{
	m_args = Json::ensureIsType<QString>(obj, "args");

	BaseComponent::load(obj);
}
QJsonObject MinecraftArgumentsComponent::save() const
{
	QJsonObject obj = BaseComponent::save();
	obj.insert("args", m_args);
	return obj;
}
