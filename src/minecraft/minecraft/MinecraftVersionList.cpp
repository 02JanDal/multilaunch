// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftVersionList.h"

#include "core/util/Json.h"
#include "core/network/CachedDownloadTask.h"
#include "core/util/Json.h"

DEFINE_SINGLETON(g_minecraftList)

MinecraftVersion::MinecraftVersion(const Version &id, const QDateTime &time, const QString &type)
	: m_id(id), m_time(time), m_type(type)
{
}
MinecraftVersion::MinecraftVersion()
{
}
void MinecraftVersion::load(const QJsonObject &obj)
{
	m_id = Version::fromString(Json::ensureIsType<QString>(obj, "id"));
	m_time = Json::ensureIsType<QDateTime>(obj, "time");
	m_type = Json::ensureIsType<QString>(obj, "type");
}
QJsonObject MinecraftVersion::save() const
{
	QJsonObject obj;
	obj.insert("id", m_id.toString());
	obj.insert("time", Json::toJson(m_time));
	obj.insert("type", m_type);
	return obj;
}

bool MinecraftVersion::operator>(const QSharedPointer<BaseVersion> &other) const
{
	const QSharedPointer<MinecraftVersion> mcv = other.dynamicCast<MinecraftVersion>();
	if (mcv)
	{
		return m_time > mcv->m_time;
	}
	else
	{
		return false;
	}
}

class MinecraftVersionListParser : public INetworkValidator
{
public:
	explicit MinecraftVersionListParser(MinecraftVersionList *list)
		: m_list(list) {}

	void validate(const QByteArray &data)
	{
		using namespace Json;
		const QJsonObject root = ensureObject(ensureDocument(data));
		const QJsonArray versions = ensureArray(root, "versions");
		QList<QSharedPointer<BaseVersion>> result;
		for (const auto val : versions)
		{
			const QJsonObject ver = ensureObject(val);
			result.append(QSharedPointer<MinecraftVersion>::create(
							  Version::fromString(ensureIsType<QString>(ver, "id")),
							  QDateTime::fromString(ensureIsType<QString>(ver, "releaseTime"), Qt::ISODate),
							  ensureIsType<QString>(ver, "type")));
		}
		m_list->setVersions(result);
	}

private:
	MinecraftVersionList *m_list;
};

MinecraftVersionList::MinecraftVersionList(QObject *parent)
	: BaseVersionList("minecraft/versions.json", new VersionFactory<MinecraftVersion>, parent)
{
}
Task *MinecraftVersionList::createRefreshTask()
{
	CachedDownloadTask *task = new CachedDownloadTask(tr("Minecraft Versions"), "lists", "minecraft.json",
													  QUrl("http://s3.amazonaws.com/Minecraft.Download/versions/versions.json"));
	task->setValidator(new MinecraftVersionListParser(this));
	return task;
}
