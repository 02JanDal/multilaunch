// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftAssetsComponent.h"

#include <QCryptographicHash>

#include "core/util/Json.h"
#include "core/tasks/LambdaTask.h"
#include "core/network/CachedDownloadTask.h"
#include "core/components/FileDownloadComponent.h"
#include "core/tasks/CopyTask.h"
#include "MinecraftLaunchData.h"

class AssetValidator : public INetworkValidator
{
public:
	explicit AssetValidator(const MinecraftAsset &asset)
		: INetworkValidator(), m_asset(asset)
	{
	}

	void validate(const QByteArray &data) override
	{
		if (data.size() != m_asset.size)
		{
			throw Exception(QObject::tr("Downloaded asset size is wrong (%1 instead of %2, for %3)").arg(data.size()).arg(m_asset.size).arg(m_asset.hash));
		}
		const QByteArray hash = QCryptographicHash::hash(data, QCryptographicHash::Sha1).toHex();
		if (hash != m_asset.hash.toLatin1())
		{
			throw Exception(QObject::tr("Downloaded asset hash is wrong (%1 instead of %2)").arg(QString::fromLatin1(hash), m_asset.hash));
		}
	}

private:
	const MinecraftAsset m_asset;
};

class AssetLegacyConvertComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit AssetLegacyConvertComponent(const MinecraftAsset &asset, BaseComponent *parent)
		: BaseComponent(parent), m_asset(asset)
	{
	}

	QString id() const override { return "Minecraft.AssetsLegacyConvert"; }
	QString name() const override { return ""; }
	QString type() const override { return "Asset convert"; }

	Task *createTask(const Stage stage, IStageData *data)
	{
		if (stage == Stage::Installation)
		{
			return new CopyTask("minecraft/assets/objects/" + m_asset.hash.left(2) + '/' + m_asset.hash,
								"minecraft/assets/virtual/legacy/" + m_asset.file);
		}
		else
		{
			return nullptr;
		}
	}

private:
	const MinecraftAsset m_asset;
};

class AssetComponent : public BaseCompoundComponent
{
	Q_OBJECT
public:
	explicit AssetComponent(const bool isVirtual, const MinecraftAsset &asset, MinecraftAssetsComponent *parent)
		: BaseCompoundComponent(parent)
	{
		m_dlComp = new FileDownloadComponent(this);
		m_dlComp->setDownloadGroup("minecraft/assets/objects");
		m_dlComp->setDownloadId(asset.hash.left(2) + '/' + asset.hash);
		m_dlComp->setUrl(QUrl("http://resources.download.minecraft.net/").resolved(m_dlComp->downloadId()));
		m_dlComp->setValidator(new AssetValidator(asset));
		addChild(m_dlComp);
		if (isVirtual)
		{
			m_convertComp = new AssetLegacyConvertComponent(asset, this);
			addChild(m_convertComp);
		}
	}

	QString id() const override { return "Minecraft.Asset"; }
	QString name() const override { return ""; }
	QString type() const override { return "Asset"; }

private:
	FileDownloadComponent *m_dlComp;
	AssetLegacyConvertComponent *m_convertComp;
};

MinecraftAssetsComponent::MinecraftAssetsComponent(BaseComponent *parent)
	: BaseCompoundComponent(parent)
{
}

Task *MinecraftAssetsComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::Launch)
	{
		return new LambdaTask<std::function<void()>>([this, data]()
		{
			dynamic_cast<MinecraftLaunchData *>(data)->assets = m_index;
		});
	}
	else
	{
		return BaseCompoundComponent::createTask(stage, data);
	}
}

void MinecraftAssetsComponent::setAssets(const QString &index, const bool isVirtual, const QList<MinecraftAsset> &assets)
{
	m_index = index;

	for (auto comp : m_assetComps)
	{
		removeChild(comp);
		delete comp;
	}
	m_assetComps.clear();

	for (const auto &asset : assets)
	{
		AssetComponent *comp = new AssetComponent(isVirtual, asset, this);
		m_assetComps.append(comp);
		addChild(comp);
	}
}

#include "MinecraftAssetsComponent.moc"
