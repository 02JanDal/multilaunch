// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftComponent.h"

#include <QJsonObject>

#include "core/components/FileDownloadComponent.h"
#include "core/components/AuthComponent.h"
#include "core/util/Json.h"
#include "core/tasks/GroupedTask.h"
#include "core/tasks/LambdaTask.h"
#include "core/BaseVersion.h"
#include "core/Container.h"
#include "minecraft/java/JavaMainClassComponent.h"
#include "minecraft/java/JavaLibraryComponent.h"
#include "minecraft/java/JavaNativesComponent.h"
#include "minecraft/java/ClasspathComponent.h"
#include "MinecraftLaunchData.h"
#include "MinecraftTask.h"
#include "MinecraftArgumentsComponent.h"
#include "MinecraftAssetsComponent.h"
#include "MinecraftProcess.h"
#include "MinecraftVersionList.h"

MinecraftComponent::MinecraftComponent(BaseComponent *parent)
	: BaseCompoundComponent(parent)
{
	setPackage(Package("minecraft", "minecraft"));
	m_dlComp = new FileDownloadComponent(this);
	m_cpComp = new ClasspathComponent(this);
	m_mcArgsComp = new MinecraftArgumentsComponent(this);
	m_mainClassComp = new JavaMainClassComponent(this);
	m_assetsComp = new MinecraftAssetsComponent(this);
	setChildren({m_dlComp, m_cpComp, m_mcArgsComp, m_mainClassComp, m_assetsComp,
				 new AuthComponent("minecraft", this)});
}

Task *MinecraftComponent::createTask(const Stage stage, IStageData *data)
{
	if (stage == Stage::PreDownload)
	{
		return new MinecraftTask(this, this);
	}
	else if (stage == Stage::Launch)
	{
		Task *ourTask = new LambdaTask<std::function<void()>>([this, data]()
		{
			dynamic_cast<BaseLaunchData *>(data)->process = new MinecraftProcess(data->container, dynamic_cast<BaseLaunchData *>(data));
			dynamic_cast<JavaLaunchData *>(data)->workingDirectory = data->container->rootDir().absolutePath();
		});
		GroupedTask *task = new GroupedTask();
		task->addTask(ourTask);
		Task *theirTask =  BaseCompoundComponent::createTask(stage, data);
		if (theirTask)
		{
			task->addTask(theirTask);
		}
		return task;
	}
	else
	{
		return BaseCompoundComponent::createTask(stage, data);
	}
}
IStageData *MinecraftComponent::createStageData(const Stage stage) const
{
	if (stage == Stage::Launch)
	{
		return new MinecraftLaunchData;
	}
	else
	{
		return BaseCompoundComponent::createStageData(stage);
	}
}

void MinecraftComponent::load(const QJsonObject &obj)
{
	BaseCompoundComponent::load(obj);
}
QJsonObject MinecraftComponent::save() const
{
	QJsonObject obj = BaseCompoundComponent::save();
	return obj;
}

void MinecraftComponent::setMinecraftData(const QString &id, const QList<BaseComponent *> &libraries, const QString &mainClass, const QString &assetsIndex,
										  const bool assetsAreVirtual, const QList<MinecraftAsset> &assets, const QString &args)
{
	m_dlComp->setDownloadGroup("minecraft/jars");
	m_dlComp->setDownloadId(id + ".jar");
	m_dlComp->setUrl("http://s3.amazonaws.com/Minecraft.Download/versions/" + id + '/' + id + ".jar");
	m_cpComp->setFile("minecraft/jars/" + id + ".jar");

	m_mainClassComp->setClass(mainClass);
	m_assetsComp->setAssets(assetsIndex, assetsAreVirtual, assets);
	m_mcArgsComp->setArguments(args);
	for (auto lib : m_libraries)
	{
		removeChild(lib);
		delete lib;
	}
	m_libraries.clear();
	for (const auto library : libraries)
	{
		addChild(library);
		m_libraries.append(library);
	}
}
