// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include <QDateTime>

#include "core/BaseVersion.h"
#include "core/BaseVersionList.h"
#include "core/util/Singleton.h"

class MinecraftVersion : public BaseVersion
{
public:
	explicit MinecraftVersion(const Version &id, const QDateTime &time, const QString &type);
	explicit MinecraftVersion();

	Version id() const override { return m_id; }
	QString type() const override { return m_type; }

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

	bool operator>(const QSharedPointer<BaseVersion> &other) const override;

private:
	Version m_id;
	QDateTime m_time;
	QString m_type;
};

class MinecraftVersionList : public BaseVersionList
{
	Q_OBJECT
public:
	explicit MinecraftVersionList(QObject *parent = nullptr);

	Task *createRefreshTask() override;

private:
	friend class MinecraftVersionListParser;
};
DECLARE_SINGLETON(MinecraftVersionList, g_minecraftList)
