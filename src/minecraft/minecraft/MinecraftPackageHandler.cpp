// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftPackageHandler.h"

#include "core/packages/Package.h"
#include "core/packages/PackageQuery.h"
#include "core/packages/Version.h"
#include "minecraft/java/JavaArgumentsComponent.h"
#include "MinecraftVersionList.h"
#include "MinecraftComponent.h"
#include "core/Container.h"

MinecraftPackageHandler::MinecraftPackageHandler()
{
}

Task *MinecraftPackageHandler::createFetchInformationTask(const Package &package) const
{
	if (package.id() == "minecraft")
	{
		return g_minecraftList->createRefreshTask();
	}
	else
	{
		return nullptr;
	}
}
BaseVersionList *MinecraftPackageHandler::getVersionsList(const Package &package) const
{
	if (package.id() == "minecraft")
	{
		return g_minecraftList.get();
	}
	else
	{
		return nullptr;
	}
}

BaseComponent *MinecraftPackageHandler::createComponent(const Package &package, Container *container) const
{
	if (package == Package("minecraft", "minecraft"))
	{
		return new MinecraftComponent(container);
	}
	else if (package == Package("java.arguments"))
	{
		return new JavaArgumentsComponent(container);
	}
	else
	{
		return nullptr;
	}
}

QList<PackageQuery> MinecraftPackageHandler::getPackageDependencies(const Package &package, const Version &version) const
{
	if (package == Package("minecraft", "minecraft") && version < Version::fromString("1.6.1"))
	{
		return QList<PackageQuery>() << PackageQuery(Package("core.folder", "texturepacks"))
									 << PackageQuery(Package("java.arguments"));
	}
	else if (package == Package("minecraft", "minecraft") && version >= Version::fromString("1.6.1"))
	{
		return QList<PackageQuery>() << PackageQuery(Package("core.folder", "resourcepacks"))
									 << PackageQuery(Package("java.arguments"));
	}
	else
	{
		return QList<PackageQuery>();
	}
}
