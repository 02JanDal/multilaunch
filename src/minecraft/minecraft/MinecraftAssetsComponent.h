// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseCompoundComponent.h"

class FileDownloadComponent;

struct MinecraftAsset
{
	QString file;
	QString hash;
	int size;
};

class AssetComponent;

class MinecraftAssetsComponent : public BaseCompoundComponent
{
	Q_OBJECT
public:
	explicit MinecraftAssetsComponent(BaseComponent *parent);

	QString id() const override { return "Minecraft.Assets"; }
	QString name() const override { return ""; }
	QString type() const override { return tr("Minecraft Assets"); }
	Task *createTask(const Stage stage, IStageData *data) override;

	void setAssets(const QString &index, const bool isVirtual, const QList<MinecraftAsset> &assets);

private:
	QString m_index;
	FileDownloadComponent *m_indexDlComp;
	QList<AssetComponent *> m_assetComps;
};
