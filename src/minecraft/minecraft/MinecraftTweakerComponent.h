// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseComponent.h"

class MinecraftTweakerComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit MinecraftTweakerComponent(BaseComponent *parent = nullptr);

	QString id() const override { return "Minecraft.Tweaker"; }
	QString name() const override { return m_tweakerClass; }
	QString type() const override { return tr("Minecraft Tweaker"); }
	Task *createTask(const Stage stage, IStageData *data) override;

	QString tweakerClass() const { return m_tweakerClass; }
	void setTweakerClass(const QString &clazz) { m_tweakerClass = clazz; }

private:
	QString m_tweakerClass;
};
