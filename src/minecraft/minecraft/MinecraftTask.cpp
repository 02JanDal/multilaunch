// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftTask.h"

#include <QCryptographicHash>

#include "core/util/Json.h"
#include "core/network/CachedDownloadTask.h"
#include "minecraft/java/JavaLibraryComponent.h"
#include "minecraft/java/JavaNativesComponent.h"
#include "MinecraftComponent.h"
#include "MinecraftAssetsComponent.h"

// ALL minecraft specific hacks should be kept in this file

static LibraryPlatform stringToPlatform(const QString &string)
{
	if (string == "linux")
	{
		return LibraryPlatform::Linux;
	}
	else if (string == "windows")
	{
		return LibraryPlatform::Win;
	}
	else if (string == "osx")
	{
		return LibraryPlatform::OSX;
	}
	return LibraryPlatform::Invalid;
}
static LibraryPlatform arch32(const LibraryPlatform common)
{
	if (common == LibraryPlatform::Linux)
	{
		return LibraryPlatform::Linux32;
	}
	else if (common == LibraryPlatform::Win)
	{
		return LibraryPlatform::Win32;
	}
	else if (common == LibraryPlatform::OSX)
	{
		return LibraryPlatform::OSX32;
	}
	else
	{
		return LibraryPlatform::Invalid;
	}
}
static LibraryPlatform arch64(const LibraryPlatform common)
{
	if (common == LibraryPlatform::Linux)
	{
		return LibraryPlatform::Linux64;
	}
	else if (common == LibraryPlatform::Win)
	{
		return LibraryPlatform::Win64;
	}
	else if (common == LibraryPlatform::OSX)
	{
		return LibraryPlatform::OSX64;
	}
	else
	{
		return LibraryPlatform::Invalid;
	}
}

MinecraftTask::MinecraftTask(MinecraftComponent *component, QObject *parent)
	: StandardTask(parent), m_component(component)
{
}

void MinecraftTask::run()
{
	doMainJson();
}

void MinecraftTask::doMainJson()
{
	using namespace Json;
	const QString version = m_component->version().toString();
	const QJsonObject masterRoot = ensureObject(ensureDocument(
		networkGetCached("Minecraft JSON", "minecraft/jsons", version + ".json",
						 "http://s3.amazonaws.com/Minecraft.Download/versions/" + version +
							 '/' + version + ".json",
						 false, new JsonValidator)));

	setStatus(tr("Converting JSON for Minecraft %1").arg(ensureIsType<QString>(masterRoot, "id")));

	QList<BaseComponent *> libs;
	for (const auto val : ensureArray(masterRoot, "libraries"))
	{
		const QJsonObject obj = ensureObject(val);
		LibraryPlatforms allowed = ~LibraryPlatforms(LibraryPlatform::All);
		if (obj.contains("rules"))
		{
			for (const auto ruleVal : ensureArray(obj, "rules"))
			{
				const QJsonObject rule = ensureObject(ruleVal);
				const QString action = ensureIsType<QString>(rule, "action");
				if (action == "allow")
				{
					if (rule.contains("os"))
					{
						allowed |= stringToPlatform(ensureIsType<QString>(ensureObject(rule, "os"), "name"));
					}
					else
					{
						allowed |= LibraryPlatform::All;
					}
				}
				else if (action == "disallow")
				{
					if (rule.contains("os"))
					{
						allowed &= ~LibraryPlatforms(stringToPlatform(ensureIsType<QString>(ensureObject(rule, "os"), "name")));
					}
					else
					{
						allowed &= ~LibraryPlatforms(LibraryPlatform::All);
					}
				}
			}
		}
		else
		{
			allowed = LibraryPlatform::All;
		}
		if (!(allowed & currentLibraryPlatform))
		{
			continue;
		}
		if (obj.contains("natives"))
		{
			JavaNativesComponent *comp = new JavaNativesComponent(m_component);
			comp->setIdentifier(ensureIsType<QString>(obj, "name"));
			if (obj.contains("url"))
			{
				comp->setBaseUrl(ensureIsType<QUrl>(obj, "url"));
			}
			comp->setAllowedOn(allowed);
			QHash<LibraryPlatform, QString> nats;
			const QJsonObject natives = ensureObject(obj, "natives");
			for (auto it = natives.constBegin(); it != natives.constEnd(); ++it)
			{
				const QString str = ensureIsType<QString>(natives, it.key());
				const LibraryPlatform plat = stringToPlatform(it.key());
				if (str.contains("${arch}"))
				{
					nats.insert(arch32(plat), QString(str).replace("${arch}", "32"));
					nats.insert(arch64(plat), QString(str).replace("${arch}", "64"));
				}
				else
				{
					nats.insert(plat, str);
				}
			}
			comp->setNatives(nats);
			libs.append(comp);
		}
		else
		{
			JavaLibraryComponent *comp = new JavaLibraryComponent(m_component);
			comp->setIdentifier(ensureIsType<QString>(obj, "name"));
			if (obj.contains("url"))
			{
				comp->setBaseUrl(ensureIsType<QUrl>(obj, "url"));
			}
			comp->setAllowedOn(allowed);
			libs.append(comp);
		}
	}

	const QString assetsIndex = ensureIsType<QString>(masterRoot, "assets", "legacy");
	const auto assets = doAssets(assetsIndex);
	m_component->setMinecraftData(
				ensureIsType<QString>(masterRoot, "id"),
				libs,
				ensureIsType<QString>(masterRoot, "mainClass"),
				assetsIndex,
				assets.first,
				assets.second,
				ensureIsType<QString>(masterRoot, "minecraftArguments"));

	setSuccess();
}

QPair<bool, QList<MinecraftAsset>> MinecraftTask::doAssets(const QString &id)
{
	using namespace Json;
	const QByteArray data =
		networkGetCached("Minecraft Assets Index", "minecraft/assets/indexes", id + ".json",
						 QUrl("https://s3.amazonaws.com/Minecraft.Download/indexes/")
							 .resolved(id + ".json"),
						 false, new JsonValidator);
	setStatus(tr("Parsing assets index"));
	const QJsonObject obj = ensureObject(ensureDocument(data));
	const QJsonObject objects = ensureObject(obj, "objects");
	QList<MinecraftAsset> assets;
	for (auto it = objects.constBegin(); it != objects.constEnd(); ++it)
	{
		const QJsonObject assetObj = ensureObject(it.value());
		MinecraftAsset asset;
		asset.file = it.key();
		asset.hash = ensureIsType<QString>(assetObj, "hash");
		asset.size = ensureIsType<int>(assetObj, "size");
		assets.append(asset);
	}
	return qMakePair(ensureIsType<bool>(obj, "virtual", false), assets);
}
