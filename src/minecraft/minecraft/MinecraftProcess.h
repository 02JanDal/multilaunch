// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/BaseProcess.h"

#include <QMap>

class QProcess;
class MinecraftLaunchData;
class Container;

class MinecraftProcess : public BaseProcess
{
	Q_OBJECT
public:
	explicit MinecraftProcess(Container *container, BaseLaunchData *data, QObject *parent = nullptr);

public slots:
	void start() override;
	void abort() override;

private slots:
	void handleOutput();

private:
	Container *m_container;
	QProcess *m_process;
	QMap<QString, QString> m_censored;

	QString censor(const QString &in) const;
	void handleLogLine(const QString &line);

	QStringList getArguments(MinecraftLaunchData *data);
	QStringList getMinecraftArguments(MinecraftLaunchData *data);
};
