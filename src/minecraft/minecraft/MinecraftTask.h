// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/tasks/StandardTask.h"

class MinecraftComponent;
struct MinecraftAsset;

class MinecraftTask : public StandardTask
{
	Q_OBJECT
public:
	explicit MinecraftTask(MinecraftComponent *component, QObject *parent = nullptr);

private:
	MinecraftComponent *m_component;

	void run() override;

	void doMainJson();
	QPair<bool, QList<MinecraftAsset> > doAssets(const QString &id);
};
