// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseComponent.h"

class MinecraftArgumentsComponent : public BaseComponent
{
	Q_OBJECT
public:
	explicit MinecraftArgumentsComponent(BaseComponent *parent);

	QString id() const override { return "Minecraft.Arguments"; }
	QString name() const override { return m_args; }
	QString type() const override { return tr("Minecraft Arguments"); }
	Task *createTask(const Stage stage, IStageData *data) override;

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

	void setArguments(const QString &args) { m_args = args; }
	QString arguments() const { return m_args; }

private:
	QString m_args;
};
