// Licensed under the Apache-2.0 license. See README.md for details.

#include "MinecraftAccount.h"

#include "MinecraftAuthTask.h"

MinecraftAccount::MinecraftAccount()
	: BaseAccount()
{
}
QString MinecraftAccount::avatar() const
{
	return "https://crafatar.com/avatars/" + token("uuid");
}
QString MinecraftAccount::bigAvatar() const
{
	return "https://crafatar.com/renders/body/" + token("uuid");
}
Task *MinecraftAccount::createLoginTask(const QString &username, const QString &password)
{
	setToken("login_username", username);
	return new MinecraftAuthenticateTask(username, password, this);
}
Task *MinecraftAccount::createCheckTask()
{
	return new MinecraftValidateTask(this);
}
Task *MinecraftAccount::createLogoutTask()
{
	return new MinecraftInvalidateTask(this);
}
