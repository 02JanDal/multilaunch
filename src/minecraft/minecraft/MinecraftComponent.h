// Licensed under the Apache-2.0 license. See README.md for details.

#pragma once

#include "core/components/BaseCompoundComponent.h"

class FileDownloadComponent;
class ClasspathComponent;
class JavaLibraryComponent;
class MinecraftArgumentsComponent;
class JavaMainClassComponent;
class MinecraftTask;
class MinecraftAssetsComponent;
struct MinecraftAsset;

class MinecraftComponent : public BaseCompoundComponent
{
	Q_OBJECT
public:
	explicit MinecraftComponent(BaseComponent *parent);

	QString id() const override { return "Minecraft.Minecraft"; }
	QString name() const override { return tr("Minecraft"); }
	QString type() const override { return tr("Minecraft"); }
	bool canBeRoot() const override { return true; }
	IStageData *createStageData(const Stage stage) const override;
	Task *createTask(const Stage stage, IStageData *data) override;

	void load(const QJsonObject &obj) override;
	QJsonObject save() const override;

	bool canChangeVersion() const override { return true; }

	// for usage by MinecraftTask
	void setMinecraftData(const QString &id, const QList<BaseComponent *> &libraries, const QString &mainClass, const QString &assetsIndex,
						  const bool assetsAreVirtual, const QList<MinecraftAsset> &assets, const QString &args);

private:
	FileDownloadComponent *m_dlComp;
	ClasspathComponent *m_cpComp;
	QList<BaseComponent *> m_libraries;
	MinecraftArgumentsComponent *m_mcArgsComp;
	JavaMainClassComponent *m_mainClassComp;
	MinecraftAssetsComponent *m_assetsComp;
};
